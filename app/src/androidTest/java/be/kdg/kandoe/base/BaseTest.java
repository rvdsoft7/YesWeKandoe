package be.kdg.kandoe.base;

import android.support.annotation.StringRes;
import android.support.design.widget.NavigationView;
import android.support.test.espresso.*;
import android.support.test.espresso.contrib.*;
import android.support.test.espresso.matcher.*;
import android.support.test.filters.*;
import android.support.test.internal.util.*;
import android.support.test.rule.*;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.ListView;

import org.hamcrest.*;
import org.junit.*;
import org.junit.rules.*;

import java.io.IOException;
import java.util.List;

import be.kdg.kandoe.MyApplication;
import be.kdg.kandoe.R;
import be.kdg.kandoe.frontend.activities.MainActivity;
import be.kdg.kandoe.util.TestApplication;
import be.kdg.kandoe.util.azimolabs.conditionwatcher.ConditionWatcher;
import be.kdg.kandoe.util.azimolabs.conditionwatcher.Instruction;
import be.kdg.kandoe.util.azimolabs.conditionwatcher.InstructionFactory;
import be.kdg.kandoe.utils.MyLog;
import retrofit2.Call;
import retrofit2.Response;


import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static be.kdg.kandoe.util.azimolabs.conditionwatcher.InstructionFactory.activityDoneLoading;
import static be.kdg.kandoe.util.azimolabs.conditionwatcher.InstructionFactory.isCurrentFragment;
import static org.hamcrest.core.AllOf.*;
import static org.junit.Assert.*;

/**
 * Created by Ruben on 11/03/2017.
 */

@LargeTest
public class BaseTest {

    static {
        MyApplication.setTestingMode(true);
    }

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Rule
    public TestName name = new TestName();


    public BaseTest(){
    }

    @SuppressWarnings("unused")
    public String getMethodName() {
        return name.getMethodName();
    }


    public MainActivity getActivity() {
        return TestApplication.getInstance().getMainActivity();
    }

    public static void clickOk() {
        onView(allOf(withText(android.R.string.ok), isDisplayed(), isClickable())).perform(click());
    }

    public static void clickCancel() {
        onView(allOf(withText(android.R.string.cancel), isDisplayed(), isClickable())).perform(click());
    }

    public void openOverFlowItem(@StringRes int stringResId) {
        openActionBarOverflowOrOptionsMenu(getActivity());
        onView(withText(stringResId)).perform(click());
    }

    public static <T> T executeCall(Call<T> call) throws IOException {
       Response<T> response= call.execute();
        assertTrue("response!=null",response!=null);
        MyLog.d(BaseTest.class.getSimpleName(), "executeCall: "+response.raw().request().url().toString() +" "+response.isSuccessful());
        assertTrue("response must be successful", response.isSuccessful());
        return response.body();
    }

    public void waitFor(Instruction instruction) {
        ConditionWatcher.waitForCondition(instruction);
    }



    /**
     * waits until specified <code>viewAssertion</code> is true or <code>timeout</code> has passed
     * the <code>interaction</code> needs to be present beforehand
     * @param interaction object needs to be an instance of either {@link ViewInteraction} or {@link DataInteraction} and needs to be present
     * @param viewAssertion
     */
    public void waitFor(Object interaction, ViewAssertion viewAssertion) {
        waitFor(InstructionFactory.interaction(this, interaction, viewAssertion));
    }

    public static Matcher<View> withListViewSize(final Matcher<Integer> countMatcher) {
        Checks.checkNotNull(countMatcher);
        return  new TypeSafeMatcher<View>(){

            @Override
            public void describeTo(Description description) {

            }

            @Override
            protected boolean matchesSafely(View item) {
                ListView listView = (ListView) item;
                return countMatcher.matches(listView.getCount());
            }
        };
    }

    public static Matcher<View> withRecyclerViewSize(final Matcher<Integer> countMatcher) {
        Checks.checkNotNull(countMatcher);
        return  new TypeSafeMatcher<View>(){

            @Override
            public void describeTo(Description description) {

            }

            @Override
            protected boolean matchesSafely(View item) {
                RecyclerView recyclerView = (RecyclerView) item;
                return countMatcher.matches(recyclerView.getAdapter().getItemCount());
            }
        };
    }


    public static ViewInteraction snackbar(Matcher matcher) {
        return onView(allOf(withId(android.support.design.R.id.snackbar_text), matcher));
    }

    public void waitUntilLoadingDialogFinished() {
        waitFor(activityDoneLoading(this));
    }

    /**
     * wait until the fragment with class <code>fragClass</code> the specified class is found in the fragment manager
     * @param fragClass
     */
    public void waitUntilFragmentLoaded(Class<? extends Fragment> fragClass) {
        waitFor(isCurrentFragment(this, fragClass));
    }

    public static ViewInteraction drawer() {
       return onView(withId(R.id.drawer_layout));
    }

    public static void openDrawer() {
        drawer().perform(DrawerActions.open(Gravity.LEFT));
    }

    /**
     * click event on select navigationview menu item id
     * @param menuItem
     * @return
     */
    public static void navigateToNavigationMenuItem(int menuItem, int stringResId){
        ViewInteraction appCompatCheckedTextView = onView(
                allOf(withId(R.id.design_menu_item_text),  hasSibling(withText(stringResId))));
        appCompatCheckedTextView.perform(click());
//
//        try {
//            onView(withId(R.id.nvView)).perform(NavigationViewActions.navigateTo(menuItem));
//        } catch (PerformException e){
//            onView(isAssignableFrom(NavigationView.class)).perform(swipeUp());
//            onView(withId(R.id.nvView)).perform(NavigationViewActions.navigateTo(menuItem));
//        }

    }

    public static <T> Matcher<Object> withEntry(final Matcher<T> entryMatcher, Class<T> clazz) {
        Checks.checkNotNull(entryMatcher);
        return new BoundedMatcher<Object, T>(clazz) {
            @Override
            public boolean matchesSafely(T item) {
                return entryMatcher.matches(item);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("with item: " + entryMatcher.toString());
                entryMatcher.describeTo(description);
            }
        };
    }
}

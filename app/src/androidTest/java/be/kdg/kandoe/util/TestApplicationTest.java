package be.kdg.kandoe.util;

import android.app.Activity;
import android.support.test.filters.*;

import org.junit.*;

import be.kdg.kandoe.base.BaseTest;


import static org.junit.Assert.assertNotNull;

/**
 * Created by Ruben on 19/03/2017.
 */


@LargeTest
public class TestApplicationTest extends BaseTest {

    @Test
    public void setup(){
     Activity activity= TestApplication.getInstance().getCurrentActivity();
        assertNotNull(activity);
    }
}

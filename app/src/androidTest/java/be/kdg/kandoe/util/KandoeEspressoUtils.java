package be.kdg.kandoe.util;

import android.support.test.espresso.*;

/**
 * Created by Ruben on 11/03/2017.
 */

public class KandoeEspressoUtils {

    public static void check(Object interaction, ViewAssertion viewAssertion) {
        if(interaction instanceof DataInteraction) {
            ((DataInteraction) interaction).check(viewAssertion);
        } else if(interaction instanceof ViewInteraction) {
            ((ViewInteraction) interaction).check(viewAssertion);
        } else {
            throw new IllegalArgumentException(" incompatible interaction "+interaction.getClass());
        }
    }
}

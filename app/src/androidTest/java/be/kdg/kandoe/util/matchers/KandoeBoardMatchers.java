package be.kdg.kandoe.util.matchers;

import android.support.test.espresso.matcher.*;
import android.view.View;

import org.hamcrest.*;


import be.kdg.kandoe.backend.dom.Session.SessionCard;
import be.kdg.kandoe.frontend.views.KandoeCard;
import be.kdg.kandoe.frontend.views.KandoeCircle;


import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Ruben on 19/03/2017.
 */

public class KandoeBoardMatchers {

    public static Matcher<View> withSessionCard(final Matcher<SessionCard> sessionCardMatcher) {
        checkNotNull(sessionCardMatcher);
        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("has session card matching: ");
                sessionCardMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                if(!(view instanceof KandoeCard)) {
                    return false;
                }
                KandoeCard kandoeCard = (KandoeCard) view;

                return sessionCardMatcher.matches(kandoeCard.getCard());
            }
        };
    }

    public static Matcher<View> withLevel(final Matcher<Integer> levelMatcher) {
        checkNotNull(levelMatcher);
        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("has level matching: ");
                levelMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                if(!(view instanceof KandoeCard)) {
                    return false;
                }
                KandoeCard kandoeCard = (KandoeCard) view;

                return levelMatcher.matches(kandoeCard.getLevel());
            }
        };
    }
}

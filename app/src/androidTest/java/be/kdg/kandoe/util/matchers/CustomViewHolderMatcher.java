package be.kdg.kandoe.util.matchers;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;

import org.hamcrest.*;


import static org.hamcrest.Matchers.any;

public class CustomViewHolderMatcher extends TypeSafeMatcher<ViewHolder> {
    private final Matcher<View> itemMatcher;

    public CustomViewHolderMatcher(Matcher<View> itemMatcher) {
      this.itemMatcher = itemMatcher;
    }

    @Override
    public boolean matchesSafely(RecyclerView.ViewHolder viewHolder) {
      return itemMatcher.matches(viewHolder.itemView);
    }

    @Override
    public void describeTo(Description description) {
      description.appendText("is assignable from CustomViewHolderMatcher");
    }
  }
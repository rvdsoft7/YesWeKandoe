package be.kdg.kandoe.util;

import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;
import android.support.test.*;

import be.kdg.kandoe.MyApplication;
import be.kdg.kandoe.frontend.activities.MainActivity;

/**
 * Created by Ruben on 19/03/2017.
 */

public class TestApplication extends MyApplication implements ActivityLifecycleCallbacks {
    private Activity currentActivity;

    @Override
    public void onCreate() {
        super.onCreate();
        registerActivityLifecycleCallbacks(this);
    }

    public static TestApplication getInstance() {
        return (TestApplication) InstrumentationRegistry.getTargetContext().getApplicationContext();
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        currentActivity = activity;
    }

    @Override
    public void onActivityStarted(Activity activity) {
        currentActivity = activity;
    }

    @Override
    public void onActivityResumed(Activity activity) {
        currentActivity = activity;
    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }

    public MainActivity getMainActivity() {
        return (MainActivity) getCurrentActivity();
    }

    public Activity getCurrentActivity() {
        return currentActivity;
    }
}

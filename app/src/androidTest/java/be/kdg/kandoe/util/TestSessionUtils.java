package be.kdg.kandoe.util;

import android.support.test.espresso.*;
import android.support.test.espresso.contrib.*;
import android.support.v4.view.ViewPager;
import android.view.View;

import org.hamcrest.*;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.Session.Session;
import be.kdg.kandoe.backend.dom.Session.SessionCard;
import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.login.LoginHelperImpl;
import be.kdg.kandoe.backend.retrofit.service.factory.ServiceGenerator;
import be.kdg.kandoe.base.BaseTest;
import be.kdg.kandoe.util.matchers.CustomViewHolderMatcher;
import be.kdg.kandoe.util.matchers.KandoeBoardMatchers;
import be.kdg.kandoe.utils.Utils;
import java8.util.stream.StreamSupport;


import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.assertion.ViewAssertions.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static be.kdg.kandoe.base.BaseTest.withListViewSize;
import static be.kdg.kandoe.util.azimolabs.conditionwatcher.InstructionFactory.viewPagerNotScrolling;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.core.AllOf.allOf;
import static org.junit.Assert.*;

/**
 * Created by Ruben on 18/03/2017.
 */

public class TestSessionUtils {


    public static Session findTestSession(BaseTest baseTest, boolean throwErrorIfNotFound) throws IOException {
        User loggedInUser = LoginHelperImpl.getInstance().getUser();
        List<Session> sessionList = baseTest.executeCall(ServiceGenerator.getInstance().createSessionService().getSessionsByUserId(loggedInUser.getId()));
        String sessionName = TestSessionConstants.SESSION_NAME;
        Session session = sessionList.stream().filter(s -> s.getName().equals(sessionName)).findAny().orElse(null);
        if (throwErrorIfNotFound)
            assertNotNull("session with name '" + sessionName + "' should be present", session);
        return session;
    }

    public static void waitForViewContainingString(BaseTest baseTest, int viewResId, String str) {
        baseTest.waitFor(onView(withId(viewResId)), matches(withText(containsString(str))));
    }

    public static void waitForViewContainingString(BaseTest baseTest, int viewResId, int stringResId) {
        String str = baseTest.getActivity().getString(stringResId);
        waitForViewContainingString(baseTest, viewResId, str);
    }

    public static void goToTestSessionScreen(BaseTest baseTest, Session session) {
        baseTest.openDrawer();
        //go to my sessions
        baseTest.navigateToNavigationMenuItem(R.id.menu_mijn_sessies, R.string.menu_my_sessions);

        onView(withId(R.id.viewpager)).check(matches(isDisplayed()));

        ViewPager viewPager = (ViewPager) baseTest.getActivity().findViewById(R.id.viewpager);

        //go to idle mode when not scrolling
        baseTest.waitFor(viewPagerNotScrolling(baseTest, viewPager));

        //goto correct tab
        int stringResId;
        if (session.isPlanned()) {
            stringResId = R.string.planned;
        } else if (session.isPlaying()) {
            stringResId = R.string.active;
        } else {
            stringResId = R.string.previous;
        }
        onView(allOf(isDescendantOfA(withId(R.id.titles)), withText(stringResId))).perform(click());

        Matcher<View> matcher = allOf(withId(R.id.listView), isCompletelyDisplayed());
        //wait until listview is loaded
        baseTest.waitFor(onView(matcher), matches(allOf(withListViewSize(Matchers.greaterThan(0)), isDisplayed())));
        //find the session item view containing the correct session and click on it
        onData(allOf(instanceOf(Session.class), baseTest.withEntry(equalTo(session), Session.class))).inAdapterView(matcher).atPosition(0).perform(click());

        baseTest.waitUntilLoadingDialogFinished();

    }

    public static ViewInteraction getCircleSlot(SessionCard sessionCard) {
      return onView(allOf(isDescendantOfA(withId(R.id.kandoeBoard)),
                KandoeBoardMatchers.withSessionCard(equalTo(sessionCard)),
                KandoeBoardMatchers.withLevel(equalTo(sessionCard.getLevel()))
        ));
    }

    public static void isDisplayedInRecyclerView(SessionCard sessionCard) {
        onView(withId(R.id.recyclerview)).perform(RecyclerViewActions.scrollToHolder(new CustomViewHolderMatcher(allOf(
                hasDescendant(allOf(withText(sessionCard.getText()), withId(R.id.cardText))),
                hasDescendant(allOf(withText(String.valueOf(sessionCard.getLevel())), withId(R.id.level))
                )))
        ));
    }

    public static void checkIfSessionCardsShownCorrectly(Collection<SessionCard> sessionCards) {
        if (Utils.isEmpty(sessionCards))
            return;
        //check if the sessionCards are displayed with correct name and the correct level

        //check recyclerview
        StreamSupport.stream(sessionCards).forEach(sessionCard -> {
            //this function will make sure all data is loaded from the adapter
            isDisplayedInRecyclerView(sessionCard);
        });

        //check kandoeboard
        StreamSupport.stream(sessionCards).forEach(sessionCard1 -> {
           getCircleSlot(sessionCard1).check(matches(isDisplayed()));;
        });
    }

}

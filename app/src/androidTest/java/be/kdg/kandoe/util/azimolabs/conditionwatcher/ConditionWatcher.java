package be.kdg.kandoe.util.azimolabs.conditionwatcher;

import android.support.test.*;

/**
 * Created by F1sherKK on 08/10/15.
 */
@SuppressWarnings("unused")
public class ConditionWatcher {

    public static final int CONDITION_NOT_MET = 0;
    public static final int CONDITION_MET = 1;
    public static final int TIMEOUT = 2;

    public static final long DEFAULT_TIMEOUT_LIMIT = 1000 * 15;
    public static final long DEFAULT_INTERVAL = 250;

    private long timeoutLimit = DEFAULT_TIMEOUT_LIMIT;
    private long watchInterval = DEFAULT_INTERVAL;

    private static ConditionWatcher conditionWatcher;

    private ConditionWatcher() {
        super();
    }

    public static ConditionWatcher getInstance() {
        if (conditionWatcher == null) {
            conditionWatcher = new ConditionWatcher();
        }
        return conditionWatcher;
    }

    public static void waitForCondition(Instruction instruction) {
        int status = CONDITION_NOT_MET;
        int elapsedTime = 0;

        do {
            if (instruction.checkCondition()) {
                status = CONDITION_MET;
            } else {
                elapsedTime += getInstance().watchInterval;
                try {
                    Thread.sleep(getInstance().watchInterval);
                } catch (InterruptedException e) {
                   throw new RuntimeException(e);
                }
            }

            if (elapsedTime == getInstance().timeoutLimit) {
                status = TIMEOUT;
                break;
            }
        } while (status != CONDITION_MET);

        if (status == TIMEOUT)
            throw new RuntimeException(instruction.getDescription() + " - took more than " + getInstance().timeoutLimit/1000 + " seconds. Test stopped.");

        instruction.cleanUp();
    }

    public static void setWatchInterval(long watchInterval) {
        getInstance().watchInterval = watchInterval;
    }

    public static void setTimeoutLimit(long ms) {
        getInstance().timeoutLimit = ms;
    }


}

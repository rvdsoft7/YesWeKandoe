package be.kdg.kandoe.util.azimolabs.conditionwatcher;

import be.kdg.kandoe.util.TestApplication;
import be.kdg.kandoe.base.BaseTest;
import be.kdg.kandoe.frontend.activities.MainActivity;

/**
 * Created by F1sherKK on 16/12/15.
 */
public abstract class Instruction {


    public Instruction(BaseTest baseTest) {
        init();
    }

    protected void init() {

    }

    protected void cleanUp() {

    }

    public MainActivity getActivity() {
        return TestApplication.getInstance().getMainActivity();
    }

    public abstract String getDescription();

    public abstract boolean checkCondition();

    public Instruction inverse() {
        Instruction self = this;
        return new Instruction(null) {
            @Override
            public String getDescription() {
                return "not "+self.getDescription();
            }

            @Override
            public boolean checkCondition() {
                return !self.checkCondition();
            }
        };
    }
}

package be.kdg.kandoe.util;

import java.util.List;

import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.retrofit.service.mock.user.MockUserRepo;
import java8.util.stream.Collectors;
import java8.util.stream.RefStreams;

/**
 * Created by Ruben on 18/03/2017.
 */

public class TestSessionConstants {

    public static final String SESSION_NAME = "11Se$$i0n Xabcdefg";

    public static List<User> getParticipants() {
        MockUserRepo repo = MockUserRepo.getInstance();
        return RefStreams.of(repo.findAdmin(), repo.findUserById(2)).collect(Collectors.toList());
    }
}

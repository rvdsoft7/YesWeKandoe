package be.kdg.kandoe.util;

import android.support.design.widget.NavigationView;
import android.support.test.espresso.*;
import android.view.View;

import org.hamcrest.*;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.login.LoginHelperImpl;
import be.kdg.kandoe.base.BaseTest;
import be.kdg.kandoe.frontend.fragments.LoginFragment;
import be.kdg.kandoe.utils.MyLog;
import java8.util.stream.RefStreams;


import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.assertion.ViewAssertions.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static be.kdg.kandoe.base.BaseTest.navigateToNavigationMenuItem;
import static be.kdg.kandoe.base.BaseTest.openDrawer;
import static be.kdg.kandoe.util.azimolabs.conditionwatcher.InstructionFactory.isCurrentFragment;
import static be.kdg.kandoe.util.azimolabs.conditionwatcher.InstructionFactory.isLoggedOff;
import static be.kdg.kandoe.util.azimolabs.conditionwatcher.InstructionFactory.loginHelperNotLoading;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.core.AllOf.*;
import static org.junit.Assert.*;

/**
 * Created by Ruben on 18/03/2017.
 */

public class TestLoginUtils {

    public static final String TAG = TestLoginUtils.class.getSimpleName();

    /**
     * do a {@link #navigateToLogin(BaseTest, boolean)} and log in the given <code>user</code>
     *
     * @param baseTest
     * @param user
     */
    public static User loginUser(BaseTest baseTest, User user) {
        TestLoginUtils.navigateToLogin(baseTest, true);
        onView(withId(R.id.emailEdit)).perform(replaceText(user.getUsername()));
        onView(withId(R.id.passwordEdit)).perform(replaceText(user.getPassword()));
        //login
        ViewInteraction loginBtn = onView(withId(R.id.loginButton));
        loginBtn.perform(click());

        //wait until dialog is gone
        baseTest.waitUntilLoadingDialogFinished();
        //check if user logged in successfully
        baseTest.snackbar(withText(R.string.login_success)).check(matches(isDisplayed()));
        User loggedInUser = LoginHelperImpl.getInstance().getUser();
        assertEquals("check if user really logged in: ", user, loggedInUser);
        return loggedInUser;
    }

    /**
     * wait until loginHelper has finished loading
     * @param baseTest
     */
    public static void waitUntilLoginLoadingCompleted(BaseTest baseTest) {
        MyLog.d(TAG, "waitUntilLoginLoadingCompleted: "+baseTest);
        baseTest.waitFor(loginHelperNotLoading(baseTest));
    }

    /**
     * log out and open te login screen
     *
     * @param baseTest
     */
    public static void navigateToLogin(BaseTest baseTest, boolean logOut) {
        MyLog.d(TAG, "navigateToLogin: "+baseTest.getClass().getSimpleName());

        //wait until loginhelper has finished loading
        waitUntilLoginLoadingCompleted(baseTest);

        if (logOut) {
            logOut(baseTest);
        }
        openDrawer();

        //check if is logged out after a check or perform method is called.
        assertTrue(!LoginHelperImpl.getInstance().isLoggedIn());
        navigateToNavigationMenuItem(R.id.menu_sign_in, R.string.menu_sign_in);


        baseTest.waitFor(isCurrentFragment(baseTest, LoginFragment.class));
    }

    /**
     * log out the user if he's logged in
     */
    public static void logOut(BaseTest baseTest) {
        //wait until loginhelper has finished loading
        waitUntilLoginLoadingCompleted(baseTest);
        boolean loggedIn = LoginHelperImpl.getInstance().isLoggedIn();
        MyLog.d(TAG, "logOut: "+baseTest.getClass().getSimpleName()+ " "+loggedIn);
        if (loggedIn) {
            openDrawer();

            navigateToNavigationMenuItem(R.id.menu_sign_out, R.string.menu_sign_out);

            //wait for logOut To Complete
            baseTest.waitFor(isLoggedOff(baseTest));
        }
    }

    public static void checkCorrectDrawerItemsDisplayed(BaseTest baseTest) {
//        ViewAssertion a, b;
//        if(LoginHelperImpl.getInstance().isLoggedIn()) {
//            a = matches(isDisplayed());
//            b = doesNotExist();
//        } else {
//            a = doesNotExist();
//            b = matches(isDisplayed());
//        }
//        //is correct menu shown
//        RefStreams.of(R.id.menu_mijn_profiel, R.id.menu_sign_out, R.id.menu_mijn_sessies, R.id.menu_mijn_themas)
//                .forEach(resId -> hasNavigationMenuItem(resId));
//
//        //is log in button not visible
//        RefStreams.of(R.id.menu_sign_in) .forEach(resId -> hasNavigationMenuItem(resId));
    }

    public static boolean hasNavigationMenuItem(int menuResId) {
        onView(withId(R.id.nvView)).check(matches(withMenuResId(menuResId)));
        return true;
    }

    public static Matcher<View> withMenuResId(int menuResId) {
       return new TypeSafeMatcher<View>() {

           @Override
           public void describeTo(Description description) {
           }

           @Override
           protected boolean matchesSafely(View item) {
               NavigationView navigationView = (NavigationView) item;
               return navigationView.getMenu().findItem(menuResId)!=null;
           }
       };
    }



}

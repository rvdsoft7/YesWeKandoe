package be.kdg.kandoe.util.azimolabs.conditionwatcher;

import android.support.test.espresso.*;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;

import junit.framework.AssertionFailedError;

import be.kdg.kandoe.backend.login.LoginHelperImpl;
import be.kdg.kandoe.base.BaseTest;
import be.kdg.kandoe.util.KandoeEspressoUtils;
import be.kdg.kandoe.util.azimolabs.conditionwatcher.Instruction;
import be.kdg.kandoe.utils.MyLog;

/**
 * Created by Ruben on 19/03/2017.
 */

public class InstructionFactory {

    public static Instruction isCurrentFragment(BaseTest baseTest, final Class<? extends Fragment> fragClass) {
        return new Instruction(baseTest) {
            @Override
            public String getDescription() {
                return "isCurrentFragment";
            }

            @Override
            public boolean checkCondition() {
                Fragment fragment = getActivity().getCurrentFragment();
                if(fragment!=null && fragClass.equals(fragment.getClass())) {
                    return true;
                }
                return false;
            }
        };
    }

    public static Instruction viewPagerNotScrolling(BaseTest baseTest, final ViewPager viewPager) {
        return new Instruction(baseTest)  {
            @Override
            protected void init() {
                viewPager.addOnPageChangeListener(viewPagerListener);
            }

            @Override
            protected void cleanUp() {
                viewPager.removeOnPageChangeListener(viewPagerListener);
            }

            ViewPagerListener viewPagerListener = new ViewPagerListener();

            boolean mIdle = true;// Default to idle since we can't query the scroll state.
            class ViewPagerListener extends ViewPager.SimpleOnPageChangeListener {
                @Override
                public void onPageScrollStateChanged(int state) {
                    mIdle = (state == ViewPager.SCROLL_STATE_IDLE
                            // Treat dragging as idle, or Espresso will block itself when swiping.
                            || state == ViewPager.SCROLL_STATE_DRAGGING);
                }
            }
            @Override
            public String getDescription() {
                return "viewPagerNotScrolling";
            }

            @Override
            public boolean checkCondition() {
                return mIdle;
            }
        };
    }

    public static Instruction activityDoneLoading(BaseTest baseTest) {
        return new Instruction(baseTest)  {

            @Override
            public String getDescription() {
                return "activityDoneLoading";
            }

            @Override
            public boolean checkCondition() {
                return !getActivity().isLoading();
            }
        };
    }

    public static Instruction dialogShowing(BaseTest baseTest, final Class<? extends DialogFragment> fragClass) {
        return new Instruction(baseTest)  {

            @Override
            public String getDescription() {
                return "dialogShowing";
            }

            @Override
            public boolean checkCondition() {
                return getActivity().getFragment(fragClass)!=null;
            }
        };
    }

    public static Instruction dialogNotShowing(BaseTest baseTest, final Class<? extends DialogFragment> fragClass) {
        return dialogShowing(baseTest, fragClass).inverse();
    }

    public static Instruction loginHelperNotLoading(BaseTest baseTest){
        return new Instruction(baseTest)  {
            @Override
            public String getDescription() {
                return "loginHelperNotLoading";
            }

            @Override
            public boolean checkCondition() {
                return !LoginHelperImpl.getInstance().isLoading();
            }
        };
    }

    /**
     * given <code>interaction</code> needs to be present
     * @param baseTest
     * @param interaction
     * @param viewAssertion
     * @return
     */
    public static Instruction interaction(BaseTest baseTest, final Object interaction, final ViewAssertion viewAssertion) {
        return new Instruction(baseTest)  {

            private volatile View mTestView;

            private boolean testView() {

                if(mTestView != null) {
                    try {
                        viewAssertion.check(mTestView, null);
                        return true;
                    }
                    catch(AssertionFailedError ex) {
                        return false;
                    }
                }
                else {
                    return false;
                }
            }


            @Override
            protected void init() {
                ViewAssertion customViewAssertion = (view, noViewFoundException) -> mTestView = view;
                KandoeEspressoUtils.check(interaction, customViewAssertion);
            }

            @Override
            public String getDescription() {
                return "interaction";
            }

            @Override
            public boolean checkCondition() {
                return testView();
            }
        };
    }

    public static Instruction isLoggedOff(BaseTest baseTest) {
        return new Instruction(baseTest)  {
            @Override
            public String getDescription() {
                return "isLoggedOff";
            }

            @Override
            public boolean checkCondition() {
                return !LoginHelperImpl.getInstance().isLoading();
            }
        };
    }

}

package be.kdg.kandoe.frontend.fragments.user;


import android.support.test.espresso.*;
import android.support.test.rule.*;

import org.junit.*;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.retrofit.service.util.ErrorResult;
import be.kdg.kandoe.backend.retrofit.service.mock.user.MockUserRepo;
import be.kdg.kandoe.base.BaseTest;
import be.kdg.kandoe.frontend.activities.MainActivity;
import be.kdg.kandoe.frontend.fragments.RegistreerFragment;
import be.kdg.kandoe.util.TestLoginUtils;


import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.assertion.ViewAssertions.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static junit.framework.Assert.assertNotNull;
import static org.hamcrest.Matchers.*;

public class RegisterTest extends BaseTest {
    private User user = MockUserRepo.getInstance().findUserById(2);

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void navigateToRegister() {

        TestLoginUtils.logOut(this);

        openDrawer();
        navigateToNavigationMenuItem(R.id.menu_sign_in, R.string.menu_sign_in);

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.registerButton), withText(R.string.register), isDisplayed()));
        appCompatButton.perform(click());

    }

    @Test
    public void validatePageTest() {

        ViewInteraction voornaamInput = onView(withId(R.id.voornaamEdit));
        voornaamInput.check(matches(isDisplayed()));

        ViewInteraction achternaamInput = onView(withId(R.id.achternaamEdit));
        achternaamInput.check(matches(isDisplayed()));

        ViewInteraction emailInput = onView(withId(R.id.emailEdit));
        emailInput.check(matches(isDisplayed()));

        ViewInteraction passwordInput = onView(withId(R.id.passwordEdit));
        passwordInput.check(matches(isDisplayed()));

        ViewInteraction password2Input = onView(withId(R.id.passwordEdit2));
        password2Input.check(matches(isDisplayed()));

        ViewInteraction button = onView(withId(R.id.registerButton));
        button.check(matches(isDisplayed()));
    }

    @Test
    public void noEmailTest() {

        ViewInteraction myEditText = onView(withId(R.id.emailEdit));
        myEditText.perform(scrollTo(), replaceText(""), closeSoftKeyboard());

        ViewInteraction appCompatButton2 = onView(withId(R.id.registerButton));
        appCompatButton2.perform(scrollTo(), click());

        ViewInteraction textView = onView(withText(R.string.validation_email));
        textView.check(matches(isDisplayed()));
    }

    @Test
    public void passwordMismatchTest() {
        ViewInteraction myEditText4 = onView(withId(R.id.emailEdit));
        myEditText4.perform(scrollTo(), replaceText("user@example.com"), closeSoftKeyboard());

        ViewInteraction myEditText5 = onView(
                withId(R.id.passwordEdit));
        myEditText5.perform(scrollTo(), replaceText("password5"), closeSoftKeyboard());

        ViewInteraction appCompatButton3 = onView(
                allOf(withId(R.id.registerButton), withText(R.string.register)));
        appCompatButton3.perform(scrollTo(), click());

        ViewInteraction textView = onView(withText(R.string.validation_password_confirm));
        textView.check(matches(isDisplayed()));
    }

    @Test
    public void noPasswordTest() {
        ViewInteraction myEditText4 = onView(withId(R.id.emailEdit));
        myEditText4.perform(scrollTo(), replaceText("user@example.com"), closeSoftKeyboard());

        ViewInteraction myEditText6 = onView(withId(R.id.passwordEdit));
        myEditText6.perform(scrollTo(), replaceText(""), closeSoftKeyboard());

        ViewInteraction appCompatButton4 = onView(
                allOf(withId(R.id.registerButton), withText(R.string.register)));
        appCompatButton4.perform(scrollTo(), click());

        ViewInteraction textView = onView(withText(R.string.validation_password));
        textView.check(matches(isDisplayed()));
    }

    @Test
    public void userAlreadyExistsTest() {

        ViewInteraction myEditText = onView(withId(R.id.emailEdit));
        myEditText.perform(scrollTo(), replaceText(user.getUsername()), closeSoftKeyboard());

        ViewInteraction myEditText2 = onView(
                withId(R.id.passwordEdit));
        myEditText2.perform(scrollTo(), replaceText(user.getPassword()), closeSoftKeyboard());

        ViewInteraction myEditText3 = onView(withId(R.id.passwordEdit2));
        myEditText3.perform(scrollTo(), replaceText(user.getPassword()), closeSoftKeyboard());

        RegistreerFragment registreerFragment = getActivity().getFragment(RegistreerFragment.class);

        ViewInteraction appCompatButton5 = onView(
                allOf(withId(R.id.registerButton), withText(R.string.register)));
        appCompatButton5.perform(scrollTo(), click());

        waitUntilLoadingDialogFinished();

       ErrorResult errorResult= registreerFragment.getErrorResult();
        assertNotNull(errorResult);

        //check if a snackbar is displaying the error message
        snackbar(withText(errorResult.getMessage())).check(matches(isDisplayed()));
    }

    @Test
    public void goodRegisterTest() {
        ViewInteraction myEditText = onView(withId(R.id.emailEdit));
        myEditText.perform(scrollTo(), replaceText("user_"+ System.currentTimeMillis()+"@domain.com"), closeSoftKeyboard());

        ViewInteraction myEditText2 = onView(
                withId(R.id.passwordEdit));
        myEditText2.perform(scrollTo(), replaceText("newpassword"), closeSoftKeyboard());

        ViewInteraction myEditText3 = onView(withId(R.id.passwordEdit2));
        myEditText3.perform(scrollTo(), replaceText("newpassword"), closeSoftKeyboard());

        ViewInteraction appCompatButton5 = onView(
                allOf(withId(R.id.registerButton), withText(R.string.register)));
        appCompatButton5.perform(scrollTo(), click());

        //wait until loading is completed
        waitUntilLoadingDialogFinished();

        //check if a snackbar is displaying the victory message
        snackbar(withText(R.string.register_success)).check(matches(isDisplayed()));

        //open the drawer
        openDrawer();
        //is correct menu shown
        TestLoginUtils.checkCorrectDrawerItemsDisplayed(this);
        //log out
        TestLoginUtils.logOut(this);
    }
}

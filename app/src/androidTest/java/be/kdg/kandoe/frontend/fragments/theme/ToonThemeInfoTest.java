package be.kdg.kandoe.frontend.fragments.theme;

import org.junit.*;

import be.kdg.kandoe.base.BaseTest;
import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.content.Theme;
import be.kdg.kandoe.frontend.fragments.ThemeSearchFragment;


import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static org.hamcrest.Matchers.*;

/**
 * Created by Ruben on 11/03/2017.
 */


public class ToonThemeInfoTest extends BaseTest {




    @Before
    public void registerThemeSearchFragmentIdlingResource() {
        //wait for ThemeSearchFragment to load
     waitUntilFragmentLoaded(ThemeSearchFragment.class);
    }

    void performListViewOverflowClick() {
        waitFor(onView(withId(R.id.listView)), matches(allOf(withListViewSize(greaterThan(0)), isDisplayed())));
        onData(instanceOf(Theme.class)).inAdapterView(allOf(withId(R.id.listView), isDisplayed())).atPosition(0).onChildView(withId(R.id.overflowMenu)).perform(click());
    }


    @Test
    public void toonInfoTest() {

        // find the listview and click first list item
        performListViewOverflowClick();

        //find view in popupwindow with text
        onView(withText(R.string.menu_toon_info)).inRoot(isPlatformPopup()).perform(click());
        onView(withId(R.id.tvNaam)).inRoot(isDialog()).check(matches(isDisplayed()));
    }

    @Test
    public void toonKaartjes() {
        toonInfoTest();
        //check if ThemeDetailFragment is being displayed by check if one of it's views are shown

        //click the showCards button that's found in the dialog;
        onView(withId(R.id.showCardsBtn)).perform(click());
        onView(withId(R.id.recyclerview)).check(matches(isDisplayed()));
    }


}

package be.kdg.kandoe.frontend.fragments.session.suite;

import android.support.test.espresso.*;
import android.support.test.espresso.contrib.*;
import android.support.test.filters.*;

import org.junit.*;
import org.junit.runner.*;
import org.junit.runners.*;
import org.junit.runners.Parameterized.*;

import java.io.IOException;
import java.util.List;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.Session.Session;
import be.kdg.kandoe.backend.dom.Session.SessionCard;
import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.base.BaseTest;
import be.kdg.kandoe.frontend.fragments.dialog.MessageDialogFragment;
import be.kdg.kandoe.util.TestSessionConstants;
import be.kdg.kandoe.util.TestSessionUtils;
import be.kdg.kandoe.util.TestLoginUtils;
import java8.util.stream.Collectors;
import java8.util.stream.RefStreams;


import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.isPlatformPopup;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static be.kdg.kandoe.util.TestSessionUtils.checkIfSessionCardsShownCorrectly;
import static be.kdg.kandoe.util.TestSessionUtils.waitForViewContainingString;
import static be.kdg.kandoe.util.azimolabs.conditionwatcher.InstructionFactory.dialogShowing;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.AllOf.allOf;

/**
 * Created by Ruben on 19/03/2017.
 */


@RunWith(Parameterized.class)
@LargeTest
public class PickCardsTest extends BaseTest {

   private final User user;

    @SuppressWarnings("unused")
    private User newUser;

    Session session;

    public PickCardsTest(User user) {
        this.user = user;
    }

    @Before
    public void setup() throws IOException {
        //log him in
        newUser = TestLoginUtils.loginUser(this, user);
        //find the test session
        session = TestSessionUtils.findTestSession(this, true);

        TestSessionUtils.goToTestSessionScreen(this, session);
    }


    void testSessionCardChooser() {
        List<SessionCard> unselectedSessionCards = session.getUnselectedSessionCards();
        //wait for cards to load and display correct amount of items
        ViewInteraction cardChooserRecyclerView = onView(allOf(withId(R.id.recyclerview), withParent(withId(R.id.cardChooser))));
        waitFor(cardChooserRecyclerView, matches(withRecyclerViewSize(equalTo(unselectedSessionCards.size()))));
        //select first item
        cardChooserRecyclerView.perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
        //confirm your selection
        clickOk();

    }

    @Test
    public void pickCardTest() {
        //check if pick cards prompt is shown
        waitFor(dialogShowing(this, MessageDialogFragment.class));
        onView(withText(R.string.prompt_pick_cards)).check(matches(isDisplayed()));

        //confirm you want to choose cards
        clickOk();
        testSessionCardChooser();



        //check if the old sessionCards are displayed correctly
        List<SessionCard> oldSelectedSessionCards = session.getParticipantSessionCards();
        List<SessionCard> newlySelectedSessionCards = RefStreams.of(session.getUnselectedSessionCards().get(0)).collect(Collectors.toList());

       //wait for recyclerView to load
        waitFor(onView(allOf(withId(R.id.recyclerview), withParent(withId(R.id.kandoeBoardRoot)))), matches(withRecyclerViewSize(equalTo(oldSelectedSessionCards.size()+newlySelectedSessionCards.size()))));

        checkIfSessionCardsShownCorrectly(oldSelectedSessionCards);

        //check if the newly sessionCards are displayed correctly

        checkIfSessionCardsShownCorrectly(newlySelectedSessionCards);


        //check if remark dialog can be opened from the recyclerview items;
        onView(allOf(withId(R.id.recyclerview), withParent(withId(R.id.kandoeBoardRoot)))).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
        onView(withText(R.string.show_remarks)).inRoot(isPlatformPopup()).perform(click());
        onView(withId(android.R.id.list)).check(matches(isDisplayed()));

    }


    @Parameters
    public static Iterable<? extends Object> getParticipants() {
        return TestSessionConstants.getParticipants();
    }
}

package be.kdg.kandoe.frontend.fragments.user;


import android.support.test.espresso.*;
import android.support.test.rule.*;

import org.hamcrest.*;
import org.junit.*;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.retrofit.service.mock.user.MockUserRepo;
import be.kdg.kandoe.base.BaseTest;
import be.kdg.kandoe.frontend.activities.MainActivity;
import be.kdg.kandoe.util.TestLoginUtils;
import be.kdg.kandoe.utils.Utils;


import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.assertion.ViewAssertions.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static org.hamcrest.Matchers.*;


public class ProfielTest extends BaseTest {

    private User user = MockUserRepo.getInstance().findUserById(5);

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void loginAndNavigateToProfile() {

        user = TestLoginUtils.loginUser(this, user);

        openDrawer();

        //goto my profile
        navigateToNavigationMenuItem(R.id.menu_mijn_profiel, R.string.menu_mijn_profiel);

    }

    @Test
    public void profielPageTest() {
        ViewInteraction textInputLayout = onView(withId(R.id.voornaamInputLayout));
        textInputLayout.check(matches(isDisplayed()));

        ViewInteraction textInputLayout2 = onView(withId(R.id.achternaamInputLayout));
        textInputLayout2.check(matches(isDisplayed()));

        ViewInteraction textInputLayout3 = onView(withId(R.id.passwordEdit));
        textInputLayout3.check(matches(isDisplayed()));

        ViewInteraction textInputLayout4 = onView(withId(R.id.passwordEdit2));
        textInputLayout4.check(matches(isDisplayed()));

        ViewInteraction button = onView(withId(R.id.saveButton));
        button.check(matches(isDisplayed()));

        if(user.getFirstName()!=null)
            textInputLayout.check(matches(hasDescendant(withText(user.getFirstName()))));

        if(user.getLastName()!=null)
        textInputLayout2.check(matches(hasDescendant(withText(user.getLastName()))));

    }

    @Test
    public void changeFirstname() {
        String newName = Utils.makeNotNull(user.getFirstName())+"R_";
        ViewInteraction myEditText = onView(withId(R.id.voornaamEdit));
        myEditText.perform(scrollTo(), click());
        myEditText.perform(scrollTo(), replaceText(newName), closeSoftKeyboard());

        ViewInteraction appCompatButton3 = onView(
                allOf(withId(R.id.saveButton), withText(R.string.bewaar_profiel)));
        appCompatButton3.perform(scrollTo(), click());

        //wait until profile edit has completed
        waitUntilLoadingDialogFinished();
        //check if success message is displayed
        snackbar(withText(R.string.profile_saved)).check(matches(isDisplayed()));

        //open the drawer
        openDrawer();

        Matcher parentMatcher = isDescendantOfA(withId(R.id.header_container));
        onView(allOf(parentMatcher, withId(R.id.fullname), isDisplayed())).check(matches(withText(startsWith(newName))));
    }
}

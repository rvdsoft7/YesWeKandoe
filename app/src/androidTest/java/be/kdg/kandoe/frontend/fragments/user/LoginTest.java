package be.kdg.kandoe.frontend.fragments.user;


import android.support.test.espresso.*;
import android.support.test.rule.*;

import org.hamcrest.*;
import org.junit.*;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.retrofit.service.util.ErrorResult;
import be.kdg.kandoe.backend.retrofit.service.mock.user.MockUserRepo;
import be.kdg.kandoe.base.BaseTest;
import be.kdg.kandoe.frontend.fragments.LoginFragment;
import be.kdg.kandoe.util.TestLoginUtils;
import be.kdg.kandoe.utils.MyLog;


import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.assertion.ViewAssertions.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static junit.framework.Assert.assertNotNull;
import static org.hamcrest.Matchers.*;


public class LoginTest extends BaseTest {
    private User user = MockUserRepo.getInstance().findUserById(2);

    @Rule
    public UiThreadTestRule uiThreadTestRule = new UiThreadTestRule();


    @Before
    public void navigateToLogin() throws Throwable {
        MyLog.d(getClass(), "navigateToLogin: " + name.getMethodName());
        TestLoginUtils.navigateToLogin(this, true);
    }

    @Test
    public void testLoginPageShowingCorrectly() {
        ViewInteraction textInputLayout = onView(withId(R.id.emailInputLayout));
        textInputLayout.check(matches(isDisplayed()));

        ViewInteraction textInputLayout2 = onView(withId(R.id.passwordInputLayout));
        textInputLayout2.check(matches(isDisplayed()));

        ViewInteraction button = onView(withId(R.id.loginButton));
        button.check(matches(isDisplayed()));

        ViewInteraction button2 = onView(withId(R.id.registerButton));
        button2.check(matches(isDisplayed()));

        ViewInteraction button3 = onView(withId(R.id.loginFacebook));
        button3.check(matches(isDisplayed()));

        ViewInteraction button4 = onView(withId(R.id.loginGoogle));
        button4.check(matches(isDisplayed()));
    }

    @Test
    public void testNoEmail() {
        ViewInteraction myEditText = onView(withId(R.id.emailEdit));
        myEditText.perform(click());
        myEditText.perform(replaceText(""), closeSoftKeyboard());

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.loginButton), withText(R.string.login)));
        appCompatButton.perform(click());

        ViewInteraction textView = onView(withId(R.id.textinput_error));
        textView.check(matches(isDisplayed()));
        textView.check(matches(withText(R.string.validation_email)));
    }

    @Test
    public void testNoPassword() {
        ViewInteraction myEditText5 = onView(withId(R.id.passwordEdit));
        myEditText5.perform(click());
        myEditText5.perform(replaceText(""), closeSoftKeyboard());

        ViewInteraction myEditText8 = onView(withId(R.id.emailEdit));
        myEditText8.perform(replaceText("user@example.com"), closeSoftKeyboard());

        ViewInteraction appCompatButton5 = onView(
                allOf(withId(R.id.loginButton), withText(R.string.login)));
        appCompatButton5.perform(click());

        ViewInteraction textView = onView(withId(R.id.textinput_error));
        textView.check(matches(isDisplayed()));
        textView.check(matches(withText(R.string.validation_password)));
    }

    @Test
    public void testWrongCredentials() {

        ViewInteraction myEditText = onView(withId(R.id.emailEdit));
        myEditText.perform(replaceText("user@example.com"), closeSoftKeyboard());

        ViewInteraction myEditText2 = onView(withId(R.id.passwordEdit));
        myEditText2.perform(replaceText("User@1234567"), closeSoftKeyboard());
        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.loginButton), withText(R.string.login)));
        LoginFragment loginFragment = (LoginFragment) getActivity().getCurrentFragment();
        assertNotNull(loginFragment);
        appCompatButton.perform(click());
        //find the loginFragment instance attached to the activity

        ErrorResult errorResult = loginFragment.getErrorResult();
        //wait until login processing has completed
        waitUntilLoadingDialogFinished();
        //get the error result
        assertNotNull(errorResult);
        //check if a snackbar is displaying the error message
        snackbar(withText(errorResult.getMessage())).check(matches(isDisplayed()));
    }

    @Test
    public void testRightCredentials() {
        ViewInteraction myEditText = onView(withId(R.id.emailEdit));
        myEditText.perform(replaceText(user.getUsername()), closeSoftKeyboard());

        ViewInteraction myEditText2 = onView(withId(R.id.passwordEdit));
        myEditText2.perform(replaceText(user.getPassword()), closeSoftKeyboard());

        ViewInteraction appCompatButton7 = onView(
                allOf(withId(R.id.loginButton), withText(R.string.login)));
        appCompatButton7.perform(click());

        //wait until login processing has completed
        waitUntilLoadingDialogFinished();
        //check if success message is displayed
        snackbar(withText(R.string.login_success)).check(matches(isDisplayed()));

        //open the drawer
        openDrawer();

        TestLoginUtils.checkCorrectDrawerItemsDisplayed(this);

        //check if correct user info is being displayed in header
        Matcher parentMatcher = isDescendantOfA(withId(R.id.header_container));
        onView(allOf(parentMatcher, withId(R.id.avatar), isDisplayed()));
        onView(allOf(parentMatcher, withId(R.id.fullname), isDisplayed())).check(matches(withText(user.getFullName())));
        onView(allOf(parentMatcher, withId(R.id.email), isDisplayed())).check(matches(withText(user.getUsername())));
    }
}

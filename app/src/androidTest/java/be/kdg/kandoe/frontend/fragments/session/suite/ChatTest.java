package be.kdg.kandoe.frontend.fragments.session.suite;

import android.support.test.espresso.matcher.*;
import android.support.test.filters.*;
import android.support.test.internal.util.*;

import org.hamcrest.Description;
import org.hamcrest.*;
import org.junit.*;
import org.junit.runner.*;
import org.junit.runners.*;
import org.junit.runners.Parameterized.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.Session.Session;
import be.kdg.kandoe.backend.dom.content.Message;
import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.retrofit.service.factory.ServiceGenerator;
import be.kdg.kandoe.base.BaseTest;
import be.kdg.kandoe.util.TestSessionConstants;
import be.kdg.kandoe.util.TestSessionUtils;
import be.kdg.kandoe.util.TestLoginUtils;
import be.kdg.kandoe.utils.Utils;
import java8.util.stream.StreamSupport;


import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.assertion.ViewAssertions.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.AllOf.allOf;
import static org.junit.Assert.*;

/**
 * Created by Ruben on 19/03/2017.
 */


@RunWith(Parameterized.class)
@LargeTest
public class ChatTest extends BaseTest {

    private final User user;

    private final String message;

    private final int correctSize;

    private User newUser;

    Session session;

    List<Message> chat;

    public ChatTest(User user, String message, int correctSize) {
        this.user = user;
        this.message = message;
        this.correctSize = correctSize;
    }

    @Before
    public void setup() throws IOException {
        //log him in
        newUser = TestLoginUtils.loginUser(this, user);
        //find the test session
        session = TestSessionUtils.findTestSession(this, true);

        TestSessionUtils.goToTestSessionScreen(this, session);

        chat = executeCall(ServiceGenerator.getInstance().createSessionService().getChat(session.getId()));
        assertNotNull(chat);
        assertEquals("chat must be size="+correctSize, correctSize, chat.size());
    }



    @Test
    public void chatTest() {
        assertTrue("message must not be empty", !Utils.isEmpty(message));
        boolean containsMessage = StreamSupport.stream(chat).anyMatch(m->m.getText().equals(message));

        assertTrue("chat must not contain message: "+message, !containsMessage);

        // open chat icon
        onView(withId(R.id.menu_chat)).perform(click());
        //wait for chat to load
        waitFor(onView(withId(R.id.listView)), matches(withListViewSize(equalTo(chat.size()))));
        //check if all chat is displayed
        validateMessages(chat);
        //send the new message
        onView(withId(R.id.messageEdit)).perform(click(), typeText(message));
        onView(withId(R.id.sendBtn)).perform(click());
        //wait until listView is loaded
        waitFor(onView(withId(R.id.listView)), matches(withListViewSize(equalTo(chat.size()+1))));
        //check if newMessage isDisplayed
        checkMessageText(message, newUser);
    }

    void validateMessages(List<Message> chat) {
        StreamSupport.stream(chat).forEach(m->{
                checkMessageText(m.getText(), m.getUser());
        });
    }

    void checkMessageText(String message, User user) {
        //check adapter data
        onData(allOf(instanceOf(Message.class), withListItem(equalTo(message), equalTo(user)))).inAdapterView(withId(R.id.listView)).atPosition(0).check(matches(isDisplayed()));
        //check if really displayed
        onView(allOf(isDescendantOfA(withId(R.id.listView)), hasDescendant(withText(containsString(message))), hasDescendant(withText(containsString(user.getShortName()))))).check(matches(isDisplayed()));
    }


    public static List<String> getMessages() {
        return Arrays.asList("Are we living in a fake universe?", "Hmmm, I don't think so");
    }

    @Parameters
    public static Iterable<Object[]> data() {
        List<Object[]> list = new ArrayList<>();
        List<String> messages = getMessages();
        List<User> participants = getParticipants();
        for (int i = 0; i < messages.size(); i++) {
            Object[] array = new Object[3];
            array[0] = participants.get(i);
            array[1] = messages.get(i);
            array[2] = i;
            list.add(array);
        }
        return list;
    }


    public static List<User> getParticipants() {
        return TestSessionConstants.getParticipants();
    }

    public static Matcher<Object> withListItem(final Matcher<String> itemText, final Matcher<User> user) {
        Checks.checkNotNull(itemText);
        Checks.checkNotNull(user);
        return new BoundedMatcher<Object, Message>(Message.class) {
            @Override
            public boolean matchesSafely(Message item) {
                return itemText.matches(item.getText()) && user.matches(item.getUser());
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("with text: " + itemText.toString()+ " and with user: "+user.toString());
                itemText.describeTo(description);
            }
        };
    }
}

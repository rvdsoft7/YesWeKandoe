package be.kdg.kandoe.frontend.fragments.session.suite;

import android.support.test.espresso.*;
import android.support.test.espresso.contrib.*;

import org.junit.*;

import java.io.IOException;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.Session.Session;
import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.retrofit.service.mock.user.MockUserRepo;
import be.kdg.kandoe.base.BaseTest;
import be.kdg.kandoe.util.TestSessionUtils;
import be.kdg.kandoe.util.TestLoginUtils;
import java8.util.stream.StreamSupport;


import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.core.AllOf.allOf;

/**
 * Created by Ruben on 18/03/2017.
 */

public class TestKandoeBoardFragmentMenuItems extends BaseTest {

    Session session;
    User user;

    static final String REMARK_TEST = "This card is ridiculous!";
    @Before
    public void setup() throws IOException {
        //find the organizer of the created session
        user = MockUserRepo.getInstance().findAdmin();
        //log him in
       user= TestLoginUtils.loginUser(this, user);
        //find the test session
        session = TestSessionUtils.findTestSession(this, true);

        // now we go to myThemes

        TestSessionUtils.goToTestSessionScreen(this, session);

    }

    @Test
    public void showParticipantsTest() {
        openOverFlowItem(R.string.menu_toon_deelnemers);
        //loop through the participants and check for each whether they're displayed or not
        StreamSupport.stream(session.getUsers()).forEach(u->{
            onData(allOf(instanceOf(User.class), withEntry(equalTo(u),User.class))).inAdapterView(withId(android.R.id.list)).atPosition(0).check(matches(isDisplayed()));
        });
    }

    @Test
    public void showRemarksAndAddRemarkTest() {
        //open session info
      openOverFlowItem(R.string.menu_toon_info);

        //open card overview fragment
        onView(withId(R.id.showCardsBtn)).perform(click());
        //wait for cards to load
        ViewInteraction viewInteraction = onView(allOf(withId(R.id.recyclerview), withParent(withId(R.id.cardOverview))));
        waitFor(viewInteraction, matches(withRecyclerViewSize(greaterThan(0))));
        //click on first item
        viewInteraction.perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        //click on add remark button
        onView(withId(R.id.add)).perform(click());

        //enter the remark and click ok
        onView(withId(R.id.remarkEdit)).perform(replaceText(REMARK_TEST));
        onView(withText(android.R.string.ok)).perform(click());

        //wait until the added remark is shown
        waitFor(onView(withId(android.R.id.list)), matches(withListViewSize(greaterThan(0))));

        //check if remark is displayed
        onView(withText(REMARK_TEST)).check(matches(isDisplayed()));
    }
}

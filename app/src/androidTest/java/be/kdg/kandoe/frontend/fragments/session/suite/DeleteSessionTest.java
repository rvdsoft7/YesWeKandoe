package be.kdg.kandoe.frontend.fragments.session.suite;



import org.junit.*;

import java.io.IOException;

import be.kdg.kandoe.backend.dom.Session.Session;
import be.kdg.kandoe.backend.retrofit.service.factory.ServiceGenerator;
import be.kdg.kandoe.base.BaseTest;
import be.kdg.kandoe.util.TestSessionUtils;

/**
 * Created by Ruben on 18/03/2017.
 */

public class DeleteSessionTest extends BaseTest{

    @Test
    public void tearDown() throws IOException {
        Session session = TestSessionUtils.findTestSession(this, true);
        executeCall(ServiceGenerator.getInstance().createSessionService().deleteSession(session.getId()));
    }

}

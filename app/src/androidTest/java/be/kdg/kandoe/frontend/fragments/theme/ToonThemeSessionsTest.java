package be.kdg.kandoe.frontend.fragments.theme;

import android.support.test.espresso.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.Session.Session;
import be.kdg.kandoe.backend.dom.content.Theme;
import be.kdg.kandoe.base.BaseTest;
import be.kdg.kandoe.frontend.fragments.ThemeSearchFragment;


import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.instanceOf;

/**
 * Created by Kevin on 13/03/2017.
 */

public class ToonThemeSessionsTest extends BaseTest {



    @Before
    public void registerThemeSearchFragmentIdlingResource() {
        //wait for ThemeSearchFragment to load
        waitUntilFragmentLoaded(ThemeSearchFragment.class);
    }

    void performListViewClick(Class<?> clazz, boolean overflow) {
        waitFor(onView(withId(R.id.listView)), matches(allOf(withListViewSize(greaterThan(0)), isDisplayed())));
        DataInteraction dataInteraction = onData(instanceOf(clazz)).inAdapterView(allOf(withId(R.id.listView), isDisplayed())).atPosition(0);
        if(overflow) {
            dataInteraction =  dataInteraction.onChildView(withId(R.id.overflowMenu));
        }
       dataInteraction.perform(click());
    }


    @Test
    public void toonSessionsTest() {
        // find the listview and click first list item
        performListViewClick(Theme.class, false);

        // wait for sessionList
        waitFor(onView(withId(R.id.listView)), matches(allOf(withListViewSize(greaterThan(0)), isDisplayed())));
    }

    /*@Test
    public void toonSessionInfo() {
        toonSessionsTest();

        // find the listview and click first list item
        performListViewClick(Session.class, true);
        //find view in popupwindow with text
        onView(withText(R.string.menu_toon_info)).inRoot(isPlatformPopup()).perform(click());
        onView(withId(R.id.tvNaam)).inRoot(isDialog()).check(matches(isDisplayed()));
    }

    @Test
    public void toonKaartjes() {
        toonSessionInfo();

        //click the showCards button that's found in the dialog;
        onView(withId(R.id.showCardsBtn)).perform(click());
        onView(withId(R.id.recyclerview)).check(matches(isDisplayed()));
    }

    @Test
    public void toonDetails(){
        toonSessionInfo();

        //click the details... button that's found in the dialog;
        onView(withId(R.id.themeButton)).perform(scrollTo(), click());
        onView(withId(R.id.tvNaam)).inRoot(isDialog()).check(matches(isDisplayed()));

    }*/

    @Test
    public void toonPlay(){
        toonSessionsTest();

        // find the listview and click first list item
        performListViewClick(Session.class, false);
        //wait for right view to be displayed
        waitFor(onView(withId(R.id.kandoeBoard)), matches(isDisplayed()));
    }
}
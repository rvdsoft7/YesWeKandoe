package be.kdg.kandoe.frontend.fragments.session.suite;

import org.junit.*;

import java.io.IOException;
import java.util.List;

import be.kdg.kandoe.backend.dom.Session.Session;
import be.kdg.kandoe.backend.dom.content.Theme;
import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.retrofit.service.factory.ServiceGenerator;
import be.kdg.kandoe.backend.retrofit.service.mock.session.DefaultSession;
import be.kdg.kandoe.backend.retrofit.service.mock.user.MockUserRepo;
import be.kdg.kandoe.backend.retrofit.service.api.session.SessionService;
import be.kdg.kandoe.backend.retrofit.service.api.theme.ThemeService;
import be.kdg.kandoe.base.BaseTest;
import be.kdg.kandoe.util.TestSessionConstants;
import be.kdg.kandoe.util.TestSessionUtils;
import be.kdg.kandoe.util.TestLoginUtils;
import be.kdg.kandoe.utils.Utils;


import static org.junit.Assert.*;

/**
 * Created by Ruben on 18/03/2017.
 * <p>
 *  end to end test
 */
public class CreateSessionTest extends BaseTest {

    MockUserRepo userRepo = MockUserRepo.getInstance();
    User admin = userRepo.findAdmin();

    ThemeService getThemeService() {
        return ServiceGenerator.getInstance().createThemeSevice();
    }

    SessionService getSessionService() {
        return ServiceGenerator.getInstance().createSessionService();
    }



    @Test
    public void setup()  throws IOException{
      admin =  TestLoginUtils.loginUser(this, admin);
       Session s = TestSessionUtils.findTestSession(this, false);

        if(s!=null) {
            //delete the session
           executeCall(getSessionService().deleteSession(s.getId()));
        }
        //find a theme
        List<Theme> themes = executeCall(getThemeService().getThemesByOrganiser(admin.getId()));
        assertTrue("themes list must not be empty", !Utils.isEmpty(themes));
        Theme theme = themes.get(0);

        //make sure we have the correct user id in our mock repo
        User user2 = executeCall(ServiceGenerator.getInstance().createUserService().findUserByUsername(MockUserRepo.getInstance().findUserById(2).getUsername()));

        //create a session for it
        s = DefaultSession.createSession(theme, 654657);

        //init session
        s.setUsers(admin, user2);
        s.setOrganiser(admin);
        s.setCurrentUser(null);
        s.setStartDate(null);
        s.setEndDate(null);
        s.setName(TestSessionConstants.SESSION_NAME);

        //save the session
       executeCall(getSessionService().createSession(s));

        //now we can play the created session;
    }

}

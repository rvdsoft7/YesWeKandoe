package be.kdg.kandoe.frontend.fragments.session;

import org.junit.runner.*;
import org.junit.runners.*;

import be.kdg.kandoe.frontend.fragments.session.suite.ChatTest;
import be.kdg.kandoe.frontend.fragments.session.suite.CreateSessionTest;
import be.kdg.kandoe.frontend.fragments.session.suite.DeleteSessionTest;
import be.kdg.kandoe.frontend.fragments.session.suite.EndSessionTest;
import be.kdg.kandoe.frontend.fragments.session.suite.PickCardsTest;
import be.kdg.kandoe.frontend.fragments.session.suite.PlayGameTest;
import be.kdg.kandoe.frontend.fragments.session.suite.StartSessionTest;
import be.kdg.kandoe.frontend.fragments.session.suite.TestKandoeBoardFragmentMenuItems;

/**
 * Created by Ruben on 18/03/2017.
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({
        CreateSessionTest.class,
        ChatTest.class,
        TestKandoeBoardFragmentMenuItems.class,
        StartSessionTest.class,
        PickCardsTest.class,
        PlayGameTest.class,
        EndSessionTest.class,
        DeleteSessionTest.class,
})
public class SessionSuiteTest {
}

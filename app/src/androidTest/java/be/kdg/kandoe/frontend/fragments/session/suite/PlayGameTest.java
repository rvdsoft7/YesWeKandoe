package be.kdg.kandoe.frontend.fragments.session.suite;

import android.support.test.espresso.*;
import android.support.test.espresso.contrib.*;
import android.support.test.filters.*;

import org.junit.*;
import org.junit.runner.*;
import org.junit.runners.*;
import org.junit.runners.Parameterized.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.Session.Session;
import be.kdg.kandoe.backend.dom.Session.SessionCard;
import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.base.BaseTest;
import be.kdg.kandoe.frontend.fragments.dialog.MessageDialogFragment;
import be.kdg.kandoe.util.TestSessionConstants;
import be.kdg.kandoe.util.TestSessionUtils;
import be.kdg.kandoe.util.TestLoginUtils;
import be.kdg.kandoe.util.matchers.CustomViewHolderMatcher;
import java8.util.stream.StreamSupport;


import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.isPlatformPopup;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static be.kdg.kandoe.util.matchers.KandoeBoardMatchers.withLevel;
import static be.kdg.kandoe.util.matchers.KandoeBoardMatchers.withSessionCard;
import static be.kdg.kandoe.util.TestSessionUtils.checkIfSessionCardsShownCorrectly;
import static be.kdg.kandoe.util.TestSessionUtils.waitForViewContainingString;
import static be.kdg.kandoe.util.azimolabs.conditionwatcher.InstructionFactory.dialogShowing;
import static junit.framework.Assert.assertNotNull;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.AllOf.allOf;

/**
 * Created by Ruben on 19/03/2017.
 */

@RunWith(Parameterized.class)
@LargeTest
public class PlayGameTest  extends BaseTest{
    private static final int ROUNDS = 3;

    private final User user;
    @SuppressWarnings("unused")
    private User newUser;

    Session session;


    public PlayGameTest(User user) {
        this.user = user;
    }


    @Before
    public void setup() throws IOException {
        //log him in
        newUser = TestLoginUtils.loginUser(this, user);
        //find the test session
        session = TestSessionUtils.findTestSession(this, true);

        TestSessionUtils.goToTestSessionScreen(this, session);
    }

    @Test
    public void playGameTest() {
        //check if do move prompt is shown
        waitFor(dialogShowing(this, MessageDialogFragment.class));
        onView(withText(R.string.prompt_do_move)).check(matches(isDisplayed()));
       //close dialog
        clickOk();
        //check round indicator
        waitForViewContainingString(this, R.id.roundTv, String.valueOf(session.getCurrentRound()));

        List<SessionCard> selectedSessionCards = session.getParticipantSessionCards();
        //check correct recyclerview count
        ViewInteraction recyclerView = onView(allOf(withId(R.id.recyclerview), withParent(withId(R.id.kandoeBoardRoot))));
        waitFor(recyclerView, matches(withRecyclerViewSize(equalTo(selectedSessionCards.size()))));

        checkIfSessionCardsShownCorrectly(selectedSessionCards);

        SessionCard sessionCardToMoveUp = StreamSupport.stream(selectedSessionCards).filter(sc->sc.canMoveUp()).findFirst().orElse(null);
        assertNotNull("need a card to move up", sessionCardToMoveUp);
        //click the sessionCardToMoveUp
        recyclerView.perform(RecyclerViewActions.actionOnHolderItem(
                new CustomViewHolderMatcher(hasDescendant(allOf(withId(R.id.cardText), withText(sessionCardToMoveUp.getText())))),
                click()
        ));
        //do move
        onView(withText(R.string.do_move)).inRoot(isPlatformPopup()).perform(click());

        //sync level
        sessionCardToMoveUp.incrementLevel();

        //wait for kandoeBoard to update layout changes
        waitFor(onView(allOf(isDescendantOfA(withId(R.id.kandoeBoard)),
                withSessionCard(equalTo(sessionCardToMoveUp)))), matches(allOf(isDisplayed(),withLevel(equalTo(sessionCardToMoveUp.getLevel())))));

    }



    @Parameters
    public static Iterable<? extends Object> getParticipantsTimesX(){
        List<Object> objectList = new ArrayList<>();
        for (int i = 0; i < ROUNDS; i++) {
            objectList.addAll(getParticipants());
        }
        return objectList;
    }

    public static List<? extends Object> getParticipants() {
        return TestSessionConstants.getParticipants();
    }
}

package be.kdg.kandoe.frontend.fragments.session.suite;

import org.junit.*;

import java.io.IOException;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.Session.Session;
import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.retrofit.service.factory.ServiceGenerator;
import be.kdg.kandoe.backend.retrofit.service.mock.user.MockUserRepo;
import be.kdg.kandoe.base.BaseTest;
import be.kdg.kandoe.util.TestSessionUtils;
import be.kdg.kandoe.util.TestLoginUtils;


import static be.kdg.kandoe.util.TestSessionUtils.waitForViewContainingString;

/**
 * Created by Ruben on 19/03/2017.
 */

public class EndSessionTest extends BaseTest {

    Session session;
    User user;

    @Before
    public void setup() throws IOException {
        //find the organizer of the created session
        user = MockUserRepo.getInstance().findAdmin();
        //log him in
        user= TestLoginUtils.loginUser(this, user);
        //find the test session
        session = TestSessionUtils.findTestSession(this, true);

        TestSessionUtils.goToTestSessionScreen(this, session);

    }

    @Test
    public void endSessionTest() throws IOException {
        clickCancel();
        //begin the session
        executeCall(ServiceGenerator.getInstance().createSessionService().endSession(session.getId()));
        //websocket test

        //Wait until indicator is updated
        waitForViewContainingString(this, R.id.statusTv, R.string.session_status_game_over);

    }

}

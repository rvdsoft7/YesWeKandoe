package be.kdg.kandoe.frontend.fragments.session.suite;

import org.junit.*;

import java.io.IOException;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.Session.Session;
import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.retrofit.service.factory.ServiceGenerator;
import be.kdg.kandoe.backend.retrofit.service.mock.user.MockUserRepo;
import be.kdg.kandoe.base.BaseTest;
import be.kdg.kandoe.frontend.fragments.dialog.MessageDialogFragment;
import be.kdg.kandoe.util.TestSessionUtils;
import be.kdg.kandoe.util.TestLoginUtils;


import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static be.kdg.kandoe.util.TestSessionUtils.waitForViewContainingString;
import static be.kdg.kandoe.util.azimolabs.conditionwatcher.InstructionFactory.dialogShowing;

/**
 * Created by Ruben on 19/03/2017.
 */

public class StartSessionTest extends BaseTest {

    Session session;
    User user;

    @Before
    public void setup() throws IOException {
        //find the organizer of the created session
        user = MockUserRepo.getInstance().findAdmin();
        //log him in
        user= TestLoginUtils.loginUser(this, user);
        //find the test session
        session = TestSessionUtils.findTestSession(this, true);

        TestSessionUtils.goToTestSessionScreen(this, session);

    }

    @Test
    public void beginSessionTest() throws IOException {


        //begin the session
        session = executeCall(ServiceGenerator.getInstance().createSessionService().beginSession(session.getId()));

        //websocket tests:
        //check if pick cards prompt is shown for the first user (the admin)
        waitFor(dialogShowing(this, MessageDialogFragment.class));
        onView(withText(R.string.prompt_pick_cards)).check(matches(isDisplayed()));
        //we'll pick cards later
        clickCancel();
        //Wait until indicator is updated
        waitForViewContainingString(this, R.id.statusTv, R.string.session_status_picking_cards);

    }

}

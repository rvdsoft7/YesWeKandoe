package be.kdg.kandoe.backend.retrofit;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

import be.kdg.kandoe.BaseTest;
import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.retrofit.service.factory.ServiceGenerator;
import be.kdg.kandoe.backend.retrofit.service.mock.user.MockUserRepo;
import be.kdg.kandoe.backend.retrofit.service.api.user.UserService;
import retrofit2.Response;

/**
 * Created by Ruben on 22/02/2017.
 * kan verkeerd type zijn indien niet correct gemocked
 */

public class TestLoginType extends BaseTest{

    @Test
    public void testLogin() throws IOException {
        UserService userService = ServiceGenerator.getInstance().createUserService();
        Response<User> response =userService.basicLogin(MockUserRepo.getInstance().findUserById(2)).execute();
        Assert.assertThat(response.body(), CoreMatchers.instanceOf(User.class));
    }
}

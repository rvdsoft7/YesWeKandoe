package be.kdg.kandoe.backend.retrofit;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.io.IOException;

import be.kdg.kandoe.BuildConfig;
import be.kdg.kandoe.MyApplication;
import be.kdg.kandoe.BaseTest;
import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.retrofit.service.api.user.UserService;
import be.kdg.kandoe.backend.retrofit.service.util.CustomRetrofitBuilder;
import be.kdg.kandoe.utils.IOUtils;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Ruben on 17/02/2017.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, application = MyApplication.class)
public class TestRegister extends BaseTest {

    User inputUser;
    private Retrofit retrofit;
    MockWebServer mockWebServer;

    @Before
    public void setup(){
        super.setup();
        mockWebServer = new MockWebServer();
        inputUser = new User();
        inputUser.setUsername("user@example.com");
        inputUser.setPassword("password");
        CustomRetrofitBuilder builder = new CustomRetrofitBuilder();
        retrofit = builder.baseUrl(mockWebServer.url("").toString()).build();
    }

    @Test
    public void testRegisterResponse() throws IOException {
        String output = IOUtils.getTextFromResources("register_response.json");
        Assert.assertNotNull(output);
        mockWebServer.enqueue(new MockResponse().setBody(output));
        UserService registerService = retrofit.create(UserService.class);
        Call<User> userCall = registerService.basicRegister(inputUser);
        Response<User> userResponse = userCall.execute();
        User u = userResponse.body();
        Assert.assertTrue(userResponse.isSuccessful());
        Assert.assertEquals(inputUser.getUsername(), u.getUsername());
    }

    @After
    public void shutdown() throws IOException {
        mockWebServer.shutdown();
    }
}

package be.kdg.kandoe.backend.retrofit;



import org.junit.Assert;
import org.junit.Test;

import be.kdg.kandoe.backend.dom.Session.SessionCard;
import be.kdg.kandoe.backend.retrofit.service.util.RetrofitHelper;
import be.kdg.kandoe.utils.IOUtils;
import be.kdg.kandoe.utils.MyLog;

/**
 * Created by Ruben on 12/03/2017.
 */


public class TestSessionCard {

    @Test
    public void testJson() {
       SessionCard sessionCard = RetrofitHelper.readValue(IOUtils.getTextFromResources("session_card.json"), SessionCard.class);
       String data= RetrofitHelper.writeValueAsString(sessionCard);
        MyLog.d(getClass(),"data:"+ data);
        SessionCard converted = RetrofitHelper.readValue(data, SessionCard.class);
        String data2 = RetrofitHelper.writeValueAsString(converted);
        MyLog.d(getClass(),"data2: "+data2);
         Assert.assertEquals(1, sessionCard.getLevel());
        Assert.assertEquals(3, sessionCard.getDistanceToCenter());
    }
}

package be.kdg.kandoe.backend.retrofit;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import be.kdg.kandoe.BaseTest;
import be.kdg.kandoe.backend.dom.content.Theme;
import be.kdg.kandoe.backend.retrofit.service.factory.ServiceGenerator;
import be.kdg.kandoe.backend.retrofit.service.api.theme.ThemeService;
import be.kdg.kandoe.backend.retrofit.service.util.RetrofitHelper;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Ruben on 17/02/2017.
 */


public class TestThemes extends BaseTest {


    @Before
    public void setup()  {
        super.setup();
    }




    @Test
    public void testThemeService() throws IOException {
       ThemeService themeService = ServiceGenerator.getInstance().createThemeSevice();
       Call<List<Theme>> themeCall =  themeService.themes();
        Response<List<Theme>> themeResponse = themeCall.execute();
       assertTrue(themeResponse.isSuccessful());
        List<Theme> list = themeResponse.body();
        assertNotNull(list);
       String dataList= RetrofitHelper.writeValueAsStringPretty(list);
        assertNotNull(dataList);
       List<Theme> listConverted = RetrofitHelper.readListValue(dataList, Theme.class);
        assertNotNull(listConverted);
        String dataListConverted = RetrofitHelper.writeValueAsStringPretty(listConverted);
        assertEquals("if themes json is converted correctly then dataList==dataListConverted", dataList, dataListConverted);
    }


}

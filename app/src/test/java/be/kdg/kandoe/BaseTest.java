package be.kdg.kandoe;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowLog;

/**
 * Created by Ruben on 19/02/2017.
 */


public class BaseTest {
    static {
        //ShadowLog.stream = System.out;
        MyApplication.setMockMode(true);
        MyApplication.setTestingMode(true);
    }

    @Before
    public void setup() {

    }
}

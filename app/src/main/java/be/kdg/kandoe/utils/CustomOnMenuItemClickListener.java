package be.kdg.kandoe.utils;

import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;

public abstract class CustomOnMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

	private final int pos;
	private final long id;
	
	
	public CustomOnMenuItemClickListener(int pos, long id) {
		super();
		this.pos = pos;
		this.id = id;
	}

	public abstract boolean onMenuItemClick(MenuItem item, int pos, long id);

	@Override
	public final boolean onMenuItemClick(MenuItem item) {
		return onMenuItemClick(item, pos, id);
	}


	@SuppressWarnings("unused")
    public int getPosition() {
		return pos;
	}


	@SuppressWarnings("unused")
    public long getId() {
		return id;
	}

}
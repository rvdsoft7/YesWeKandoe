package be.kdg.kandoe.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;



import java.util.List;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.content.Tag;
import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.frontend.views.UserView;
import java8.util.stream.StreamSupport;


public class FlowLayoutHelper {


    private ViewGroup viewGroup;
    private LayoutInflater inflater;
    private Context context;
    private Resources res;

    public FlowLayoutHelper(ViewGroup viewGroup) {
        super();
        this.viewGroup = viewGroup;
        this.context = viewGroup.getContext();
        inflater = LayoutInflater.from(viewGroup.getContext());
        res = context.getResources();
    }


    public void setUsers(List<User> users, List<Long> organisators, long onTurn) {
        viewGroup.removeAllViews();
        viewGroup.invalidate();
        for (User user : users) {
            inflater.inflate(R.layout.userview_default, viewGroup, true);
            UserView userv = (UserView) viewGroup.getChildAt(viewGroup.getChildCount() - 1);
            if(!Utils.isEmpty(organisators)) {
                if (StreamSupport.stream(organisators).anyMatch(id->id==user.getId())) {
                    userv.setOrganistor(true);
                }
            }

            if(user.getId()==onTurn) {
                userv.setIsOnTurn(true);
            }
            initUserView(userv, user);
        }
    }


    public void setTags(List<Tag> tags) {
        viewGroup.removeAllViews();
        viewGroup.invalidate();
        for (Tag tag : tags) {
            inflater.inflate(R.layout.tag_textview, viewGroup, true);
            TextView tv = (TextView) viewGroup.getChildAt(viewGroup.getChildCount() - 1);
            initTagTv(tv, tag);
        }
    }

    private int fetchPrimaryColor() {
        TypedValue typedValue = new TypedValue();

        TypedArray a = context.obtainStyledAttributes(typedValue.data, new int[] { R.attr.colorPrimary });
        int color = a.getColor(0, 0);

        a.recycle();

        return color;
    }

    private void initUserView(UserView userView, User user) {
        userView.setUser(user);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @SuppressWarnings("deprecation")
    private void initTagTv(TextView tv, Tag tag) {
        LayerDrawable layerDrawable = (LayerDrawable) res.getDrawable(R.drawable.tag_background);
        GradientDrawable drawable = (GradientDrawable) layerDrawable.findDrawableByLayerId(R.id.tagbg);
        int bg = fetchPrimaryColor();
        int fg = res.getColor(R.color.tag_foreground_color);

        int background = bg;
        drawable.setColor(Utils.changeAlpha(background, 255));
        if (APIUtils.isCompatWithApiLevel16()) {
            tv.setBackground(drawable);
        } else
            tv.setBackgroundDrawable(drawable);

        tv.setText(tag.getName());
        int foreground = fg;

        tv.setTextColor(Utils.changeAlpha(foreground, 255));
    }


}

package be.kdg.kandoe.utils;


import android.text.TextUtils;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import be.kdg.kandoe.MyApplication;

public class StringUtils {


	private StringUtils() {
	}

    public static final String EMAIL_PATTERN = android.util.Patterns.EMAIL_ADDRESS.pattern();

	public static String toHexString(int color) {
		return String.format("#%06X", (0xFFFFFF & color));
	}


	/**
	 * format a string with english locale
	 * 
	 * @param format
	 * @param args
	 */
	public static String format(String format, Object... args) {
		return String.format(Locale.US, format, args);
	}

}

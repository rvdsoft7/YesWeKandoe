package be.kdg.kandoe.utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

/**
 * Created by Ruben on 19/03/2017.
 */

public class CallHelper {
    List<Call<?>> calls = new ArrayList<>();

    public CallHelper() {

    }

    public void add(Call<?> call) {
        calls.add(call);
    }

    /**
     *
     * @return if one or more calls have been canceled
     */
    public boolean cancelAll() {
        MyLog.d(getClass(), "cancelAll");
        boolean cancelled= false;
        for (Call<?> call : calls) {
            if (call != null && !call.isCanceled() && call.isExecuted()) {
                call.cancel();
                cancelled = true;
            }
        }
        calls.clear();
        return cancelled;
    }


}

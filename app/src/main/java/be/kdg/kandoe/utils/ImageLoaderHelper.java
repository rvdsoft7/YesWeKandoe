package be.kdg.kandoe.utils;


import android.graphics.Bitmap.Config;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.ImageSize;

import be.kdg.kandoe.R;

public class ImageLoaderHelper {

	public static final ImageSize DEFAULT_TARGET_SIZE = new ImageSize(500, 500);

	

	public static void displayImage(String uri, ImageView imageView) {
		displayImage(uri, imageView, DEFAULT_TARGET_SIZE);
	}

	public static void displayImage(String uri, ImageView imageView, ImageSize targetSize) {
		DisplayImageOptions options = createOptionsBuilder(targetSize).build();
		ImageLoader.getInstance().displayImage(uri, imageView, options);
	}



	static DisplayImageOptions.Builder createOptionsBuilder(ImageSize targetSize) {
		return new DisplayImageOptions.Builder().bitmapConfig(Config.RGB_565).cacheInMemory(true).cacheOnDisk(true)
				.targetSize(targetSize).imageScaleType(ImageScaleType.EXACTLY)
				.showImageOnLoading(R.drawable.ic_launcher)
				.showImageForEmptyUri(R.drawable.ic_launcher).showImageOnFail(R.drawable.ic_clear);
	}


}

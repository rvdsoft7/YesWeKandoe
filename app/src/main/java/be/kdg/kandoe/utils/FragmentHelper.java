package be.kdg.kandoe.utils;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import be.kdg.kandoe.frontend.activities.BaseActivity;
import be.kdg.kandoe.R;
import be.kdg.kandoe.frontend.fragments.BaseFragment;

@SuppressWarnings("unused")
public class FragmentHelper {

	FragmentManager fragMgr;
	String tag;
	int id = R.id.content_frame;
	BaseActivity baseActivity;

	public FragmentHelper(BaseFragment context) {
		baseActivity = context.getBaseActivity();
		fragMgr = context.getFragmentManager();
	}

	public FragmentHelper(BaseActivity context) {
		baseActivity = context;
		fragMgr = context.getSupportFragmentManager();
	}

	public FragmentHelper id(int id) {
		this.id = id;
		return this;
	}

	public FragmentHelper tag(String tag) {
		this.tag = tag;
		return this;
	}

	public void destory(Fragment f) {
		fragMgr.beginTransaction().remove(f).commit();
	}



	public void open(final Fragment f) {
		if (tag == null) {
			tag = f.getClass().getName();
		}
		baseActivity.postOnResume(new Runnable() {

			@Override
			public void run() {
				fragMgr.beginTransaction().replace(id, f, tag).addToBackStack(tag).commit();
			}
		}, tag);
	}
}

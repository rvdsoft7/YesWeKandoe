package be.kdg.kandoe.utils;


import android.os.Message;

import be.kdg.kandoe.utils.MyLog;


public class PauseHandlerImpl extends PauseHandler {

	public PauseHandlerImpl() {
	}

	@Override
	protected boolean storeMessage(Message message) {
		//always store messages
		return true;
	}
	


	@Override
	protected void processMessage(Message message) {
		 MyLog.d(getClass(), "processMessage()");
		if(message.obj instanceof Runnable) {
			Runnable callback = (Runnable) message.obj;
			 MyLog.d(getClass(), "running callback()");
			callback.run();
		}
	}

}

package be.kdg.kandoe.utils;





import java.util.List;

import be.kdg.kandoe.backend.dom.Session.Session;
import java8.util.function.Predicate;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

public class SessionPredicates
{
    public static Predicate<Session> isPlanned() {
        return s-> s.isPlanned();
    }
     
    public static Predicate<Session> isPast() {
        return s -> !s.isGameOver();
    }
     
    public static Predicate<Session> isActive() {
        return s -> s.isPickingCards() || s.isPlaying();
    }
     
    public static List<Session> filterSessions (List<Session> sessions, Predicate<Session> predicate) {
        if(Utils.isEmpty(sessions))
            return sessions;
        return StreamSupport.stream(sessions).filter( predicate ).collect(Collectors.toList());
    }
}   
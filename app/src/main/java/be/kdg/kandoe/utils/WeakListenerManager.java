package be.kdg.kandoe.utils;

import java.lang.ref.WeakReference;
import java.util.LinkedList;
import java.util.List;

public class WeakListenerManager<T> {
	List<WeakReference<T>> listeners = new LinkedList<>();
	
	public void register(T listener) {
		if(!contains(listener)) {
			listeners.add(new WeakReference<T>(listener));
		}
	}
	
	public void unregister(T listener) {
		for (int i = listeners.size()-1;i>=0;i--) {
			WeakReference<T> item = listeners.get(i);
			if(item.get()==listener)
				listeners.remove(i);
		}
	}
	

	boolean contains(T listener) {
		for(WeakReference<T> weakRef : listeners) {
			T l = weakRef.get();
			if(l==listener)
				return true;
		}
		return false;
	}
	
	public void invoke(Invoker<T> invoker) {
		for(WeakReference<T> wl : listeners) {
			T l = wl.get();
			if(l!=null) {
				invoker.invoke(l);
			}
		}
	}
	
	public interface Invoker<T> {
		void invoke(T listener);
	}
}

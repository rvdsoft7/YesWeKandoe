package be.kdg.kandoe.utils;


import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.util.DisplayMetrics;
import android.util.LongSparseArray;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.download.ImageDownloader.Scheme;

import org.json.JSONArray;
import org.json.JSONObject;
import org.threeten.bp.format.DateTimeFormatter;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import be.kdg.kandoe.MyApplication;
import java8.util.concurrent.ThreadLocalRandom;

public class Utils {

	static final String TAG = Utils.class.getSimpleName();

	public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");


	public static final <T> T getListener(Fragment frag, Class<T> listenerType) {
		Object listener = frag.getTargetFragment();
		if(listener!=null && listenerType.isInstance(listener)) {
			MyLog.d(TAG, "getListener: targetFragment");
			return (T) listener;
		}
		listener = frag.getParentFragment();
		if(listener!=null && listenerType.isInstance(listener)) {
			MyLog.d(TAG, "getListener: parentFragment");
			return (T) listener;
		}
		listener = frag.getActivity();
		if(listener!=null && listenerType.isInstance(listener)) {
			MyLog.d(TAG, "getListener: activity");
			return (T) listener;
		}

		MyLog.d(TAG, "getListener: null");
		return null;
	}


	public static int changeAlpha(int argb, int alpha) {
		return Color.argb(alpha, Color.red(argb), Color.green(argb), Color.blue(argb));
	}

	public static int convertDipToPixels(float dip, DisplayMetrics dispMetrics) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip, dispMetrics);
	}


	public static <T> T createNewInstance(Class<T> clazz) {
		try {
			return clazz.newInstance();
		} catch (InstantiationException e) {
			be.kdg.kandoe.utils.MyLog.e(TAG, null, e);
		} catch (IllegalAccessException e) {
			be.kdg.kandoe.utils.MyLog.e(TAG, null, e);
		}
		return null;
	}

	public static <T> T ifNull(T a,T b) {
		return a==null?b:a;
	}
	public static String getStackTraceString(Throwable t) {
		StringWriter sw = new StringWriter();
		t.printStackTrace(new PrintWriter(sw));
		return sw.toString();
	}


	public static void checkArgument(boolean condition) {
		if (!condition) {
			throw new IllegalArgumentException();
		}
	}


	public static Class<?> getRawType(Type type) {
		if (type instanceof Class<?>) {
			// type is a normal class.
			return (Class<?>) type;

		} else if (type instanceof ParameterizedType) {
			ParameterizedType parameterizedType = (ParameterizedType) type;

			// I'm not exactly sure why getRawType() returns Type instead of Class.
			// Neal isn't either but suspects some pathological case related
			// to nested classes exists.
			Type rawType = parameterizedType.getRawType();
			checkArgument(rawType instanceof Class);
			return (Class<?>) rawType;

		} else if (type instanceof GenericArrayType) {
			Type componentType = ((GenericArrayType)type).getGenericComponentType();
			return Array.newInstance(getRawType(componentType), 0).getClass();

		} else if (type instanceof TypeVariable) {
			// we could use the variable's bounds, but that won't work if there are multiple.
			// having a raw type that's more general than necessary is okay
			return Object.class;

		} else if (type instanceof WildcardType) {
			return getRawType(((WildcardType) type).getUpperBounds()[0]);

		} else {
			String className = type == null ? "null" : type.getClass().getName();
			throw new IllegalArgumentException("Expected a Class, ParameterizedType, or "
					+ "GenericArrayType, but <" + type + "> is of type " + className);
		}
	}

	//	public static <Params, Progress, Result> void executeAsyncTask(VGCTAsyncTask<Void, Progress, Result> asyncTask) {
//		executeAsyncTask(asyncTask, (Void) null);
//	}
//
//	public static <Params, Progress, Result> void executeAsyncTask(VGCTAsyncTask<Params, Progress, Result> asyncTask,
//			Params... params) {
//		asyncTask.executeOnExecutor(VGCTAsyncTask.mCachedSerialExecutor, params);
//	}
//
//	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
//	public static <Params, Progress, Result> void executeAsyncTask(AsyncTask<Void, Progress, Result> asyncTask) {
//		executeAsyncTask(asyncTask, (Void) null);
//	}

	public static String makeNotNull(Object o) {
		return o != null ? o.toString() : "";
	}

	/**
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(String str) {
		return str == null || str.length() == 0 || str.trim().length() == 0;
	}

	/**
	 * 
	 * @param list
	 * @return true if the list is empty or null, otherwise false
	 */
	public static boolean isEmpty(Collection<?> list) {
		return list == null || list.size() == 0;
	}

	public static void hideKeyboard(Activity a) {
		View v = a.getCurrentFocus();
		if (v != null)
			hideKeyboard(v);
	}

	public static void hideKeyboard(View view) {
		InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}


}

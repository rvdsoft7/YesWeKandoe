package be.kdg.kandoe.utils;


import android.os.Build;

public class APIUtils{

	private APIUtils() {
	}

	public static boolean isCompatWith(int versionCode) {
		return Build.VERSION.SDK_INT >= versionCode;
	}

	public static boolean isCompatWithApiLevel16() {
		return isCompatWith(Build.VERSION_CODES.JELLY_BEAN);
	}


}

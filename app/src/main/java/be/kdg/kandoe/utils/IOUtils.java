package be.kdg.kandoe.utils;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.download.ImageDownloader.Scheme;
import com.nostra13.universalimageloader.utils.IoUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import be.kdg.kandoe.MyApplication;

public class IOUtils {
	private static final String TAG = IOUtils.class.getSimpleName();

	@SuppressWarnings("unused")
	public static boolean deleteCache(String imagePath) {
		if (Utils.isEmpty(imagePath))
			return false;
		ImageLoader imageLoader = ImageLoader.getInstance();
		String imageUri = toUri(imagePath);
		boolean success = imageLoader.getDiskCache().remove(imageUri);
		imageLoader.getMemoryCache().remove(imageUri);
		if (MyApplication.isDeveloperMode())
			MyLog.d("test", "removed " + imagePath + "  :" + success);
		return success;
	}

	public static String getTextFromResources(String txtFile) {
		BufferedReader reader = null;
		InputStream inputStream = null;
		try {
			inputStream = IOUtils.class.getClassLoader().getResourceAsStream(txtFile);
			if(inputStream==null) {
				return getTextFromAssets(txtFile);
			}
			reader = new BufferedReader(new InputStreamReader(inputStream));
			final StringBuilder builder = new StringBuilder();
			String line;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
				builder.append("\n");
			}
			return builder.toString().trim();
		} catch (IOException e) {
			MyLog.e(TAG, "Error loading assests.");
			return null;
		} finally {
			IoUtils.closeSilently(reader);
			IoUtils.closeSilently(inputStream);

		}
	}
	public static String getTextFromAssets(String txtFile) {
		InputStream inputStream = null;
		BufferedReader reader = null;
		try {
			inputStream = MyApplication.getInstance().getAssets().open(txtFile);
			reader = new BufferedReader(new InputStreamReader(inputStream));
			final StringBuilder builder = new StringBuilder();
			String line;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
				builder.append("\n");
			}
			return builder.toString().trim();
		} catch (IOException e) {
			MyLog.e(TAG, "Error loading assests.");
			return null;
		} finally {
			IoUtils.closeSilently(reader);
			IoUtils.closeSilently(inputStream);
		}
	}

	public static String toUri(String filePath) {
		if (filePath != null && filePath.length() > 0) {
			if (!filePath.startsWith("file://")) {
				return Scheme.FILE.wrap(filePath);
			}
		}
		return filePath;
	}
}

package be.kdg.kandoe.utils;

import android.util.Log;

import be.kdg.kandoe.MyApplication;

@SuppressWarnings("unused")
public class MyLog {

	private static volatile boolean ERROR_DEBUG = true;
	
	private static boolean isTestingMode() {
		return false;
	}

	private MyLog() {

	}

	public static void print(String msg) {
		if (MyApplication.isDeveloperMode()) {
			System.out.println(msg);
		}
	}

	private static boolean validate(String tag, String msg) {
		return tag != null && tag.length() > 0 && msg != null && msg.length() > 0;
	}

	private static boolean validate(Class<?> tag, String msg) {
		return tag != null && msg != null && msg.length() > 0;
	}

	public static void e(Class<?> tag, String msg, Throwable tr) {
		if (ERROR_DEBUG || MyApplication.isDeveloperMode()) {
			if (msg == null) {
				msg = "Error occurred: ";
			}
			e(tag.getSimpleName(), msg, tr);
		}
	}

	private static String getStackTraceString(Throwable tr) {
		return Utils.getStackTraceString(tr);
	}

	/**
	 * 
	 * @param tag
	 * @param msg
	 * @param tr
	 */
	public static void e(String tag, String msg, Throwable tr) {
		if (ERROR_DEBUG || MyApplication.isDeveloperMode()) {
			if (msg == null) {
				msg = "Error occurred: ";
			}
			if(isTestingMode())
				print(tag, msg, tr);
				else
			e(tag, msg + '\n' + getStackTraceString(tr));
		}
	}

	public static void e(Class<?> tag, String msg) {
		if ((ERROR_DEBUG || MyApplication.isDeveloperMode()) && validate(tag, msg)) {
			e(tag.getSimpleName(), msg);
		}
	}

	/**
	 * 
	 * @param tag
	 * @param msg
	 */
	public static void e(String tag, String msg) {
		if ((ERROR_DEBUG || MyApplication.isDeveloperMode()) && validate(tag, msg)) {
			if (ERROR_DEBUG)
				handleLog(tag, msg);
			if (MyApplication.isDeveloperMode())
				Log.e(tag, msg);
			if(isTestingMode()) {
				print(tag, msg);
			}
		}
	}

	private static void handleLog(final String tag, final String msg) {

	}

	/**
	 * 
	 * @param tag
	 * @param msg
	 * @param tr
	 */
	public static void d(Class<?> tag, String msg, Throwable tr) {
		if (MyApplication.isDeveloperMode()) {
			if (msg == null) {
				msg = "Error occurred: ";
			}
			Log.d(tag.getSimpleName(), msg, tr);
			if(isTestingMode()) {
				print(tag.getSimpleName(), msg, tr);
			}
		}
	}

	/**
	 * 
	 * @param tag
	 * @param msg
	 * @param tr
	 */
	public static void d(String tag, String msg, Throwable tr) {
		if (MyApplication.isDeveloperMode()) {
			if (msg == null) {
				msg = "Error occurred: ";
			}
			Log.d(tag, msg, tr);
			if(isTestingMode()) {
				print(tag, msg, tr);
			}
		}
	}

	static void print(String tag, String msg) {
		print(tag, msg, null);
	}

	static void print(String tag, String msg, Throwable tr) {
		System.out.println(tag + ": "+msg);
		if(tr!=null) {
			tr.printStackTrace();
		}
	}

	public static void d(Class<?> tag, String msg) {
		if (MyApplication.isDeveloperMode() && validate(tag, msg)) {
			Log.d(tag.getSimpleName(), msg);
			if(isTestingMode()) {
				print(tag.getSimpleName(), msg);
			}
		}
	}

	/**
	 * 
	 * @param tag
	 * @param msg
	 */
	public static void d(String tag, String msg) {
		if (MyApplication.isDeveloperMode() && validate(tag, msg)) {
			Log.d(tag, msg);
			if(isTestingMode()) {
				print(tag, msg);
			}
		}


	}

	/**
	 * 
	 * @param tag
	 * @param msg
	 * @param tr
	 */
	public static void i(String tag, String msg, Throwable tr) {
		if (MyApplication.isDeveloperMode()) {
			Log.i(tag, msg, tr);
			if(isTestingMode()) {
				print(tag, msg);
			}
		}
	}

	/**
	 * 
	 * @param tag
	 * @param msg
	 */
	public static void i(String tag, String msg) {
		if (MyApplication.isDeveloperMode() && validate(tag, msg)) {
			Log.i(tag, msg);
			if(isTestingMode()) {
				print(tag, msg);
			}
		}
	}

	public static void w(Class<?> clazz, Throwable tr) {
		if (MyApplication.isDeveloperMode()) {
			w(clazz.getSimpleName(), tr);
			if(isTestingMode()) {
				print(clazz.getSimpleName(),null, tr);
			}
		}
	}

	public static void w(String tag, Throwable tr) {
		if (MyApplication.isDeveloperMode() && tag != null) {
			Log.w(tag, tr);
			if(isTestingMode()) {
				print(tag, null, tr);
			}
		}
	}

	public static void w(Class<?> tag, String msg, Throwable tr) {
		if (MyApplication.isDeveloperMode() && validate(tag, msg)) {
			w(tag.getSimpleName(), msg, tr);
			if(isTestingMode()) {
				print(tag.getSimpleName(), msg, tr);
			}
		}
	}

	/**
	 * 
	 * @param tag
	 * @param msg
	 * @param tr
	 */
	public static void w(String tag, String msg, Throwable tr) {
		if (MyApplication.isDeveloperMode() && validate(tag, msg)) {
			if (msg == null) {
				msg = "Warning: ";
			}
			Log.w(tag, msg, tr);

			if(isTestingMode()) {
				print(tag, msg, tr);
			}
		}
	}

	public static void w(Class<?> tag, String msg) {
		if (MyApplication.isDeveloperMode() && validate(tag, msg)) {
			Log.w(tag.getSimpleName(), msg);
			if(isTestingMode()) {
				print(tag.getSimpleName(), msg);
			}

		}
	}

}
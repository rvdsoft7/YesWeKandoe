package be.kdg.kandoe.backend.dom.content;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import be.kdg.kandoe.backend.dom.Session.Session;
import be.kdg.kandoe.backend.dom.user.User;
import java8.util.stream.StreamSupport;


/**
 * A subject for a {@link Session}
 */

public class Theme implements Serializable {

    private static final long serialVersionUID = 6994325654472701057L;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Theme theme = (Theme) o;
        return Objects.equals(themeId, theme.themeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(themeId);
    }

    private Long themeId;
    private String themeName;
    private String description;

    private List<User> organisers = new ArrayList<>();
    private List<Tag> tags;
    @JsonIgnore
    private List<Card> cards;
    @JsonIgnore
    private List<Session> sessions;

    private boolean publicAllowed = true;

    public Theme() {
    }

    public String getName() {
        return themeName;
    }

    public List<Session> getSessions() {
        return sessions;
    }

    public void setSessions(List<Session> sessions) {
        this.sessions = sessions;
    }

    public Long getId() {
        return themeId;
    }

    public List<User> getOrganisers() {
        return organisers;
    }

    public void setOrganisers(List<User> organisers) {
        this.organisers = organisers;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    public void setThemeId(Long themeId) {
        this.themeId = themeId;
    }

    public void setThemeName(String themeName) {
        this.themeName = themeName;
    }

    public boolean isPublicAllowed() {
        return publicAllowed;
    }

    public void setPublicAllowed(boolean publicAllowed) {
        this.publicAllowed = publicAllowed;
    }

    @JsonSetter("cards")
    public void customCardsSetter(List<Card> cards) {
        this.cards = cards;
        if (this.cards != null) {
            StreamSupport.stream(cards).forEach(card -> card.setTheme(this));
        }
    }

    @JsonSetter("sessions")
    public void customSessionSetter(List<Session> sessions) {
        this.sessions = sessions;
        if (this.sessions != null) {
            StreamSupport.stream(sessions).forEach(session -> session.setTheme(this));
        }
    }
}

package be.kdg.kandoe.backend.retrofit.service.mock.session;

import java.util.ArrayList;
import java.util.List;

import be.kdg.kandoe.backend.dom.content.Remark;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by Ruben on 12/03/2017.
 */

public class MockRemarkRepo {
    List<Remark> remarkList = new ArrayList<>();
    static MockRemarkRepo instance = new MockRemarkRepo();

    public static MockRemarkRepo getInstance() {
        return instance;
    }

    public void add(Remark... remarks) {
        for(Remark r : remarks) {
            remarkList.add(r);
        }
    }

    public  List<Remark> getRemarksBySessionCardId(long sessionCardId) {
        return StreamSupport.stream(remarkList).filter(r->r.getCard().getId()==sessionCardId).collect(Collectors.toList());
    }
}

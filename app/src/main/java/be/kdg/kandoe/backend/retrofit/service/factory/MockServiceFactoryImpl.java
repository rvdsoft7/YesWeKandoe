package be.kdg.kandoe.backend.retrofit.service.factory;

import be.kdg.kandoe.backend.retrofit.service.util.CustomRetrofitBuilder;
import be.kdg.kandoe.backend.retrofit.service.mock.MockService;
import be.kdg.kandoe.backend.retrofit.service.mock.session.MockSessionService;
import be.kdg.kandoe.backend.retrofit.service.mock.theme.MockThemeService;
import be.kdg.kandoe.backend.retrofit.service.mock.user.MockFacebookLoginService;
import be.kdg.kandoe.backend.retrofit.service.mock.user.MockGoogleLoginService;
import be.kdg.kandoe.backend.retrofit.service.mock.user.MockUserService;
import be.kdg.kandoe.backend.retrofit.service.api.session.SessionService;
import be.kdg.kandoe.backend.retrofit.service.api.theme.ThemeService;
import be.kdg.kandoe.backend.retrofit.service.api.user.FacebookLoginService;
import be.kdg.kandoe.backend.retrofit.service.api.user.GoogleLoginService;
import be.kdg.kandoe.backend.retrofit.service.api.user.UserService;

/**
 * Created by Ruben on 19/02/2017.
 */

public class MockServiceFactoryImpl implements IServiceFactory {

    public <S, M extends MockService<S>> M createMockService(Class<S> service, Class<M> mockService) {
        return new CustomRetrofitBuilder().buildMockService(service, mockService);
    }

    @Override
    public UserService createUserService() {
        return createMockService(UserService.class, MockUserService.class);
    }

    @Override
    public ThemeService createThemeSevice() {
        return createMockService(ThemeService.class, MockThemeService.class);
    }

    @Override
    public FacebookLoginService createFacebookLoginService() {
        return createMockService(FacebookLoginService.class, MockFacebookLoginService.class);
    }

    @Override
    public GoogleLoginService createGoogleLoginService() {
        return createMockService(GoogleLoginService.class, MockGoogleLoginService.class);
    }

    @Override
    public SessionService createSessionService() {
        return createMockService(SessionService.class, MockSessionService.class);
    }
}

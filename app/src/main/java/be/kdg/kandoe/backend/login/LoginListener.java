package be.kdg.kandoe.backend.login;


import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.retrofit.service.util.ErrorResult;

public interface LoginListener {

	void onLoggedIn(User user);
	void onLoggedOut();
	void onRegistered(User user);
	void onProfileEdited(User user);
	void onAuthenticationFailed(ErrorResult errorResult);
}

package be.kdg.kandoe.backend.login.facebook;

import android.net.Uri;

import com.facebook.AccessToken;
import com.facebook.internal.ImageRequest;
import com.nostra13.universalimageloader.core.assist.ImageSize;

import org.json.JSONObject;

public class FacebookPerson {
	
	public void setInfo(JSONObject json, AccessToken accessToken) {
		this.accessToken = accessToken;
		setId(json.optString("id"));
		setFamilyName(json.optString("last_name"));
		setSurName(json.optString("first_name"));
		setEmail(json.optString("email"));
	}
	
	AccessToken accessToken;

	public AccessToken getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(AccessToken accessToken) {
		this.accessToken = accessToken;
	}

	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	private String surName;

	public String getSurName() {
		return surName;
	}

	public void setSurName(String surName) {
		this.surName = surName;
	}

	private String familyName;

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}


	private String email;

	public Uri getAvatar(ImageSize targetSize) {
		return ImageRequest.getProfilePictureUri(this.id, targetSize.getWidth(), targetSize.getHeight());
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}

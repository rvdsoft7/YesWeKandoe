package be.kdg.kandoe.backend.login.kandoe;

public enum AuthType {

	LOGIN, REGISTER, EDIT_PROFILE;
}

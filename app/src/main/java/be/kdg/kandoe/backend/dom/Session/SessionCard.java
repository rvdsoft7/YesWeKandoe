package be.kdg.kandoe.backend.dom.Session;


import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Objects;

import be.kdg.kandoe.backend.dom.content.Card;

/**
 * A SessionCard is a card for a {@link Session}
 */


public class SessionCard implements Serializable, Comparable<SessionCard> {

    private Card card;
    private long sessionCardId;
    @JsonIgnore
    private Session session;
    private boolean isSelected;
    private String color;
    private int distanceToCenter = Session.MAX_LEVEL;

    public SessionCard() {

    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SessionCard that = (SessionCard) o;
        return sessionCardId == that.sessionCardId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(sessionCardId);
    }

    public int getDistanceToCenter() {
        return distanceToCenter;
    }

    public void setDistanceToCenter(int distanceToCenter) {
        this.distanceToCenter = distanceToCenter;
    }

    public String getText() {
        return getCard().getText();
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public long getId() {
        return sessionCardId;
    }

    @Override
    public int compareTo(SessionCard cs) {
        return this.getText().compareToIgnoreCase(cs.getText());
    }

    @Override
    public String toString() {
        return getId() + "_" + card.getText();
    }

    public void setSessionCardId(long sessionCardId) {
        this.sessionCardId = sessionCardId;
    }

    public int getLevel() {
        return Session.MAX_LEVEL - distanceToCenter;
    }

    public void setLevel(int level) {
        distanceToCenter = Session.MAX_LEVEL - level;
    }

    public void incrementLevel() {
        setLevel(getLevel() + 1);
    }

    public boolean canMoveUp() {
        return getLevel()<Session.MAX_LEVEL;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @JsonGetter
    public Long getSessionCardId() {
        if(session==null)
            return null;
        return session.getId();
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}


package be.kdg.kandoe.backend.retrofit.service.mock.theme;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


import be.kdg.kandoe.backend.dom.content.Card;
import be.kdg.kandoe.backend.dom.content.Tag;
import be.kdg.kandoe.backend.dom.content.Theme;
import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.retrofit.service.mock.session.DefaultSession;
import be.kdg.kandoe.backend.retrofit.service.mock.user.MockUserRepo;
import java8.util.stream.Collectors;
import java8.util.stream.RefStreams;

/**
 * Created by Ruben on 21/02/2017.
 */

public class DefaultThemes {

    static List<Theme> themes;

    private static Card createCard(long nummer) {
        Card card = new Card();
        card.setCardId(nummer);
        card.setText("Kaart "+nummer);
        return card;
    }

    private static Theme createTheme(int nummer) {
        Tag tag1 = new Tag();
        tag1.setName("tag1");
        Tag tag2 = new Tag();
        tag2.setName("tag2");
        Tag tag3 = new Tag();
        tag3.setName("tag3");
        Theme theme = new Theme();
        theme.setThemeId((long) nummer);
        theme.setThemeName("Theme "+nummer);
        if(nummer>2)
        theme.setPublicAllowed(new Random().nextBoolean());
        theme.setDescription("Description "+nummer);
        theme.setTags(RefStreams.of(tag1, tag2, tag3).collect(Collectors.toList()));
        List<User> organisators = new ArrayList<>();
        for(int i = 1;i<3;i++) {
            organisators.add(MockUserRepo.getInstance().findUserById(i));
        }
        theme.setOrganisers(organisators);

        MockUserRepo repo = MockUserRepo.getInstance();
        theme.setOrganisers(RefStreams.of(repo.findUserById(2), repo.findUserById(3), repo.findUserById(4)).collect(Collectors.toList()));

        List<Card> cards = new ArrayList<>();
        int i = 0;
        for(char alphabet = 'A'; alphabet <= 'Z';alphabet++) {
            Card card = createCard(i+1);
            card.setText(String.valueOf(alphabet)+"K"+i+"Aart "+i);
            card.setImageURL("http://images.techtimes.com/data/images/full/132112/metal-gear-chronological-order.jpg");
            card.setTheme(theme);
            cards.add(card);
            i++;
        }
       theme.setCards(cards);
        theme.setSessions(DefaultSession.create(theme));
        return theme;
    }

    public static List<Theme> create() {
        if(themes!=null)
            return new ArrayList<>(themes);
       return (themes =  RefStreams.of(createTheme(1), createTheme(2), createTheme(3), createTheme(4)).collect(Collectors.toList()));
    }
}

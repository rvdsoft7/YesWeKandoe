package be.kdg.kandoe.backend.login.kandoe;


import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import be.kdg.kandoe.MyApplication;
import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.login.BaseSignIn;
import be.kdg.kandoe.backend.login.LoginListener;
import be.kdg.kandoe.backend.login.LoginListener2;
import be.kdg.kandoe.backend.login.LoginPrefsContract;
import be.kdg.kandoe.backend.login.facebook.FacebookPerson;
import be.kdg.kandoe.backend.login.facebook.FacebookSignInHelper;
import be.kdg.kandoe.backend.login.google.GooglePerson;
import be.kdg.kandoe.backend.login.google.GoogleSignInHelper;
import be.kdg.kandoe.backend.retrofit.service.util.ErrorResult;
import be.kdg.kandoe.backend.retrofit.service.util.RetrofitException;
import be.kdg.kandoe.backend.retrofit.service.util.RetrofitHelper;
import be.kdg.kandoe.backend.retrofit.service.factory.ServiceGenerator;
import be.kdg.kandoe.frontend.activities.BaseActivity;
import be.kdg.kandoe.utils.MyLog;
import retrofit2.Call;
import retrofit2.Response;

public class KandoeSignInHelper extends BaseSignIn<LoginListener> {

    static final String TAG = KandoeSignInHelper.class.getSimpleName();

    User mUser;
    SharedPreferences prefs;
    Call<User> mCall;
    AuthCallback mCallback;
    User mTempLogin;
    GoogleSignInHelper googleHelper;
    FacebookSignInHelper facebookHelper;

    public KandoeSignInHelper(LoginListener loginListener) {
        super(loginListener);
        facebookHelper = new FacebookSignInHelper(facebookListener);
        googleHelper = new GoogleSignInHelper(googleListener);
        prefs = MyApplication.getLoginPrefs();
        loginSilently();
    }

    @Override
    public boolean loginSilently() {
        User cached = readPrefs();
        if (cached != null) {
            tryLogin(cached);
        } else {
            facebookHelper.loginSilently();
            googleHelper.loginSilently();
        }
        return true;
    }

    @Override
    public boolean isLoading() {
        return mCall!=null && mCall.isExecuted() ||facebookHelper.isLoading() ||googleHelper.isLoading();
    }

    public void loginGoogle() {
        if (googleHelper.isLoggedIn()) {
            logOut();
        } else {
            googleHelper.logIn();
        }
    }

    public GoogleSignInHelper getGoogleSignInHelper() {
        return googleHelper;
    }

    public FacebookSignInHelper getFacebookSignInHelper() {
        return facebookHelper;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        googleHelper.onActivityResult(requestCode, resultCode, data);
        facebookHelper.onActivityResult(requestCode, resultCode, data);
    }

    public void setActivity(BaseActivity activity) {
        super.setActivity(activity);
        facebookHelper.setActivity(activity);
        googleHelper.setActivity(activity);
    }

    ;

    LoginListener2 facebookListener = new LoginListener2() {

        @Override
        public void onLoggedOut() {
        }

        @Override
        public void onLoggedIn() {
            tryFacebookLogin(facebookHelper.getPerson());
        }
    };

    LoginListener2 googleListener = new LoginListener2() {

        @Override
        public void onLoggedOut() {
        }

        @Override
        public void onLoggedIn() {
          tryGoogleLogin(googleHelper.getPerson());
        }
    };

    public void clearPrefs() {
        savePrefs(null);
    }


    User readPrefs() {
        String json = prefs.getString(LoginPrefsContract.PREF_LOGIN, null);
        if (json != null && json.length() > 0) {
            return RetrofitHelper.readValue(json, User.class);
        }

        return null;
    }


    void savePrefs(User login) {
        String json = login != null ? RetrofitHelper.writeValueAsString(login) : null;
        prefs.edit().putString(LoginPrefsContract.PREF_LOGIN, json).apply();
    }


    @Override
    public User getUser() {
        return mUser;
    }

    @Override
    public boolean isLoggedIn() {
        return mUser != null;
    }

    void tryFacebookLogin(FacebookPerson person) {
        cancelCall();
        User u = new User();
        u.fillInfo(person);
        u.setFbToken(person.getAccessToken().getToken());
        mCall = ServiceGenerator.getInstance().createFacebookLoginService().facebookLogin(u);
        mCall.enqueue(createNewCallback(AuthType.LOGIN));

    }

    void tryGoogleLogin(GooglePerson person) {
        cancelCall();
        User u = new User();
        u.fillInfo(person);
        if(person.getIdToken()==null) {
            Toast.makeText(getContext(), "Error token is null"+person.getIdToken() +" "+person.getServerAuthCode(), Toast.LENGTH_SHORT).show();
        }
        u.setGoogleToken(person.getIdToken());
        mCall = ServiceGenerator.getInstance().createGoogleLoginService().googleLogin(u);
        mCall.enqueue(createNewCallback(AuthType.LOGIN));

    }

    void tryLogin(User login) {
        cancelCall();
        mCall = ServiceGenerator.getInstance().createUserService().basicLogin(login);
        mCall.enqueue(createNewCallback(AuthType.LOGIN));
    }

    public boolean logIn(User login) {
        if (isRememberMe()) {
            mTempLogin = login;
        }
        tryLogin(login);
        return true;
    }

    boolean isRememberMe() {
        return true;
    }

    void cancelCall() {
        if (mCall != null && !mCall.isCanceled()) {
            mCall.cancel();
        }
        if (mCallback != null) {
            mCallback.cancel();
        }

        nullifyCalls();
    }

    public void editProfile(User user) {
        cancelCall();
        mCall = ServiceGenerator.getInstance().createUserService().editProfile(user);
        mCall.enqueue(createNewCallback(AuthType.EDIT_PROFILE));
    }

    public boolean register(User user) {
        cancelCall();
        mCall = ServiceGenerator.getInstance().createUserService().basicRegister(user);
        mCall.enqueue(createNewCallback(AuthType.REGISTER));
        return true;
    }

    void nullifyCalls() {
        mCall = null;
        mCallback = null;
    }


    AuthCallback createNewCallback(AuthType authType) {
        mCallback = new AuthCallback(authType) {

            @Override
            void onResponse(Call<User> call, Response<User> response, AuthType authType) {
                nullifyCalls();
                User user = response.body();
                if(authType == AuthType.REGISTER ||authType == AuthType.LOGIN) {
                    String token = response.headers().get("Authorization");
                    AccessToken.setAccessToken(token);
                }
                if (authType == AuthType.LOGIN) {
                    mUser = user;
                    if (mTempLogin != null) {
                        savePrefs(mTempLogin);
                        mTempLogin = null;
                    }
                    getListener().onLoggedIn(mUser);
                } else if (authType == AuthType.REGISTER) {
                    mUser = user;
                    getListener().onRegistered(user);
                } else if (authType == AuthType.EDIT_PROFILE) {
                    mUser = user;
                    getListener().onProfileEdited(user);

                }

                MyLog.d(getClass(), "onResponse: "+authType + " "+user);
            }

            @Override
            void onFailure(Call<User> call, RetrofitException throwable, AuthType authType) {
                nullifyCalls();
                mUser = null;
                ErrorResult result = throwable.getErrorResult();
                String url = throwable.getResponse()!=null ? throwable.getResponse().raw().request().url().toString() : null;
                MyLog.d(TAG, "onFailure: "+authType + " "+result + " "+url);
                getListener().onAuthenticationFailed(result);
            }
        };
        return mCallback;
    }

    @Override
    public boolean logIn() {
        loginSilently();
        return true;
    }

    @Override
    public void logOut() {
        MyLog.d(getClass(), "logOut");
        if (isLoggedIn()) {
            AccessToken.setAccessToken(null);
            mUser = null;
            clearPrefs();
            facebookHelper.logOut();
            googleHelper.logOut();
            getListener().onLoggedOut();
        }
    }
}

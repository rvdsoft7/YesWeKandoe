package be.kdg.kandoe.backend.dom.user;


import android.content.Context;
import android.provider.Settings;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Objects;

import be.kdg.kandoe.backend.login.facebook.FacebookPerson;
import be.kdg.kandoe.backend.login.google.GooglePerson;
import be.kdg.kandoe.utils.ImageLoaderHelper;


public class User implements Serializable {

    private static final long serialVersionUID = -8369648823000434206L;
    private Long userId;
    private String username;
    private String password;
    private String firstname;
    private String lastname;
    private String fbToken;
    private String googleToken;
    @JsonProperty("profileImg")
    private String avatarUrl;

    public User() {
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getShortName() {
        return getUsername().substring(0,getUsername().indexOf('@'));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(getUsername(), user.getUsername());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUsername());
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", fbToken='" + fbToken + '\'' +
                ", googleToken='" + googleToken + '\'' +
                ", avatarUrl='" + avatarUrl + '\'' +
                '}';
    }

    public void fillInfo(GooglePerson person) {
        setAvatarUrl(person.getAvatar());
        setUsername(person.getEmail());
        setFirstName(person.getSurName());
        setLastName(person.getFamilyName());
    }

    public void fillInfo(FacebookPerson person) {
        setAvatarUrl(person.getAvatar(ImageLoaderHelper.DEFAULT_TARGET_SIZE).toString());
        setUsername(person.getEmail());
        setFirstName(person.getSurName());
        setLastName(person.getFamilyName());
    }

    public String getFullName() {
        if (firstname == null && lastname == null)
            return "";
        if(lastname==null && firstname!=null) {
            if(firstname==null && lastname!=null) {
                return lastname;
            }
            return firstname;
        }
        return firstname + " " + lastname;
    }

    public long getId() {
        return userId;
    }

    public void setId(Long id) {
        this.userId = id;
    }

    public String getLastName() {
        return lastname;
    }

    public void setLastName(String lastName) {
        this.lastname = lastName;
    }

    public String getFirstName() {
        return firstname;
    }

    public void setFirstName(String firstName) {
        this.firstname = firstName;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUsername() {

        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getFbToken() {
        return fbToken;
    }

    public void setFbToken(String fbToken) {
        this.fbToken = fbToken;
    }

    public String getGoogleToken() {
        return googleToken;
    }

    public void setGoogleToken(String googleToken) {
        this.googleToken = googleToken;
    }
}


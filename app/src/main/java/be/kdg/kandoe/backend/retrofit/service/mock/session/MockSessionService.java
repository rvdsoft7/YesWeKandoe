package be.kdg.kandoe.backend.retrofit.service.mock.session;

import org.threeten.bp.LocalDateTime;

import java.util.Collections;
import java.util.List;

import be.kdg.kandoe.backend.dom.Session.Session;
import be.kdg.kandoe.backend.dom.Session.SessionCard;
import be.kdg.kandoe.backend.dom.content.Message;
import be.kdg.kandoe.backend.dom.content.Remark;
import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.login.LoginHelperImpl;
import be.kdg.kandoe.backend.retrofit.service.mock.MockService;
import be.kdg.kandoe.backend.retrofit.service.mock.theme.DefaultThemes;
import be.kdg.kandoe.backend.retrofit.service.api.session.SessionService;
import be.kdg.kandoe.utils.MyLog;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Path;

/**
 * Created by Ruben on 08/03/2017.
 */

public class MockSessionService extends MockService<SessionService> implements SessionService {

    @Override
    public Call<Session> makeMove(@Path("sessionId") long sessionId, @Path("sessionCardID") long sessionCardId) {
        Session session = findSession(sessionId);
        SessionCard sessionCard = StreamSupport.stream(session.getOrganiserSessionCards()).filter(card -> card.getId() == sessionCardId).findAny().get();
        sessionCard.incrementLevel();
        User nextUser = getNextUserTurn(session);
        session.setCurrentUser(nextUser);
        return getDelegate().returningResponse(session).makeMove(sessionId, sessionCardId);
    }

    void incrementAndSaveRound(Session session) {
        session.incrementRound();
        session.setCurrentUser(null);
    }


    User getNextUserTurn(Session session) {
        if (session.isGameOver()) {
            MyLog.d(getClass(), "getNextUserTurn game over");
            return null;
        }
        User currentUser = session.getCurrentUser();
        List<User> sortedUsers = session.getUsers();
        if (currentUser == null) {
            MyLog.d(getClass(), "getNextUserTurn ==null");
            return sortedUsers.get(0);
        }

        int index = sortedUsers.indexOf(currentUser);
        int nextIndex = index + 1;
        if (nextIndex == sortedUsers.size()) {
            incrementAndSaveRound(session);
            MyLog.d(getClass(), "getNextUserTurn size");
            return getNextUserTurn(session);
        } else if (index >= 0) {
            MyLog.d(getClass(), "getNextUserTurn index: " + (index + 1));
            return sortedUsers.get(nextIndex);
        }
        return null;
    }

    Session findSession(long sessionId) {
        Session session = StreamSupport.stream(DefaultThemes.create()).flatMap(t -> StreamSupport.stream(t.getSessions())).filter(s -> s.getId() == sessionId).findAny().get();
        return session;
    }

    @Override
    public Call<Session> getSession(@Path("sessionId") long sessionId) {
        return getDelegate().returningResponse(findSession(sessionId)).getSession(sessionId);
    }

    @Override
    public Call<List<Message>> getChat(@Path("sessionId") long sessionId) {
        return getDelegate().returningResponse(Collections.emptyList()).getChat(sessionId);
    }

    @Override
    public Call<Message> uploadMessage(@Path("sessionId") long sessionId, Message message) {
        return getDelegate().returningResponse(message).uploadMessage(sessionId, message);
    }

    @Override
    public Call<Session> pickCards(@Path("sessionId") long sessionId, Long[] sessionCardIds) {
        Session session = findSession(sessionId);
        for (long id : sessionCardIds)
            StreamSupport.stream(session.getOrganiserSessionCards()).filter(sessionCard -> sessionCard.getId() == id).forEach(sessionCard -> sessionCard.setSelected(true));
        User nextUser = getNextUserTurn(session);
        session.setCurrentUser(nextUser);
        return getDelegate().returningResponse(session).pickCards(sessionId, sessionCardIds);
    }

    @Override
    public Call<List<Session>> getSessionsByUserId(@Path("userId") long userId) {
        List<Session> sessions = StreamSupport.stream(DefaultThemes.create()).flatMap(t -> StreamSupport.stream(t.getSessions()))
                .filter(s -> StreamSupport.stream(s.getUsers()).anyMatch(u -> u.getId() == userId)).collect(Collectors.toList());
        return getDelegate().returningResponse(sessions).getSessionsByUserId(userId);
    }

    @Override
    public Call<List<Session>> getSessionByThemeId(@Path("themeId") long themeId) {
        List<Session> sessions = StreamSupport.stream(DefaultThemes.create()).filter(t -> t.getId() == themeId).findAny().get().getSessions();
        return getDelegate().returningResponse(sessions).getSessionByThemeId(themeId);
    }

    @Override
    public Call<Session> createSession(@Body Session session) {
        StreamSupport.stream(DefaultThemes.create()).filter(t -> t.getId() == session.getTheme().getId()).findAny().get().getSessions().add(session);
        session.setId(DefaultSession.idCounter.getAndIncrement());
        return getDelegate().returningResponse(session).createSession(session);
    }

    @Override
    public Call<Session> addUser(@Path(("sessionId")) long sessionId, @Body User user) {
        Session session = findSession(sessionId);
        session.getUsers().add(user);
        return getDelegate().returningResponse(session).addUser(sessionId, user);
    }

    @Override
    public Call<Session> beginSession(@Path(("sessionId")) long sessionId) {
        Session session = findSession(sessionId);
        session.setStartDate(LocalDateTime.now());
        session.setCurrentUser(session.getUsers().get(0));
        return getDelegate().returningResponse(session).beginSession(sessionId);
    }

    @Override
    public Call<Session> endSession(@Path(("sessionId")) long sessionId) {
        return null;
    }

    @Override
    public Call<ResponseBody> deleteSession(@Body long sessionId) {
        StreamSupport.stream(DefaultThemes.create()).forEach(t-> {
            Session session = StreamSupport.stream(t.getSessions()).filter(s->s.getId()==sessionId).findAny().orElse(null);
            DefaultThemes.create().remove(session);
        });
        return getDelegate().returningResponse(null).deleteSession(sessionId);
    }

    @Override
    public Call<List<Remark>> uploadRemark(@Path("sessionId") long sessionId, @Path("sessionCardId") long sessionCardId, @Body Remark remark) {
        Session session = findSession(sessionId);
        SessionCard sessionCard = StreamSupport.stream(session.getOrganiserSessionCards()).filter(c -> c.getId() == sessionCardId).findAny().get();
        remark.setCard(sessionCard);
        remark.setUser(LoginHelperImpl.getInstance().getUser());
        MockRemarkRepo.getInstance().add(remark);
        return getDelegate().returningResponse(MockRemarkRepo.getInstance().getRemarksBySessionCardId(sessionCardId)).uploadRemark(sessionId, sessionCardId, remark);

    }

    @Override
    public Call<List<Remark>> getRemarks(@Path("sessionId") long sessionId, @Path("sessionCardId") long sessionCardId) {
        return getDelegate().returningResponse(MockRemarkRepo.getInstance().getRemarksBySessionCardId(sessionCardId)).getRemarks(sessionId, sessionCardId);
    }
}

package be.kdg.kandoe.backend.retrofit.service.util;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import org.threeten.bp.LocalDateTime;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import be.kdg.kandoe.MyApplication;
import be.kdg.kandoe.backend.config.KandoeConfig;
import be.kdg.kandoe.utils.MyLog;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class RetrofitHelper {

	public static final String BASE_URL = KandoeConfig.getBaseUrl(false)+"/api/";

    public static final String JSON_MIME_TYPE = "application/json";

	public static final String TOKEN_HEADER = "Authorization";

	public static final MediaType JSON_MEDIA_TYPE = MediaType.parse(JSON_MIME_TYPE);

	public static final long MOCK_DELAY = 500;

    static final SimpleModule JACKSON_MODULE = new SimpleModule();
    static {
        JACKSON_MODULE.addDeserializer(LocalDateTime.class, new DateDeserializer());
        JACKSON_MODULE.addSerializer(LocalDateTime.class, new DateSerializer());
    }

    private static final ObjectMapper DEFAULT_MAPPER = createMapper();




	public static OkHttpClient.Builder createHttpClientBuiler() {
	OkHttpClient.Builder builder= new OkHttpClient.Builder()
				.connectTimeout(10, TimeUnit.SECONDS)
				.readTimeout(30, TimeUnit.SECONDS)
				.addInterceptor(new HeaderRequestInterceptor("Accept", JSON_MIME_TYPE));

        if(MyApplication.isDeveloperMode()) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }
        return builder;

	}

    public static <T> Response<T> createErrorResponse(int code, int stringResId) {
        return createErrorResponse(code, MyApplication.getInstance().getString(stringResId));
    }

    public static <T> Response<T> createErrorResponse(int code, String message) {
        ErrorResult error = new ErrorResult(code, message);
        return Response.error(code, ResponseBody.create(JSON_MEDIA_TYPE, writeValueAsString(error)));
    }


	public static Retrofit.Builder builder() {
		return  new Retrofit.Builder()
				.baseUrl(BASE_URL)
				.addCallAdapterFactory(ErrorHandlingExecutorCallAdapterFactory.create())
				.addConverterFactory(JacksonConverterFactory.create(DEFAULT_MAPPER));
	}



	public static ObjectMapper createMapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.registerModule(JACKSON_MODULE);
        mapper.setVisibility(mapper.getSerializationConfig().getDefaultVisibilityChecker()
                .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
                .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withSetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withCreatorVisibility(JsonAutoDetect.Visibility.NONE));
		return mapper;
	}

	public static String writeValueAsStringPretty(Object value) {
		try {
			return createMapper().writerWithDefaultPrettyPrinter().writeValueAsString(value);
		} catch (JsonProcessingException e) {
			MyLog.e(RetrofitHelper.class, null, e);
		}
		return null;
	}

	public static String writeValueAsString(Object value) {
		try {
			return DEFAULT_MAPPER.writeValueAsString(value);
		} catch (JsonProcessingException e) {
			MyLog.e(RetrofitHelper.class, null, e);
		}
		return null;
	}

	public static <T> T readValue(String content, Class<T> clazz) {
		try {
			return DEFAULT_MAPPER.readValue(content, clazz);
		} catch (IOException e) {
			MyLog.e(RetrofitHelper.class, null, e);
		}
		return null;
	}

	public static <T> List<T> readListValue(String content, Class<T> clazz) {
		try {
			return DEFAULT_MAPPER.readValue(content, DEFAULT_MAPPER.getTypeFactory().constructCollectionType(List.class, clazz));
		} catch (IOException e) {
			MyLog.e(RetrofitHelper.class, null, e);
		}
		return null;
	}

}

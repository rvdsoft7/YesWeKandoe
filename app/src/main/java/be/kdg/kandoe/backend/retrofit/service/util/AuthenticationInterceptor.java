package be.kdg.kandoe.backend.retrofit.service.util;

import be.kdg.kandoe.backend.login.kandoe.AccessToken;

public class AuthenticationInterceptor extends HeaderRequestInterceptor {


    public AuthenticationInterceptor(String accessToken) {
        super(RetrofitHelper.TOKEN_HEADER, accessToken);
    }

    public AuthenticationInterceptor(AccessToken accessToken) {
        this(accessToken.getAuthToken());
    }
}
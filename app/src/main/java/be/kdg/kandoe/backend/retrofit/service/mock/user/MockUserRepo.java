package be.kdg.kandoe.backend.retrofit.service.mock.user;

import java.util.ArrayList;
import java.util.List;

import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.utils.MyLog;
import java8.util.function.Predicate;
import java8.util.stream.StreamSupport;

/**
 * Created by Ruben on 22/02/2017.
 */

public class MockUserRepo {
    List<User> userRepo = new ArrayList<>();

    private MockUserRepo() {
        seed();
    }
    private static MockUserRepo instance;
    public static MockUserRepo getInstance() {
        if(instance == null)
            instance = new MockUserRepo();
        return instance;
    }

    public User findAdmin() {
        return findUserById(1);
    }

   private void seed() {
       User admin = DefaultUser.create(1);
       admin.setId(1L);
       admin.setFirstName(null);
       admin.setLastName(null);
       admin.setUsername("admin@kandoe.com");
       admin.setPassword("123");
        userRepo.add(admin);
        for(int i = 2;i<=6;i++) {
            userRepo.add(DefaultUser.create(i));
        }


    }

    public boolean doesUserExist(String username) {
        return findUserByUsername(username)!=null;
    }

    public boolean register(User user) {
        if(!doesUserExist(user.getUsername())) {
            user.setId((long) (userRepo.size() + 1));
         userRepo.add(user);
            MyLog.d(getClass(), "registered: "+user.getUsername());
            return true;
        }
        return false;
    }

    private User filter(Predicate<User> predicate) {
        return StreamSupport.stream(userRepo)
                .filter(predicate)
                .findAny().orElse(null);
    }

    public User findUserById(long id) {
        return filter(x-> x.getId() == id);
    }

    public User findUserByUsername(String username) {
        return filter(x-> x.getUsername().equals(username));
    }

    public User updateUser(User user) {
        User u = findUserById(user.getId());
        u.setUsername(user.getUsername());
        u.setLastName(user.getLastName());
        u.setFirstName(user.getFirstName());
        return u;
    }

    public boolean authenticate(User user){
        User found = findUserByUsername(user.getUsername());
        if(found == null)
            return false;

        return found.getPassword().equals(user.getPassword());
    }
}

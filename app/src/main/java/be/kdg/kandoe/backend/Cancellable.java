package be.kdg.kandoe.backend;

public interface Cancellable {

	@SuppressWarnings("unused")
    void cancel();
	@SuppressWarnings("unused")
    boolean isCancelled();
}

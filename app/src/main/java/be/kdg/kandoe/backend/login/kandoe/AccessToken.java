package be.kdg.kandoe.backend.login.kandoe;

import be.kdg.kandoe.utils.MyLog;

/**
 * Created by Ruben on 24/02/2017.
 */

public class AccessToken {

    public static final String TAG = AccessToken.class.getSimpleName();

    private final String authToken;

    private static AccessToken instance;

    private AccessToken(String authToken) {
        this.authToken = authToken;
    }

    public static AccessToken getCurrentAccessToken() {
       return instance;
    }

    protected static void setAccessToken(String authToken) {
        if(authToken!=null){
            instance = new AccessToken(authToken);
            MyLog.d(TAG, "Token set: "+instance);
        } else {
            instance = null;
            MyLog.d(TAG, "Token reset");
        }
    }

    @Override
    public String toString() {
        return authToken;
    }

    public String getAuthToken() {
        return authToken;
    }
}

package be.kdg.kandoe.backend.dom.content;

import org.threeten.bp.LocalDateTime;

import java.io.Serializable;

import be.kdg.kandoe.backend.dom.Session.SessionCard;
import be.kdg.kandoe.backend.dom.user.User;


/**
 * Or a Remark for a {@link Card} -> Remark
 */

public class Remark implements Serializable {

    private static final long serialVersionUID = -6951389912497759887L;
    private Long remarkId;
    private String text;
    private LocalDateTime timeStamp;
    private User user;
    private SessionCard card;

    public Remark() {
        this.timeStamp = LocalDateTime.now();
    }

    public Long getId() {
        return remarkId;
    }

    public void setId(Long remarkId) {
        this.remarkId = remarkId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDateTime getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(LocalDateTime timeStamp) {
        this.timeStamp = timeStamp;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public SessionCard getCard() {
        return card;
    }

    public void setCard(SessionCard card) {
        this.card = card;
    }
}


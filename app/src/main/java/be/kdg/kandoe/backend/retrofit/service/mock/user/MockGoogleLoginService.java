package be.kdg.kandoe.backend.retrofit.service.mock.user;

import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.retrofit.service.mock.MockService;
import be.kdg.kandoe.backend.retrofit.service.api.user.GoogleLoginService;
import retrofit2.Call;
import retrofit2.http.Body;

/**
 * Created by Ruben on 26/02/2017.
 */

public class MockGoogleLoginService extends MockService<GoogleLoginService> implements GoogleLoginService {
    public MockGoogleLoginService() {
    }

    @Override
    public Call<User> googleLogin(@Body User user) {
        user.setId(144L);
        return getDelegate().returningResponse(user).googleLogin(user);
    }
}

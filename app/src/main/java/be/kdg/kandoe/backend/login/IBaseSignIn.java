package be.kdg.kandoe.backend.login;


import be.kdg.kandoe.backend.dom.user.User;

@SuppressWarnings("unused")
public interface IBaseSignIn {

	User getUser();
	boolean isLoggedIn();
	void logOut();
	boolean isLoading();
	boolean logIn();
	boolean loginSilently();

}

package be.kdg.kandoe.backend.websocket;

import org.java_websocket.WebSocket;

import be.kdg.kandoe.backend.config.KandoeConfig;
import be.kdg.kandoe.utils.MyLog;
import ua.naiksoftware.stomp.Stomp;
import ua.naiksoftware.stomp.client.StompClient;

/**
 * Created by Ruben on 14/03/2017.
 */

public class WebSocketUtil {

    private static String getUrl(String endPoint) {
        return KandoeConfig.getBaseUrl(true)+endPoint+SUFFIX;
    }

    public interface EndPoint{
        String SESSION = getUrl("/session");
        String CHAT = getUrl("/chat");
    }

    public static class Topic {
        public static String session(long sessionId){
            return getTopic("session/"+sessionId);
        }
        public static String chat(long sessionId) {
            return getTopic("chat/"+sessionId);
        }
    }

    static String getTopic(String txt) {
        return "/topic/"+txt;
    }

    public static StompClient createStompClient(String endPoint) {
        MyLog.d(WebSocketUtil.class.getSimpleName(), "createStompClient: "+endPoint);
       return Stomp.over(WebSocket.class, endPoint);
    }

    private static final String SUFFIX = "/websocket";
}

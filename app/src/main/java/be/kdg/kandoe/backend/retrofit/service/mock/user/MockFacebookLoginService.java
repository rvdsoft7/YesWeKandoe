package be.kdg.kandoe.backend.retrofit.service.mock.user;

import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.retrofit.service.mock.MockService;
import be.kdg.kandoe.backend.retrofit.service.api.user.FacebookLoginService;
import retrofit2.Call;
import retrofit2.http.Body;

/**
 * Created by Ruben on 26/02/2017.
 */

public class MockFacebookLoginService extends MockService<FacebookLoginService> implements FacebookLoginService{
    public MockFacebookLoginService() {
    }

    @Override
    public Call<User> facebookLogin(@Body User user) {
        user.setId(133L);
        return getDelegate().returningResponse(user).facebookLogin(user);
    }
}

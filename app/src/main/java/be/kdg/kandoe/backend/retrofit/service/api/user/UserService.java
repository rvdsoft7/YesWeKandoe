package be.kdg.kandoe.backend.retrofit.service.api.user;

import be.kdg.kandoe.backend.dom.user.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface UserService {

	@SuppressWarnings("unused")
    @GET("users/{userid}")
	Call<User> getProfiel(@Path("userid") long userId);

	@POST("users/register")
	Call<User> basicRegister(@Body User user);

	@PUT("users/updateUser")
	Call<User> editProfile(@Body User user);

	@POST("users/login")
	Call<User> basicLogin(@Body User user);

	@GET("users/findByUsername")
	Call<User> findUserByUsername(@Query("username") String username);


}

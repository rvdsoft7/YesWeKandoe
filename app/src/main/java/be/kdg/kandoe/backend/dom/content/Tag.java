package be.kdg.kandoe.backend.dom.content;


import java.io.Serializable;

/**
 * A category of a {@link Theme}
 */
public class Tag implements Serializable {
    private static final long serialVersionUID = -7405788567544198446L;
    private Integer tagId;
    private String tagName;

    public Tag() {

    }

    public String getName() {
        return tagName;
    }

    public void setName(String name) {
        this.tagName = name;
    }

    public Integer getTagId() {
        return tagId;
    }

    public void setTagId(Integer tagId) {
        this.tagId = tagId;
    }

    @Override
    public String toString() {
        return tagName;
    }
}

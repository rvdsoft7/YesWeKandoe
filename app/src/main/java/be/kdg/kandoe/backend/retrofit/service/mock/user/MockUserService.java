package be.kdg.kandoe.backend.retrofit.service.mock.user;

import java.net.HttpURLConnection;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.retrofit.service.util.RetrofitHelper;
import be.kdg.kandoe.backend.retrofit.service.mock.MockService;
import be.kdg.kandoe.backend.retrofit.service.api.user.UserService;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.mock.Calls;

/**
 * Created by Ruben on 17/02/2017.
 */

public class MockUserService extends MockService<UserService> implements UserService {


    public MockUserService() {
    }




    @Override
    public Call<User> getProfiel(@Path("userid") long userId) {
        User u = MockUserRepo.getInstance().findUserById(userId);
        Response<User> response;
        if(u==null) {
           response = RetrofitHelper.createErrorResponse(HttpURLConnection.HTTP_NOT_FOUND,R.string.error_user_not_found);
        }else
            response = Response.success(u);
        return getDelegate().returning(Calls.response(response)).getProfiel(userId);
    }

    @Override
    public Call<User> basicRegister(@Body User user) {
        User u = MockUserRepo.getInstance().findUserByUsername(user.getUsername());
        Response<User> response;
        if(u!=null) {
            response = RetrofitHelper.createErrorResponse(HttpURLConnection.HTTP_CONFLICT,R.string.error_user_already_exists);
        }else
            response = Response.success(DefaultUser.create(user));
        return getDelegate().returning(Calls.response(response)).basicRegister(user);
    }

    @Override
    public Call<User> editProfile(@Body User user) {
        MockUserRepo repo = MockUserRepo.getInstance();
        User u = repo.updateUser(user);
      return getDelegate().returningResponse(u).editProfile(user);
    }

    @Override
    public Call<User> basicLogin(@Body User user) {
        Response<User> response;
        if (!MockUserRepo.getInstance().authenticate(user))
            response = RetrofitHelper.createErrorResponse(HttpURLConnection.HTTP_UNAUTHORIZED,R.string.error_user_unauthorized);
        else
            response = Response.success(MockUserRepo.getInstance().findUserByUsername(user.getUsername()));

        //moet calls.response anders type verkeerd
        return getDelegate().returning(Calls.response(response)).basicLogin(user);
    }

    @Override
    public Call<User> findUserByUsername(@Query("username") String username) {
        User user = MockUserRepo.getInstance().findUserByUsername(username);
        return getDelegate().returningResponse(user).findUserByUsername(username);
    }


}

package be.kdg.kandoe.backend.retrofit.service.util;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by Ruben on 15/02/2017.
 */

public abstract class MyCallback<T> implements Callback<T> {

    public final void onFailure(Call<T> call, Throwable t) {
        RetrofitException error = (RetrofitException) t;
        onFailure(call, error);
    }

    public abstract void onFailure(Call<T> call, RetrofitException t);
}

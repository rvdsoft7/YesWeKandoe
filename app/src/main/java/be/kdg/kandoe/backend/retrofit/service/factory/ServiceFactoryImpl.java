package be.kdg.kandoe.backend.retrofit.service.factory;

import be.kdg.kandoe.backend.login.kandoe.AccessToken;
import be.kdg.kandoe.backend.retrofit.service.util.AuthenticationInterceptor;
import be.kdg.kandoe.backend.retrofit.service.util.CustomRetrofitBuilder;
import be.kdg.kandoe.backend.retrofit.service.api.session.SessionService;
import be.kdg.kandoe.backend.retrofit.service.api.theme.ThemeService;
import be.kdg.kandoe.backend.retrofit.service.api.user.FacebookLoginService;
import be.kdg.kandoe.backend.retrofit.service.api.user.GoogleLoginService;
import be.kdg.kandoe.backend.retrofit.service.api.user.UserService;

/**
 * Created by Ruben on 19/02/2017.
 */

public class ServiceFactoryImpl implements IServiceFactory {

    private <T> T createService(Class<T> service) {
        return getDefBuilder().buildService(service);
    }

    CustomRetrofitBuilder getDefBuilder() {
        CustomRetrofitBuilder builder = new CustomRetrofitBuilder();
        if(AccessToken.getCurrentAccessToken()!=null) {
            builder.addInterceptor(new AuthenticationInterceptor(AccessToken.getCurrentAccessToken()));
        }
        return builder;
    }

    @Override
    public UserService createUserService() {

      return getDefBuilder().buildService(UserService.class);
    }

    @Override
    public ThemeService createThemeSevice() {
        return createService(ThemeService.class);
    }

    @Override
    public FacebookLoginService createFacebookLoginService() {
        return getDefBuilder().buildService(FacebookLoginService.class);
    }

    @Override
    public GoogleLoginService createGoogleLoginService() {
        return getDefBuilder().buildService(GoogleLoginService.class);
    }

    @Override
    public SessionService createSessionService() {
      return getDefBuilder().buildService(SessionService.class);
    }
}

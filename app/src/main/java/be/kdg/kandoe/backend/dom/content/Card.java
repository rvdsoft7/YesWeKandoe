package be.kdg.kandoe.backend.dom.content;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import java.io.Serializable;

/**
 * A Card for a {@link Theme}
 */
@SuppressWarnings("unused")
public class Card implements Serializable {

    private static final long serialVersionUID = -5799936262844829590L;
    private Long cardId;
    private String text;
    private String imageURL;
    @JsonProperty(access = Access.READ_ONLY)
    private Theme theme;

    public Card() {
    }

    public Long getId() {
        return cardId;
    }

    public Long getCardId() {
        return cardId;
    }

    public void setCardId(Long cardId) {
        this.cardId = cardId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public Theme getTheme() {
        return theme;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }

    @JsonGetter("themeId")
    public Long getThemeId() {
        if(theme==null)
            return null;

        return theme.getId();
    }
}

package be.kdg.kandoe.backend.login.facebook;

import android.content.Intent;
import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import be.kdg.kandoe.MyApplication;
import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.login.BaseSignIn;
import be.kdg.kandoe.backend.login.LoginListener2;
import be.kdg.kandoe.utils.ImageLoaderHelper;
import be.kdg.kandoe.utils.MyLog;

public class FacebookSignInHelper extends BaseSignIn<LoginListener2> {

	private CallbackManager callbackManager;

	private AccessToken mAccessToken;

	private FacebookPerson cache;

	private AccessTokenTracker accessTokenTracker;

	public static final List<String> PERMISSIONS = Arrays.asList("public_profile", "email");

	private boolean mLoading;

	public FacebookSignInHelper(LoginListener2 loginListener) {
		super(loginListener);
		FacebookSdk.sdkInitialize(MyApplication.getInstance());
		callbackManager = CallbackManager.Factory.create();

		LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

			@Override
			public void onSuccess(LoginResult loginResult) {
				mAccessToken = loginResult.getAccessToken();
				System.out.println("OnSuccess: " + loginResult);
				fetchPerson(loginResult.getAccessToken());
			}

			@Override
			public void onError(FacebookException error) {
				MyLog.e(getClass(), "Facebook error", error);
			}

			@Override
			public void onCancel() {
				System.out.println("onCancel: ");
			}
		});

		accessTokenTracker = new AccessTokenTracker() {

			@Override
			protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
				handleAccessTokenChange(currentAccessToken);
			}
		};

		accessTokenTracker.startTracking();

	}

	@Override
	public boolean isLoading() {
		return mLoading;
	}

	@Override
	public boolean loginSilently() {
		handleAccessTokenChange(AccessToken.getCurrentAccessToken());
		return true;
	}

	public void onLoggedIn() {
		FacebookPerson p = getPerson();
		if(p!=null) {
			getListener().onLoggedIn();
		}
	}

	void handleAccessTokenChange(AccessToken token) {
		mAccessToken = token;
		if(mAccessToken!=null) {
			fetchPerson(mAccessToken);
		}
	}
	
	public FacebookPerson getPerson() {
		return cache;
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		callbackManager.onActivityResult(requestCode, resultCode, data);
		onLoggedIn();
	}

	/**
	 * open permission activity
	 *
	 */
	public void tryLogin() {
		LoginManager.getInstance().logInWithReadPermissions(getContext(), PERMISSIONS);
		onLoggedIn();
	}

	private void fetchPerson(final AccessToken accessToken) {
		System.out.println("fetchPerson");
		mLoading = true;
		GraphRequest request = GraphRequest.newMeRequest(accessToken, (object, response) -> {
            System.out.println("onCompleted");
			mLoading = false;
            // Application code
            if (mAccessToken!=null && object != null) {
                fillData(object,accessToken);
                onLoggedIn();
            }
        });
		Bundle parameters = new Bundle();
		parameters.putString("fields", "email,first_name,last_name,id");
		request.setParameters(parameters);
		request.executeAsync();
	}

	@Override
	public User getUser() {
		FacebookPerson person = getPerson();
		User u = null;
		if (person != null) {
			u = new User();
			u.setLastName(person.getFamilyName());
			u.setFirstName(person.getSurName());
			u.setAvatarUrl(person.getAvatar(ImageLoaderHelper.DEFAULT_TARGET_SIZE).toString());
			u.setUsername(person.getEmail());
		}
		return u;
	}

	@Override
	public boolean isLoggedIn() {
		return cache != null;
	}

	@Override
	public boolean logIn() {
		tryLogin();
		return true;
	}
	
	public void fillData(JSONObject json, AccessToken accessToeken) {
		if(json!=null)  {
			getOrCreateCache().setInfo(json,accessToeken);
			MyLog.d(getClass(), cache.toString());
		}
	}
	
	private FacebookPerson getOrCreateCache() {
		if (cache == null)
			cache = new FacebookPerson();
		return cache;
	}

	@Override
	public void logOut() {
		System.out.println("logOut() facebook");
		if (isLoggedIn()) {
			System.out.println("logOut() facebook test");
			LoginManager.getInstance().logOut();
			mAccessToken = null;
			cache = null;
			getListener().onLoggedOut();
		}
	}

}

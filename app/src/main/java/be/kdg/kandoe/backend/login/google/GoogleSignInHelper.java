package be.kdg.kandoe.backend.login.google;

import android.content.Intent;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.SignInButton;

import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.login.BaseSignIn;
import be.kdg.kandoe.backend.login.LoginListener2;
import be.kdg.kandoe.frontend.activities.BaseActivity;
import be.kdg.kandoe.utils.MyLog;

public class GoogleSignInHelper extends BaseSignIn<LoginListener2> {

	GoogleAuthProcessor googleAuthProcessor;

	@Override
	public boolean isLoading() {
		return googleAuthProcessor.mLoading;
	}

	public GoogleSignInHelper(LoginListener2 loginListener) {
		super(loginListener);
		googleAuthProcessor = new GoogleAuthProcessor(this);
	}

	@Override
	public boolean loginSilently() {
		return googleAuthProcessor.loginSilently();
	}

	public GooglePerson getPerson() {
		return googleAuthProcessor.getPerson();
	}

	static final int RC_SIGN_IN = GoogleAuthProcessor.RC_SIGN_IN;

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == RC_SIGN_IN) {
			GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
			MyLog.d(getClass(), "handleSignInResult:" + result.isSuccess() + " status: "+result.getStatus().getStatusMessage());
			if (result.isSuccess()) {
				GoogleSignInAccount acct = result.getSignInAccount();
				googleAuthProcessor.fillData(acct);
			}
		}
	}
	
	@Override
	public void setActivity(BaseActivity activity) {
		super.setActivity(activity);
		googleAuthProcessor.onActivitySet();
	}
	
	public void connect() {
		googleAuthProcessor.connect();
	}
	
	public void disconnect() {
		googleAuthProcessor.disconnect();
	}

	@Override
	public User getUser() {
		GooglePerson person = googleAuthProcessor.getPerson();
		User u = null;
		if (person != null) {
			u = new User();
			u.setLastName(person.getFamilyName());
			u.setFirstName(person.getSurName());
			u.setAvatarUrl(person.getAvatar());
			u.setUsername(person.getEmail());
		}
		return u;
	}

	@Override
	public boolean isLoggedIn() {
		return googleAuthProcessor.getPerson()!=null;
	}

	@Override
	public void logOut() {
		if (isLoggedIn())
			googleAuthProcessor.signOut();
	}

	@Override
	public boolean logIn() {
		googleAuthProcessor.signIn();
		return true;
	}
}

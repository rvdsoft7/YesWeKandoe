package be.kdg.kandoe.backend.retrofit.service.util;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.concurrent.Executor;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

import be.kdg.kandoe.utils.Utils;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.CallAdapter;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

// Modified version of https://github.com/square/retrofit/blob/master/retrofit/src/main/java/retrofit/ExecutorCallAdapterFactory.java
public class ErrorHandlingExecutorCallAdapterFactory extends CallAdapter.Factory {
    private final Executor callbackExecutor;

    ErrorHandlingExecutorCallAdapterFactory(Executor callbackExecutor) {
        this.callbackExecutor = callbackExecutor;
    }

    public static CallAdapter.Factory create() {
        return new ErrorHandlingExecutorCallAdapterFactory(new MainThreadExecutor());
    }

    @Override
    public CallAdapter<Call<?>> get(Type returnType, Annotation[] annotations, Retrofit retrofit) {
        if (Utils.getRawType(returnType) != Call.class) {
            return null;
        }
        final Type responseType = getCallResponseType(returnType);
        return new CallAdapter<Call<?>>() {
            @Override
            public Type responseType() {
                return responseType;
            }

            @Override
            public <R> Call<R> adapt(Call<R> call) {
                return new ExecutorCallbackCall<>(callbackExecutor, call, retrofit);
            }
        };
    }

    static final class ExecutorCallbackCall<T> implements Call<T> {
        private final Executor callbackExecutor;
        private final Call<T> delegate;
        private Retrofit retrofit;

        ExecutorCallbackCall(Executor callbackExecutor, Call<T> delegate, Retrofit retrofit) {
            this.callbackExecutor = callbackExecutor;
            this.delegate = delegate;
            this.retrofit = retrofit;
        }

        @Override
        public void enqueue(Callback<T> callback) {
            delegate.enqueue(new ExecutorCallback<>(callbackExecutor, callback, retrofit));
        }

        @Override
        public boolean isExecuted() {
            return delegate.isExecuted();
        }

        @Override
        public Response<T> execute() throws IOException {
            return delegate.execute();
        }

        @Override
        public void cancel() {
            delegate.cancel();
        }

        @Override
        public boolean isCanceled() {
            return delegate.isCanceled();
        }

        @SuppressWarnings("CloneDoesntCallSuperClone") // Performing deep clone.
        @Override
        public Call<T> clone() {
            return new ExecutorCallbackCall<>(callbackExecutor, delegate.clone(), retrofit);
        }

        @Override
        public Request request() {
            return delegate.request();
        }
    }

    static final class ExecutorCallback<T> implements Callback<T> {
        private final Executor callbackExecutor;
        private final Callback<T> delegate;
        private final Retrofit retrofit;

        ExecutorCallback(Executor callbackExecutor, Callback<T> delegate, Retrofit retrofit) {
            this.callbackExecutor = callbackExecutor;
            this.delegate = delegate;
            this.retrofit = retrofit;
        }

        @Override
        public void onResponse(Call<T> call, Response<T> response) {
            if (response.isSuccessful()) {
                callbackExecutor.execute(() -> delegate.onResponse(call, response));
            } else {
                callbackExecutor.execute(() -> delegate.onFailure(call, RetrofitException.httpError(response.raw().request().url().toString(), response, retrofit)));
            }
        }

        @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
        @Override
        public void onFailure(Call<T> call, final Throwable t) {
            RetrofitException exception;
            if (t instanceof IOException) {
                exception = RetrofitException.networkError((IOException) t);
            } else {
                exception = RetrofitException.unexpectedError(t);
            }
            final RetrofitException finalException = exception;
            callbackExecutor.execute(() -> delegate.onFailure(call, finalException));
        }

    }

    public static class MainThreadExecutor implements Executor {
        private final Handler handler = new Handler(Looper.getMainLooper());

        @Override
        public void execute(@NonNull Runnable r) {
            handler.post(r);
        }
    }

    static Type getCallResponseType(Type returnType) {
        if (!(returnType instanceof ParameterizedType)) {
            throw new IllegalArgumentException(
                    "Call return type must be parameterized as Call<Foo> or Call<? extends Foo>");
        }
        final Type responseType = getSingleParameterUpperBound((ParameterizedType) returnType);

        // Ensure the Call response type is not Response, we automatically deliver the Response object.
        if (Utils.getRawType(responseType) == Response.class) {
            throw new IllegalArgumentException(
                    "Call<T> cannot use Response as its generic parameter. "
                            + "Specify the response body type only (e.g., Call<TweetResponse>).");
        }
        return responseType;
    }

    public static Type getSingleParameterUpperBound(ParameterizedType type) {
        Type[] types = type.getActualTypeArguments();
        if (types.length != 1) {
            throw new IllegalArgumentException(
                    "Expected one type argument but got: " + Arrays.toString(types));
        }
        Type paramType = types[0];
        if (paramType instanceof WildcardType) {
            return ((WildcardType) paramType).getUpperBounds()[0];
        }
        return paramType;
    }
}
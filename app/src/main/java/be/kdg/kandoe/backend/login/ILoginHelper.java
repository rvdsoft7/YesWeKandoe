package be.kdg.kandoe.backend.login;

import android.content.Intent;

import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.login.facebook.FacebookSignInHelper;
import be.kdg.kandoe.backend.login.google.GoogleSignInHelper;
import be.kdg.kandoe.backend.login.kandoe.KandoeSignInHelper;
import be.kdg.kandoe.frontend.activities.BaseActivity;

/**
 * Created by Ruben on 10/02/2017.
 */

@SuppressWarnings("ALL")
public interface ILoginHelper extends LoginListener{
    boolean isLoggedIn();
    boolean isLoading();
    void logOut();
    User getUser();
    void register(LoginListener l);

   void unregister(LoginListener l);

    void setActivity(BaseActivity activity);

    GoogleSignInHelper getGoogleSignInHelper();

    public FacebookSignInHelper getFacebookSignInHelper();

    public KandoeSignInHelper getDefaultSignInHelper();

    public void loginGoogle();

   void onActivityResult(int requestCode, int resultCode, Intent data);
}

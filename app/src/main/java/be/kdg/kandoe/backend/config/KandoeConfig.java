package be.kdg.kandoe.backend.config;

import be.kdg.kandoe.MyApplication;

/**
 * Created by Ruben on 14/03/2017.
 */

public class KandoeConfig {

    public static String getBaseUrl(boolean webSocket) {
        StringBuilder sb = new StringBuilder();
        sb.append(webSocket ? "ws" : "http");
        boolean useMaster = !MyApplication.isMockMode() && !MyApplication.isLocalhostMode();
        if (useMaster) {
            sb.append("s");
        }
        sb.append("://");
        if (!useMaster) {
            //reroute localhost from local pc
            sb.append("10.0.2.2");
            sb.append(":8080");
        } else {
            sb.append("yes-we-kandoe.herokuapp.com");
        }
        return sb.toString();
    }
}

package be.kdg.kandoe.backend.login;


import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.retrofit.service.util.ErrorResult;

public class SimpleLoginListener implements LoginListener {
	@Override
	public void onLoggedIn(User user) {

	}

	@Override
	public void onLoggedOut() {

	}

	@Override
	public void onRegistered(User user) {

	}

	@Override
	public void onProfileEdited(User user) {

	}

	@Override
	public void onAuthenticationFailed(ErrorResult errorResult) {

	}
}

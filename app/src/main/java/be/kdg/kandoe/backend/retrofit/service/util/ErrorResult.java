package be.kdg.kandoe.backend.retrofit.service.util;

import java.io.Serializable;

/**
 * Created by Ruben on 22/02/2017.
 */

public class ErrorResult  implements Serializable{

    private static final long serialVersionUID = 690945617617997092L;
    int code;

    String message;

    public ErrorResult() {

    }

    public ErrorResult(int code, String message) {
        this.message = message;
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "ErrorResult{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }
}

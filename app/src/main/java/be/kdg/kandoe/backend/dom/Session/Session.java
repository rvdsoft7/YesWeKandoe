package be.kdg.kandoe.backend.dom.Session;


import android.content.Context;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;

import org.threeten.bp.LocalDateTime;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.content.Theme;
import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.utils.Utils;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;


/**
 * A Session is a Kandoe Game, you can play a Session for a {@link Theme}.
 * A Session is an abstract class.
 */
public class Session implements Serializable {
    public static final int MIN_LEVEL = 0;
    public static final int MAX_LEVEL = 4;
    public static final int AMOUNT_OF_CIRCLES = (MAX_LEVEL - MIN_LEVEL) + 1;
    private static final long serialVersionUID = -8239732483685617363L;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Session session = (Session) o;

        return sessionId != null ? sessionId.equals(session.sessionId) : session.sessionId == null;

    }

    @Override
    public int hashCode() {
        return sessionId != null ? sessionId.hashCode() : 0;
    }

    private Long sessionId;
    private String name;
    private int minCards;
    private int maxCards;
    @JsonIgnore
    private Theme theme;
    private int maxTimeTurn; //in seconds
    private List<User> users = new ArrayList<>();
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private List<SessionCard> sessionCards = new ArrayList<>();
    private boolean commentaryAllowed = true;
    private boolean addingAdmitted = true;
    private long currentUser;
    private int currentRound;
    private boolean problem;
    private boolean gameOver;
    private long organiser;
    private Boolean pickingCards;

    public Session() {
    }

    public boolean isPickingCards() {
        return pickingCards != null && pickingCards
                || (pickingCards == null && (currentRound == 0 && !gameOver && startDate != null && LocalDateTime.now().plusSeconds(1).isAfter(startDate)));
    }

    public long getOrganiserId() {
        return organiser;
    }

    public long getCurrentUserId() {
        return currentUser;
    }

    public List<SessionCard> getOrganiserSessionCards() {
        return sessionCards;
    }

    public void setOrganiserSessionCards(List<SessionCard> organisatorSessionCards) {
        this.sessionCards = organisatorSessionCards;
    }

    @JsonIgnore
    public boolean isPlanned() {
        return getStartDate() == null || LocalDateTime.now().isBefore(getStartDate());
    }

    @JsonIgnore
    public List<SessionCard> getParticipantSessionCards() {
        return StreamSupport.stream(sessionCards).filter(c -> c.isSelected()).collect(Collectors.toList());
    }

    @JsonIgnore
    public List<SessionCard> getUnselectedSessionCards() {
        return StreamSupport.stream(sessionCards).filter(c -> !c.isSelected()).collect(Collectors.toList());
    }

    public User getCurrentUser() {
        return getUser(currentUser);
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser != null ? currentUser.getId() : -1;
    }

    public User getNextUser() {
        int index = getUsers().indexOf(currentUser);

        if (index == -1) {
            return getUsers().get(0);
        }
        int nextIndex = index + 1;
        if (nextIndex == getUsers().size())
            return null;

        return getUsers().get(nextIndex);
    }

    public Integer getCurrentRound() {
        return currentRound;
    }

    public void setCurrentRound(Integer currentRound) {
        this.currentRound = currentRound;
    }

    public boolean isProblem() {
        return problem;
    }

    public void setProblem(boolean problem) {
        this.problem = problem;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
    }

    public int getMaxTimeTurn() {
        return maxTimeTurn;
    }

    public void setMaxTimeTurn(int maxTimeTurn) {
        this.maxTimeTurn = maxTimeTurn;
    }

    public boolean isCommentaryAllowed() {
        return commentaryAllowed;
    }

    public void setCommentaryAllowed(boolean commentaryAllowed) {
        this.commentaryAllowed = commentaryAllowed;
    }

    public boolean isAddingAdmitted() {
        return addingAdmitted;
    }

    public void setAddingAdmitted(boolean addingAdmitted) {
        this.addingAdmitted = addingAdmitted;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(User... users) {
        setUsers(Arrays.asList(users));
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public boolean isParticipant(User u) {
        if (getUsers() == null || u == null)
            return false;

        return StreamSupport.stream(getUsers()).anyMatch(x -> x.equals(u));
    }

    public User getUser(Long id) {
        return StreamSupport.stream(getUsers()).filter(x -> x.getId() == id).findAny().orElse(null);
    }

    public void setSelected(List<SessionCard> cards) {
        StreamSupport.stream(cards).forEach(card -> {
            SessionCard sessionCardOrganisator = StreamSupport.stream(sessionCards).filter(c -> c.getId() == card.getId()).findAny().get();
            sessionCardOrganisator.setSelected(true);
        });
    }

    public int getMinCards() {
        return minCards;
    }

    public void setMinCards(int minCards) {
        this.minCards = minCards;
    }

    public int getMaxCards() {
        return maxCards;
    }

    public void setMaxCards(int maxCards) {
        this.maxCards = maxCards;
    }

    public Theme getTheme() {
        return theme;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }

    private Theme getOrCreateTheme() {
        if (theme == null)
            theme = new Theme();

        return theme;
    }

    @JsonSetter("themeName")
    public void setTheme(String themeName) {
        getOrCreateTheme().setThemeName(themeName);
    }

    @JsonSetter("themeId")
    public void setTheme(long themeId) {
        getOrCreateTheme().setThemeId(themeId);
    }

    @JsonGetter("themeId")
    public long getThemeId(){
        return getOrCreateTheme().getId();
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return sessionId;
    }

    public void setId(Long sessionId) {
        this.sessionId = sessionId;
    }

    public String getStartDateToString() {
        if (startDate == null)
            return null;
        return Utils.DATE_TIME_FORMATTER.format(startDate);
    }

    public String getEndDateToString() {
        if (endDate == null)
            return null;
        return Utils.DATE_TIME_FORMATTER.format(endDate);
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    @JsonIgnore
    public boolean isPlaying() {
        return currentRound > 0;
    }

    public void incrementRound() {
        this.currentRound++;
    }

    public User getOrganiser() {
        return getUser(organiser);
    }

    public void setOrganiser(User organiser) {

        this.organiser = organiser != null ? organiser.getId() : -1;
    }

    public String getSessionTypeName(Context context) {
        return isProblem() ? "Problem Circle" : "Chance Circle";
    }

    public String getSessionStatusName(Context context) {
        Session session = this;
        String data = null;
        if (session.isGameOver()) {
            data = context.getString(R.string.session_status_game_over);
        } else if (session.isPickingCards()) {
            data = context.getString(R.string.session_status_picking_cards);
        } else if (session.isPlaying()) {
            data = context.getString(R.string.session_status_playing);
        } else {
            return context.getString(R.string.not_available);
        }
        return data;
    }

    @JsonSetter("sessionCards")
    public void customSessionCardSetter(List<SessionCard> sessionCards) {
        this.sessionCards = sessionCards;
        if (this.sessionCards != null) {
            StreamSupport.stream(sessionCards).forEach(sessionCard -> sessionCard.setSession(this));
        }
    }
}

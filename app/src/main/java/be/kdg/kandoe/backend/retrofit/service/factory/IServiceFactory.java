package be.kdg.kandoe.backend.retrofit.service.factory;

import be.kdg.kandoe.backend.retrofit.service.api.session.SessionService;
import be.kdg.kandoe.backend.retrofit.service.api.theme.ThemeService;
import be.kdg.kandoe.backend.retrofit.service.api.user.FacebookLoginService;
import be.kdg.kandoe.backend.retrofit.service.api.user.GoogleLoginService;
import be.kdg.kandoe.backend.retrofit.service.api.user.UserService;

/**
 * Created by Ruben on 19/02/2017.
 */

public interface IServiceFactory {

    UserService createUserService();
    ThemeService createThemeSevice();
    FacebookLoginService createFacebookLoginService();
    GoogleLoginService createGoogleLoginService();
    SessionService createSessionService();
}

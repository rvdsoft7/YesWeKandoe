package be.kdg.kandoe.backend.retrofit.service.mock.session;

import org.threeten.bp.LocalDateTime;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

import be.kdg.kandoe.backend.dom.Session.SessionCard;
import be.kdg.kandoe.backend.dom.Session.Session;
import be.kdg.kandoe.backend.dom.content.Theme;
import be.kdg.kandoe.backend.retrofit.service.mock.user.MockUserRepo;
import java8.util.concurrent.ThreadLocalRandom;
import java8.util.stream.Collectors;
import java8.util.stream.RefStreams;
import java8.util.stream.StreamSupport;

/**
 * Created by Ruben on 21/02/2017.
 */

public class DefaultSession {

   static AtomicLong idCounter = new AtomicLong(1);


    public static List<Session> create(Theme theme) {
        return RefStreams.of(createSession(theme, 1), createSession(theme, 2), createSession(theme, 3), createSession(theme, 4)).collect(Collectors.toList());
    }

    public static Session createSession(Theme theme, long nummer) {
       final Session session = new Session();
        session.setId(idCounter.getAndIncrement());
        session.setName("Session "+nummer);
        session.setMaxCards(24);
        session.setMinCards(1);
        session.setTheme(theme);
        session.setProblem(ThreadLocalRandom.current().nextBoolean());
        List<SessionCard> sessionCards = StreamSupport.stream(theme.getCards()).map(x -> {
            SessionCard sessionCard = new SessionCard();
            sessionCard.setSession(session);
            sessionCard.setSessionCardId(x.getId());
            sessionCard.setCard(x);
            return sessionCard;
        }).collect(Collectors.toList());
        session.setOrganiserSessionCards(sessionCards);
        MockUserRepo repo = MockUserRepo.getInstance();
        session.setUsers(RefStreams.of(repo.findUserById(2), repo.findUserById(3)).collect(Collectors.toList()));
        Collections.shuffle(session.getUsers());
        session.setOrganiser(session.getUsers().get(0));
        LocalDateTime startDate = LocalDateTime.of((int) (2015+nummer), 01, 01, 1, 1, 1);
        LocalDateTime endDate = new Random().nextBoolean()? startDate.plusDays(1) : null;
        session.setStartDate(LocalDateTime.now().minusHours(1));
        session.setCurrentUser(session.getNextUser());
        session.setEndDate(null);
        return session;
    }
}

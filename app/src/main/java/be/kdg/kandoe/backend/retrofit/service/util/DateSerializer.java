package be.kdg.kandoe.backend.retrofit.service.util;

import java.io.IOException;

import org.threeten.bp.LocalDateTime;
import org.threeten.bp.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import android.annotation.SuppressLint;


public class DateSerializer extends StdSerializer<LocalDateTime> {

	public DateSerializer() {
		this(null);
	}

	public DateSerializer(Class<LocalDateTime> t) {
		super(t);
	}

	@Override
	public void serialize(LocalDateTime value, JsonGenerator gen, SerializerProvider provider) throws IOException {
		gen.writeObject(value.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
	}
}
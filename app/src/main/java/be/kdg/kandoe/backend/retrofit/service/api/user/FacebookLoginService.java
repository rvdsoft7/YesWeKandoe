package be.kdg.kandoe.backend.retrofit.service.api.user;

import be.kdg.kandoe.backend.dom.user.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Ruben on 26/02/2017.
 */

public interface FacebookLoginService {
    @POST("users/facebook")
    Call<User> facebookLogin(@Body User user);
}

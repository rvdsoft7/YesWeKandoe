package be.kdg.kandoe.backend.retrofit.service.util;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Ruben on 01/03/2017.
 */

public class HeaderRequestInterceptor implements Interceptor {
    private final String headerName;

    private final String headerValue;

    public HeaderRequestInterceptor(String headerName, String headerValue) {
        this.headerName = headerName;
        this.headerValue = headerValue;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();

        Request.Builder builder = original.newBuilder()
                .header(headerName, headerValue);

        Request request = builder.build();
        return chain.proceed(request);
    }
}

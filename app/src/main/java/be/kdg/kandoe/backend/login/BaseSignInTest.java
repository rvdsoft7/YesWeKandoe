package be.kdg.kandoe.backend.login;


import java.lang.ref.WeakReference;

import be.kdg.kandoe.frontend.activities.BaseActivity;

public abstract class BaseSignInTest implements IBaseSignIn {

	private WeakReference<BaseActivity> mActivity = new WeakReference<>(null);
	
	public BaseSignInTest() {
	}
	
	public void setActivity(BaseActivity activity) {
		this.mActivity = new WeakReference<BaseActivity>(activity);
	}
	
	public BaseActivity getContext() {
		return mActivity.get();
	}
}

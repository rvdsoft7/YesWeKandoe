package be.kdg.kandoe.backend.retrofit.service.api.session;


import java.util.List;

import be.kdg.kandoe.backend.dom.Session.Session;
import be.kdg.kandoe.backend.dom.content.Message;
import be.kdg.kandoe.backend.dom.content.Remark;
import be.kdg.kandoe.backend.dom.user.User;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface SessionService {

    @POST("sessions/{sessionId}/makeMove/{sessionCardID}")
    Call<Session> makeMove(@Path("sessionId")long sessionId,@Path("sessionCardID") long sessionCardId);

    @GET("sessions/{sessionId}")
    Call<Session> getSession(@Path("sessionId") long sessionId);

    @GET("sessions/{sessionId}/chat")
    Call<List<Message>> getChat(@Path("sessionId") long sessionId);

    @POST("sessions/{sessionId}/addMessage")
    Call<Message> uploadMessage(@Path("sessionId") long sessionId, @Body Message message);

    @POST("sessions/{sessionId}/pickCards")
    Call<Session> pickCards(@Path("sessionId") long sessionId, @Body Long[] sessionCardIds);

    @GET("sessions/byUserId/{userId}")
    Call<List<Session>> getSessionsByUserId(@Path("userId") long userId);

    @GET("sessions/byThemeId/{themeId}")
    Call<List<Session>> getSessionByThemeId(@Path("themeId") long themeId);

    @POST("sessions/create")
    Call<Session> createSession(@Body Session session);

    @POST("sessions/{sessionId}/addUser")
    Call<Session> addUser(@Path(("sessionId"))long sessionId, @Body User user);

    @POST("sessions/{sessionId}/startSession")
    Call<Session> beginSession(@Path(("sessionId"))long sessionId);

    @POST("sessions/{sessionId}/endSession")
    Call<Session> endSession(@Path(("sessionId"))long sessionId);

    @DELETE("sessions/{sessionId}/delete")
    Call<ResponseBody> deleteSession(@Path("sessionId") long sessionId);

    @POST("sessions/{sessionId}/sessionCard/{sessionCardId}/addRemark")
    Call<List<Remark>> uploadRemark(@Path("sessionId") long sessionId, @Path("sessionCardId") long sessionCardId, @Body Remark remark);

    @GET("sessions/{sessionId}/sessionCard/{sessionCardId}/remarks")
    Call<List<Remark>> getRemarks(@Path("sessionId") long sessionId, @Path("sessionCardId") long sessionCardId);


}
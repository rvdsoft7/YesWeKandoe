package be.kdg.kandoe.backend.retrofit.service.mock.user;

import be.kdg.kandoe.backend.dom.user.User;

/**
 * Created by Ruben on 19/02/2017.
 */

 class DefaultUser {
    private static final String DEF_PWD = getEncryptedPass();
    public static User create(long id) {
        User user = new User();
        user.setId(id);
        user.setFirstName("John");
        user.setLastName("Doe");
        user.setUsername(String.format("user.%d@domain.com", id-1));
        user.setPassword(DEF_PWD);
        return user;
    }

    private static String getEncryptedPass() {
        return "P4$$w0rd";
    }

    public static User create(User user) {
        MockUserRepo.getInstance().register(user);

       return MockUserRepo.getInstance().findUserByUsername(user.getUsername());

    }
}

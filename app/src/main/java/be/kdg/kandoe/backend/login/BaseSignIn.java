package be.kdg.kandoe.backend.login;

public abstract class BaseSignIn<L> extends BaseSignInTest implements IBaseSignIn {

	private L listener;
	
	public BaseSignIn(L loginListener) {
		setListener(loginListener);
	}
	
	public L getListener() {
		return listener;
	}
	
	public void setListener(L loginListener) {
		this.listener = loginListener;
	}
}

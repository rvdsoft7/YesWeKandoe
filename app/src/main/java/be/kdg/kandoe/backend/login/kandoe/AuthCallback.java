package be.kdg.kandoe.backend.login.kandoe;


import be.kdg.kandoe.backend.Cancellable;
import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.retrofit.service.util.MyCallback;
import be.kdg.kandoe.backend.retrofit.service.util.RetrofitException;
import retrofit2.Call;
import retrofit2.Response;

public abstract class AuthCallback extends MyCallback<User> implements Cancellable {

	AuthType authType;
	boolean mCancelled;

	public AuthCallback(AuthType authType) {
		super();
		this.authType = authType;
	}

	public void cancel() {
		mCancelled = true;
	}

	public boolean isCancelled() {
		return mCancelled;
	}

	abstract void onResponse(Call<User> call, Response<User> response, AuthType authType);

	abstract void onFailure(Call<User> call, RetrofitException throwable, AuthType authType);

	@Override
	public void onResponse(Call<User> call, Response<User> response) {
		if (!isCancelled())
			onResponse(call, response, authType);
	}

	@Override
	public void onFailure(Call<User> call, RetrofitException throwable) {
		if (!isCancelled())
			onFailure(call, throwable, authType);
	}

}

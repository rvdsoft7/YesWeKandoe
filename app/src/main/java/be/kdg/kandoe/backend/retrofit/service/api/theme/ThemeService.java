package be.kdg.kandoe.backend.retrofit.service.api.theme;


import java.util.List;

import be.kdg.kandoe.backend.dom.content.Theme;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ThemeService {

    @GET("themes/public")
    Call<List<Theme>> themes();

    @GET("themes/{themeid}")
    Call<Theme> getTheme(@Path("themeid") long themeId);

    @GET("themes/organiser/{organisatorId}")
    Call<List<Theme>> getThemesByOrganiser(@Path("organisatorId") long organisatorId);


    @GET("themes/search")
    Call<List<Theme>> filterThemes(@Query("keywords") String keywords);

}
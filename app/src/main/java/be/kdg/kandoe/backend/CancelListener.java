package be.kdg.kandoe.backend;

public class CancelListener implements Cancellable {
	
	private volatile boolean cancel;

	@SuppressWarnings("unused")
    public CancelListener() {
	}
	
	public boolean isCancelled() {
		return cancel;
	}
	
	public void cancel() {
		cancel = true;
	}
	

}

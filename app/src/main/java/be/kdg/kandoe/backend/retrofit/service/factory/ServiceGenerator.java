package be.kdg.kandoe.backend.retrofit.service.factory;

import be.kdg.kandoe.MyApplication;
import be.kdg.kandoe.backend.retrofit.service.factory.IServiceFactory;
import be.kdg.kandoe.backend.retrofit.service.factory.MockServiceFactoryImpl;
import be.kdg.kandoe.backend.retrofit.service.factory.ServiceFactoryImpl;

public class ServiceGenerator {

     public static final boolean MOCK_MODE = MyApplication.isMockMode();


    public static IServiceFactory getInstance(boolean mock) {
        return mock ? new MockServiceFactoryImpl() : new ServiceFactoryImpl();
    }
    public static IServiceFactory getInstance() {
       return getInstance(MOCK_MODE);
    }
}
package be.kdg.kandoe.backend.login.google;

import android.net.Uri;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

public class GooglePerson {
	
	public void setInfo(GoogleSignInAccount googleAccont) {
		setEmail(googleAccont.getEmail());
		setId(googleAccont.getId());
		setIdToken(googleAccont.getIdToken());
		Uri photo = googleAccont.getPhotoUrl();
		if (photo != null)
			setAvatar(photo.toString());
		setFamilyName(googleAccont.getFamilyName());
		setSurName(googleAccont.getGivenName());
		this.serverAuthCode = googleAccont.getServerAuthCode();
	}


	String serverAuthCode;
	
	public String getServerAuthCode() {
		return serverAuthCode;
	}
	public void setServerAuthCode(String serverAuthCode) {
		this.serverAuthCode = serverAuthCode;
	}

	String idToken;
	
	public String getIdToken() {
		return idToken;
	}
	public void setIdToken(String idToken) {
		this.idToken = idToken;
	}
	String id;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	String surName;
	public String getSurName() {
		return surName;
	}
	public void setSurName(String surName) {
		this.surName = surName;
	}
	String familyName;
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	String avatar;
	String email;
	

	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
}

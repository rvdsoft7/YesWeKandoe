package be.kdg.kandoe.backend.login;

import android.content.Intent;


import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.login.facebook.FacebookSignInHelper;
import be.kdg.kandoe.backend.login.google.GoogleSignInHelper;
import be.kdg.kandoe.backend.login.kandoe.KandoeSignInHelper;
import be.kdg.kandoe.backend.retrofit.service.util.ErrorResult;
import be.kdg.kandoe.frontend.activities.BaseActivity;
import be.kdg.kandoe.utils.MyLog;
import be.kdg.kandoe.utils.WeakListenerManager;

/**
 * Created by Ruben on 10/02/2017.
 */

public class LoginHelperImpl implements ILoginHelper {
    KandoeSignInHelper defaultHelper;
    WeakListenerManager<LoginListener> listeners = new WeakListenerManager<>();
    User mUser;
    static ILoginHelper instance;

    public static ILoginHelper getInstance() {
        if (instance == null) {
            MyLog.d(LoginHelperImpl.class, "created instance");
            instance = new LoginHelperImpl();
        }
        return instance;
    }

    public void register(LoginListener l) {
        listeners.register(l);
    }

    public void unregister(LoginListener l) {
        listeners.unregister(l);
    }

    private LoginHelperImpl() {
        defaultHelper = new KandoeSignInHelper(this);
    }

    public void setActivity(BaseActivity activity) {
        defaultHelper.setActivity(activity);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        defaultHelper.onActivityResult(requestCode, resultCode, data);
    }

    public GoogleSignInHelper getGoogleSignInHelper() {
        return defaultHelper.getGoogleSignInHelper();
    }

    public FacebookSignInHelper getFacebookSignInHelper() {
        return defaultHelper.getFacebookSignInHelper();
    }

    public KandoeSignInHelper getDefaultSignInHelper() {
        return defaultHelper;
    }

    public void loginGoogle() {
        defaultHelper.loginGoogle();
    }

    public void logAllOut() {
        defaultHelper.logOut();
    }

    @Override
    public void onLoggedIn(final User user) {
        mUser = user;
        System.out.println("OnLoggedIn " + getUser());
        listeners.invoke(listener -> listener.onLoggedIn(user));
    }

    @Override
    public void onLoggedOut() {
        System.out.println("onLoggedOut");
        mUser = null;
        listeners.invoke(listener -> listener.onLoggedOut());
    }

    public User getUser() {
        return mUser;
    }

    public boolean isLoggedIn() {
        if(mUser==null) {
            mUser = defaultHelper.getUser();
        }
        return mUser != null;
    }

    @Override
    public boolean isLoading() {
        return defaultHelper.isLoading();
    }

    public void logOut() {
        logAllOut();
    }

    @Override
    public void onAuthenticationFailed(final ErrorResult errorResult) {
        System.out.println("onFailedToLogin: "+errorResult);
        mUser = null;
        listeners.invoke(listener -> listener.onAuthenticationFailed(errorResult));

    }

    @Override
    public void onRegistered(final User user) {
        System.out.println("onRegistered: "+user);
        mUser =null;
        listeners.invoke(listener -> listener.onRegistered(user));
    }

    @Override
    public void onProfileEdited(final User user) {
        mUser = user;
        listeners.invoke(listener -> listener.onProfileEdited(user));
    }
}

package be.kdg.kandoe.backend.dom.content;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.threeten.bp.LocalDateTime;

import java.io.Serializable;

import be.kdg.kandoe.backend.dom.user.User;


public class Message implements Serializable {

    private static final long serialVersionUID = -6951389912497759887L;
    private Long messageId;

    @JsonProperty("from")
    private User user;
    private String text;
    @JsonProperty("time")
    private LocalDateTime timeStamp;

    public Message() {
    }

    public String getShortName() {
        return user.getShortName();
    }

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDateTime getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(LocalDateTime timeStamp) {
        this.timeStamp = timeStamp;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}


package be.kdg.kandoe.backend.retrofit.service.mock;

import retrofit2.mock.BehaviorDelegate;

/**
 * Created by Ruben on 18/02/2017.
 */

public abstract class MockService<T> {
    public MockService() {
    }

    private BehaviorDelegate<T> delegate;


    public BehaviorDelegate<T> getDelegate() {
        return delegate;
    }


    public void setDelegate(BehaviorDelegate<T> delegate) {
        this.delegate = delegate;
    }
}

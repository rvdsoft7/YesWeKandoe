package be.kdg.kandoe.backend.retrofit.service.mock.theme;

import android.text.TextUtils;

import java.util.List;

import be.kdg.kandoe.backend.dom.content.Theme;
import be.kdg.kandoe.backend.retrofit.service.mock.MockService;
import be.kdg.kandoe.backend.retrofit.service.api.theme.ThemeService;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;
import retrofit2.Call;
import retrofit2.http.Path;

/**
 * Created by Ruben on 17/02/2017.
 */

public class MockThemeService extends MockService<ThemeService> implements ThemeService {

    public MockThemeService() {
    }



    @Override
    public Call<List<Theme>> themes() {
        return getDelegate().returningResponse(DefaultThemes.create()).themes();
    }

    @Override
    public Call<Theme> getTheme(@Path("themeid") long themeId) {
        Theme theme = StreamSupport.stream(DefaultThemes.create()).filter(t->t.getId()==themeId).findAny().get();
        return getDelegate().returningResponse(theme).getTheme(themeId);
    }

    @Override
    public Call<List<Theme>> getThemesByOrganiser(@Path("organisatorId") long organisatorId) {
        List<Theme> themes = StreamSupport.stream(DefaultThemes.create()).filter(t->StreamSupport.stream(t.getOrganisers()).anyMatch(o->o.getId()==organisatorId)).collect(Collectors.toList());
        return getDelegate().returningResponse(themes).getThemesByOrganiser(organisatorId);
    }

    @Override
    public Call<List<Theme>> filterThemes(String keywords) {
        List<Theme> themes= StreamSupport.stream(DefaultThemes.create()).filter(t->(t.getName()+t.getDescription()+ TextUtils.join(", ", t.getTags())).contains(keywords)).collect(Collectors.toList());
        return getDelegate().returningResponse(themes).filterThemes(keywords);
    }

}

package be.kdg.kandoe.backend.retrofit.service.util;

import java.util.concurrent.TimeUnit;

import be.kdg.kandoe.backend.retrofit.service.mock.MockService;
import be.kdg.kandoe.utils.Utils;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.MockRetrofit;
import retrofit2.mock.NetworkBehavior;

/**
 * Created by Ruben on 18/02/2017.
 */

public class CustomRetrofitBuilder {
    private OkHttpClient.Builder client;
    private Retrofit.Builder retrofitBuilder;

    public CustomRetrofitBuilder() {
        client = RetrofitHelper.createHttpClientBuiler();
        retrofitBuilder = RetrofitHelper.builder();
    }

    public CustomRetrofitBuilder addInterceptor(Interceptor interceptor) {
       client.addInterceptor(interceptor);
        return this;
    }

    public CustomRetrofitBuilder baseUrl(String url) {
        retrofitBuilder.baseUrl(url);
        return this;
    }


    public <T> T buildService(Class<T> service) {
        return build().create(service);
    }

    public Retrofit build() {
        return retrofitBuilder.client(client.build()).build();
    }

    public <S, M extends MockService<S>> M buildMockService(Class<S> service, Class<M> mockService) {
        Retrofit r = build();
        NetworkBehavior networkBehavior = NetworkBehavior.create();
        networkBehavior.setFailurePercent(0);
        networkBehavior.setDelay(RetrofitHelper.MOCK_DELAY, TimeUnit.MILLISECONDS);
        MockRetrofit mockRetrofit = new MockRetrofit.Builder(r).networkBehavior(networkBehavior).build();
         BehaviorDelegate<S> delegate = mockRetrofit.create(service);
        M mockServiceObj =  Utils.createNewInstance(mockService);
        mockServiceObj.setDelegate(delegate);
        return mockServiceObj;
    }



    @SuppressWarnings("unused")
    public OkHttpClient.Builder getClient() {
        return client;
    }

    @SuppressWarnings("unused")
    public Retrofit.Builder getRetrofitBuilder() {
        return retrofitBuilder;
    }
}

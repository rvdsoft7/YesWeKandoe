package be.kdg.kandoe.backend.login.google;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.Scope;

import be.kdg.kandoe.MyApplication;
import be.kdg.kandoe.R;
import be.kdg.kandoe.frontend.activities.BaseActivity;
import be.kdg.kandoe.utils.MyLog;

/**
 * Created by sandeep on 4/5/16.
 */
public class GoogleAuthProcessor
		implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

	private static final String TAG = "GoogleAuthProcessor";
	public static final int RC_SIGN_IN = 9001;
	GoogleApiClient m_GoogleApiClient;
	GoogleSignInHelper helper;
	GooglePerson cache;
	boolean mLoading;

	public GooglePerson getPerson() {
		return cache;
	}

	GoogleAuthProcessor(GoogleSignInHelper googleSignInHelper) {
		helper = googleSignInHelper;
		GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
				.requestProfile()
                .requestServerAuthCode(MyApplication.getInstance().getResources().getString(R.string.google_server_client_id))
                .requestIdToken(MyApplication.getInstance().getResources().getString(R.string.google_server_client_id))
				.requestScopes(new Scope(Scopes.PROFILE))
				.requestEmail().build();
		m_GoogleApiClient = new GoogleApiClient.Builder(MyApplication.getInstance()).addOnConnectionFailedListener(this)
				.addApi(Auth.GOOGLE_SIGN_IN_API, gso).build();
	}

	public boolean loginSilently() {
		OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(m_GoogleApiClient);
		if (opr.isDone()) {
			// If the user's cached credentials are valid, the OptionalPendingResult will be "done"
			// and the GoogleSignInResult will be available instantly.
			GoogleSignInResult result = opr.get();
			handleSignInResult(result);
		} else {
			// If the user has not previously signed in on this device or the sign-in has expired,
			// this asynchronous branch will attempt to sign in the user silently.  Cross-device
			// single sign-on will occur in this branch.
			mLoading = true;
			opr.setResultCallback(googleSignInResult -> handleSignInResult(googleSignInResult));
		}
		return true;
	}
	
	public void disconnect() {
		m_GoogleApiClient.disconnect();
	}
	
	public void connect() {
		m_GoogleApiClient.connect();
	}

	void onActivitySet() {
	}

	public void signOut() {
		Auth.GoogleSignInApi.signOut(m_GoogleApiClient).setResultCallback(status -> {

            cache = null;
            helper.getListener().onLoggedOut();
        });
	}

	private BaseActivity getActivity() {
		return helper.getContext();
	}

	public void signIn() {
		Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(m_GoogleApiClient);
		getActivity().startActivityForResult(signInIntent, RC_SIGN_IN);
	}

	private GooglePerson getOrCreateCache() {
		if (cache == null)
			cache = new GooglePerson();
		return cache;
	}


	public void fillData(GoogleSignInAccount googleAccont) {
		getOrCreateCache().setInfo(googleAccont);
		MyLog.d(getClass(),cache.toString());
		mLoading = false;
		helper.getListener().onLoggedIn();
	}

	public void handleSignInResult(GoogleSignInResult result) {
		mLoading = false;
		Log.d(TAG, "handleSignInResult:" + result.isSuccess());
		if (result.isSuccess()) {
			// Signed in successfully, show authenticated UI.
			GoogleSignInAccount acct = result.getSignInAccount();
			fillData(acct);

		} else {
			// Signed out, show unauthentic
		}
	}

	@Override
	public void onConnected(Bundle bundle) {
		// Get user's information
		Log.d(TAG, "onConnected called");
		if (bundle != null) {
			Log.d(TAG, bundle.toString());
		}

	}

	@Override
	public void onConnectionSuspended(int i) {

	}

	@Override
	public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
		Log.d(TAG, "onConnectionFailed:" + connectionResult);
	}
}
package be.kdg.kandoe;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDexApplication;

import com.jakewharton.threetenabp.AndroidThreeTen;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.utils.L;

import be.kdg.kandoe.backend.login.LoginPrefsContract;
import be.kdg.kandoe.utils.MyLog;
import butterknife.ButterKnife;


/**
 * Created by Ruben on 09/02/2017.
 */

@SuppressWarnings("unused")
public class MyApplication extends MultiDexApplication{

    private static boolean DEVELOPER_MODE = true;
    private static boolean MOCK_MODE = false;
    private static boolean TESTING_MODE =false;
    private static boolean LOCALHOST_MODE = true;

    private static MyApplication instance;
    private static SharedPreferences prefs;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        initImageLoader(getApplicationContext());

        try {
            AndroidThreeTen.init(this);
        } catch(Throwable e) {
            MyLog.e(getClass(), null, e);
        }
        ButterKnife.setDebug(false);
    }


    public static MyApplication getInstance() {
        return instance;
    }


    public static SharedPreferences getLoginPrefs() {
        return instance.getSharedPreferences(LoginPrefsContract.NAME, Context.MODE_PRIVATE);
    }

    private static boolean isImageLoaderLoggingEnabled() {
        return isDeveloperMode() && true;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        ImageLoader.getInstance().clearMemoryCache();
    }


    public static void initImageLoader(Context context) {
        // This configuration tuning is custom. You can tune every option, you
        // may tune some of them,
        // or you can create default configuration by
        // ImageLoaderConfiguration.createDefault(this);
        // method.
        ImageLoaderConfiguration.Builder imageLoaderBuilder = new ImageLoaderConfiguration.Builder(context);
        imageLoaderBuilder.threadPriority(Thread.NORM_PRIORITY - 2);
        imageLoaderBuilder.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        imageLoaderBuilder.denyCacheImageMultipleSizesInMemory();
        imageLoaderBuilder.tasksProcessingOrder(QueueProcessingType.LIFO);
        imageLoaderBuilder.diskCacheFileCount(100);
        if (isImageLoaderLoggingEnabled()) {
            imageLoaderBuilder.writeDebugLogs();
        }

        L.writeLogs(isImageLoaderLoggingEnabled());

        ImageLoaderConfiguration config = imageLoaderBuilder.build();
        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config);

    }

    public static boolean isEmulatorMode() {
        return Build.FINGERPRINT.startsWith("generic")
                || Build.FINGERPRINT.startsWith("unknown")
                || Build.MODEL.contains("google_sdk")
                || Build.MODEL.contains("Emulator")
                || Build.MODEL.contains("Android SDK built for x86")
                || Build.MANUFACTURER.contains("Genymotion")
                || (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))
                || "google_sdk".equals(Build.PRODUCT);
    }

    public static final void setMockMode(boolean mock) {
        MOCK_MODE = mock;
    }

    public static final boolean isMockMode() {return MOCK_MODE;}

    public static final void setLocalhostMode(boolean localhost) {
        LOCALHOST_MODE = localhost;
    }

    public static final boolean isLocalhostMode() {return LOCALHOST_MODE;}


    public static final void setTestingMode(boolean testing) {
        TESTING_MODE = testing;
    }

    public static final boolean isTestingMode() {return TESTING_MODE;}

    public static boolean isDeveloperMode() {
        return DEVELOPER_MODE;
    }

    public static boolean isDeveloperMode(boolean debug) {
        return DEVELOPER_MODE && debug;
    }
}

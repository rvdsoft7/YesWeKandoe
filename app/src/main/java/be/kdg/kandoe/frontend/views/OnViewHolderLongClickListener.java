package be.kdg.kandoe.frontend.views;

import android.support.v7.widget.RecyclerView;

@SuppressWarnings("ALL")
public interface OnViewHolderLongClickListener {

	boolean onViewHolderLongClick(RecyclerView.ViewHolder viewHolder, int position, long id);
}

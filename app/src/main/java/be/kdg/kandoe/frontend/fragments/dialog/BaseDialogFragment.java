package be.kdg.kandoe.frontend.fragments.dialog;


import android.app.Dialog;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.widget.Toast;

import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.login.LoginHelperImpl;
import be.kdg.kandoe.backend.retrofit.service.util.ErrorResult;
import be.kdg.kandoe.backend.retrofit.service.util.RetrofitException;
import be.kdg.kandoe.utils.CallHelper;
import be.kdg.kandoe.utils.MyLog;
import be.kdg.kandoe.utils.StringUtils;
import be.kdg.kandoe.utils.Utils;
import retrofit2.Call;

public abstract class BaseDialogFragment extends DialogFragment {
	
	public BaseDialogFragment() {
	}

	CallHelper callHelper = new CallHelper();

	public boolean isFragmentReady() {
		MyLog.d(getClass(), StringUtils.format("isFragmentReady: %b %b", !isDetached(), getFragmentManager()!=null));
		return !isDetached() && getFragmentManager() != null;
	}

	protected void handleOnFailure(RetrofitException throwable) {
		handleOnFailure(throwable, true);
	}

	protected void handleOnFailure(RetrofitException throwable, boolean showMsg) {
		ErrorResult result = throwable.getErrorResult();
		MyLog.d(getClass(), "Retrofit error:::"+result, throwable);
		if(showMsg) {
			if (result != null && isFragmentReady())
				showMessage(result.getMessage());
		}

	}

	protected void loadData() {

    }

	public void manage(Call<?> call) {
        callHelper.add(call);
    }

	public boolean isLoggedIn() {
		return getLoggedInUser()!=null;
	}

	public User getLoggedInUser() {
		return LoginHelperImpl.getInstance().getUser();
	}
	@Override
	public void onActivityCreated(Bundle arg0) {
		super.onActivityCreated(arg0);
		Utils.hideKeyboard(getActivity());
		getActivity(). setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        loadData();
	}

    protected void showDialog(DialogFragment f)
	{
		FragmentTransaction ft = getChildFragmentManager().beginTransaction();
		// Create and show the dialog.
		DialogFragment newFragment = f;
		newFragment.show(ft, getClass().getName());
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
	    MyLog.d(getClass(), "onDestroy()");
	}
	
	protected Bundle getOrCreateArgs() {
		if (getArguments() == null) {
			setArguments(new Bundle());
		}
		return getArguments();
	}

	protected void cancelAllCalls() {
		MyLog.d(getClass(), "cancelAllCalls");
		callHelper.cancelAll();
	}

	protected <T> T getListener(Class<T> listenerType) {
		return Utils.getListener(this, listenerType);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		MyLog.d(getClass(), "onCreate()");
	}
	
	@Override
	public void onDestroyView()
	{
		MyLog.d(getClass(), "onDestroyView");
		cancelAllCalls();
	    Dialog dialog = getDialog();

	    // Work around bug: http://code.google.com/p/android/issues/detail?id=17423
	    if (dialog != null && getRetainInstance())
	        dialog.setDismissMessage(null);

	    super.onDestroyView();
	}

	void showMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }




}

package be.kdg.kandoe.frontend.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.widget.TextView;

import org.threeten.bp.LocalDateTime;
import org.threeten.bp.format.DateTimeFormatter;

import java.util.List;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.content.Message;
import butterknife.BindView;

public class MessageAdapter extends MyAdapter<Message> {

	static final DateTimeFormatter MESSAGE_TIME_TODAY_FORMATTER = DateTimeFormatter.ofPattern("HH:mm");
    static final DateTimeFormatter MESSAGE_TIME_YESTERDAY_FORMATTER = DateTimeFormatter.ofPattern("dd/MM HH:mm");
    LocalDateTime now = LocalDateTime.now();

	public MessageAdapter(Context context, List<Message> items) {
		super(context, R.layout.chat_row, items);
	}
	
	static class MessageHolder extends ViewHolder {
		@BindView(R.id.chatMessage)
        TextView chatMessage;

	}
	
	@Override
	protected ViewHolder createViewHolder() {
		return new MessageHolder();
	}
	@Override
	public void initItem(int position, Message item, ViewHolder holder) {
		MessageHolder messageHolder = (MessageHolder) holder;
        String text = item.getText();
        String time = getDate(item.getTimeStamp());
        String username = item.getShortName();
        CharSequence line = formatText(time,username,text);
        messageHolder.chatMessage.setText(line, TextView.BufferType.SPANNABLE);
	}

	String getDate(LocalDateTime dateTime) {
        if(dateTime.getDayOfYear() == now.getDayOfYear()) {
            return MESSAGE_TIME_TODAY_FORMATTER.format(dateTime);
        } else {
            return MESSAGE_TIME_YESTERDAY_FORMATTER.format(dateTime);
        }
    };

	private SpannableString getSpannableString(String data, int color, int spaces) {
        String padding = "";
        for (int i = 0; i < spaces; i++) {
            padding+=" ";
        }
      SpannableString str1= new SpannableString(data+padding);
        str1.setSpan(new ForegroundColorSpan(color), 0, str1.length(), 0);
        return str1;
    }

	private CharSequence formatText(String date, String username, String text) {
        SpannableStringBuilder builder = new SpannableStringBuilder();
    int padding = 2;
        SpannableString str1= getSpannableString(date, getContext().getResources().getColor(R.color.chat_hour_color), padding);
        builder.append(str1);

        SpannableString str2= getSpannableString(username, Color.BLACK, 0);
        str2.setSpan(new StyleSpan(Typeface.BOLD), 0, str2.length(), 0);
        builder.append(str2);

        SpannableString str3=getSpannableString(":", Color.BLACK, padding);
        builder.append(str3);

        SpannableString str4= getSpannableString(text, Color.BLACK, 0);
        builder.append(str4);

        return builder;
    }

}

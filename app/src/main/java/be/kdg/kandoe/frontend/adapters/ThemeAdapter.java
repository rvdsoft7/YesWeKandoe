package be.kdg.kandoe.frontend.adapters;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.wefika.flowlayout.FlowLayout;

import java.util.List;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.content.Theme;
import be.kdg.kandoe.utils.FlowLayoutHelper;
import butterknife.BindView;

public class ThemeAdapter extends MyAdapter<Theme> {


	public interface OnThemeItemOverflowClickListener {
		void onOverflowClicked(View v, int pos);
	}

    public OnThemeItemOverflowClickListener getOnThemeItemOverflowClickListener() {
        return onThemeItemOverflowClickListener;
    }

    public void setOnThemeItemOverflowClickListener(OnThemeItemOverflowClickListener onThemeItemOverflowClickListener) {
        this.onThemeItemOverflowClickListener = onThemeItemOverflowClickListener;
    }

    OnThemeItemOverflowClickListener onThemeItemOverflowClickListener;

	public ThemeAdapter(Context context, List<Theme> items) {
		super(context, R.layout.theme_row, items);
	}
	
	static class ThemeHolder extends ViewHolder {
		@BindView(R.id.themeText)
		TextView text;
		@BindView(R.id.themeDescription)
		TextView description;
		@BindView(R.id.themeTags)
		FlowLayout themeTags;
        @BindView(R.id.overflowMenu)
        View overflow;

	}
	
	@Override
	protected ViewHolder createViewHolder() {
		return new ThemeHolder();
	}
	@Override
	public void initItem(int position, Theme item, ViewHolder holder) {
		ThemeHolder themeHolder = (ThemeHolder) holder;
		themeHolder.text.setText(item.getName());
		themeHolder.description.setText(item.getDescription());
		new FlowLayoutHelper(themeHolder.themeTags).setTags(item.getTags());
		int drawable = !item.isPublicAllowed() ? R.drawable.ic_private : R.drawable.ic_public;
		themeHolder.text.setCompoundDrawablesWithIntrinsicBounds(0, 0, drawable, 0);
        if(getOnThemeItemOverflowClickListener()!=null) {
            themeHolder.overflow.setOnClickListener((v)->getOnThemeItemOverflowClickListener().onOverflowClicked(v, position));
        }
	}

}

package be.kdg.kandoe.frontend.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.facebook.login.widget.LoginButton;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.login.ILoginHelper;
import be.kdg.kandoe.backend.login.LoginHelperImpl;
import be.kdg.kandoe.backend.login.LoginListener;
import be.kdg.kandoe.backend.login.SimpleLoginListener;
import be.kdg.kandoe.backend.login.facebook.FacebookSignInHelper;
import be.kdg.kandoe.backend.login.kandoe.KandoeSignInHelper;
import be.kdg.kandoe.backend.retrofit.service.util.ErrorResult;
import be.kdg.kandoe.backend.retrofit.service.mock.user.MockUserRepo;
import be.kdg.kandoe.frontend.activities.BaseActivity;
import be.kdg.kandoe.frontend.views.MyEditText;
import be.kdg.kandoe.utils.FragmentHelper;
import be.kdg.kandoe.utils.StringUtils;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginFragment extends BaseFragment {

    @BindView(R.id.emailEdit)
    MyEditText emailEdit;
    @BindView(R.id.passwordEdit)
    MyEditText wachtwoordEdit;

    @BindView(R.id.registerButton)
    Button registreerButton;

    @BindView(R.id.loginButton)
    Button logInButton;

    @BindView(R.id.loginFacebook)
    LoginButton loginFacebook;

    @BindView(R.id.loginGoogle)
    View loginGoogle;

    ILoginHelper loginHelper;

    KandoeSignInHelper defaultSignInHelper;
    AwesomeValidation mValidator;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initBaseActivity(BaseActivity baseActivity, ActionBar actionBar) {
        super.initBaseActivity(baseActivity, actionBar);
        actionBar.setTitle(R.string.menu_sign_in);
        actionBar.setSubtitle("");

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        loginHelper.onActivityResult(requestCode, resultCode, data);
    }

    int getLayoutResId() {
        return R.layout.login;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(getLayoutResId(), container, false);
    }

    @OnClick(R.id.registerButton)
    public void registreer(View view) {
        RegistreerFragment nextFrag = RegistreerFragment.newInstance(emailEdit.getText().toString());
        new FragmentHelper(getSelf()).open(nextFrag);
    }


    void handleLoginButtonClick() {
        if (!mValidator.validate())
            return;
        clearErrorResult();
        setLoading(true);
        User login = new User(emailEdit.getText().toString(), wachtwoordEdit.getText().toString());
        defaultSignInHelper.logIn(login);
    }

    @OnClick(R.id.loginButton)
    public void login(View view) {
        handleLoginButtonClick();
    }

    LoginListener l = new SimpleLoginListener() {

        public void onAuthenticationFailed(ErrorResult errorResult) {
            setLoading(false);
            setErrorResult(errorResult);
            showMessage(errorResult.getMessage());
        }

        @Override
        public void onLoggedIn(User user) {
            setLoading(false);
            showMessage(getString(R.string.login_success));
        }


    };

    @OnClick(R.id.loginGoogle)
    public void loginGooglePlus(View v) {
        loginHelper.loginGoogle();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        loginHelper.unregister(l);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        ButterKnife.bind(this, view);
        mValidator = new AwesomeValidation(ValidationStyle.TEXT_INPUT_LAYOUT);
        mValidator.addValidation(getTextInputLayout(emailEdit), StringUtils.EMAIL_PATTERN, getString(R.string.validation_email));
        mValidator.addValidation(getTextInputLayout(wachtwoordEdit), RegexTemplate.NOT_EMPTY, getString(R.string.validation_password));
        loginHelper = LoginHelperImpl.getInstance();
        loginHelper.register(l);
        loginHelper.setActivity(getBaseActivity());
        defaultSignInHelper = loginHelper.getDefaultSignInHelper();
        User user = MockUserRepo.getInstance().findUserById(2);
        emailEdit.setText(user.getUsername());
        wachtwoordEdit.setText(user.getPassword());

        loginFacebook.setFragment(this);
        loginFacebook.setReadPermissions(FacebookSignInHelper.PERMISSIONS);
    }

}


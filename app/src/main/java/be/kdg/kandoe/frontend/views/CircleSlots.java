package be.kdg.kandoe.frontend.views;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import be.kdg.kandoe.utils.MyLog;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by Ruben on 06/03/2017.
 */

@SuppressWarnings("unused")
public class CircleSlots {

    public List<Coordinate> getSlots() {

        return positions;
    }

    private List<Coordinate> positions = new ArrayList<>();

    public CircleSlots(int size, int radiusOffset) {
        this(size, size, radiusOffset);
    }
    public CircleSlots(int width, int height, int radiusOffset) {
        this.width = width;
        this.height = height;
        this.radiusOffset = radiusOffset;
        fillPositions();
        sort();
    }

    void sort() {
       final Coordinate min = StreamSupport.stream(getSlots()).min((a, b) -> (int) (a.getY() - b.getY())).orElse(null);
       int index = positions.indexOf(min);
        List<Coordinate> coordinates = new ArrayList<>();
        for(int i = index;i<positions.size();i++) {
            coordinates.add(positions.get(i));
        }

        for(int i = 0;i<index;i++) {
            coordinates.add(positions.get(i));
        }

        if(this.positions.size()!=coordinates.size()) {
            throw new IllegalStateException();
        }
        Collections.reverse(coordinates);
        List<Coordinate> data= StreamSupport.stream(coordinates).distinct().collect(Collectors.toList());
       this.positions =data;
    }



    private int width;
    private int height;
    private int radiusOffset;

    public int getRadius() {
        return radius;
    }

    private int radius;

    private void fillPositions() {
        int cx = width/2;
        int cy = height/2;
         radius = Math.min(width, height) / 2 - radiusOffset;
        if(radius<=0) {
            throw new IllegalStateException("radius<=0");
        }
        fillPositions(cx, cy, radius, 40);
    }

    void fillPositions(int centerX, int centerY, int r, int slots) {
        positions.clear();
        float step = (float) (2f*Math.PI/((float)slots));
        int i = 0;
        System.out.println("fillPositions: slots: "+slots + " r: "+r+ " cX: "+centerX + " cY: "+centerY);
        for(float theta=0;  theta < 2*Math.PI;  theta+=step) {
            float x = (float) (centerX + r*Math.cos(theta));
            float y = (float) (centerY - r*Math.sin(theta));
            Coordinate c = new Coordinate(x,y);
            c.setIndex(i);
            positions.add(c);
            i++;
        }

    }

}

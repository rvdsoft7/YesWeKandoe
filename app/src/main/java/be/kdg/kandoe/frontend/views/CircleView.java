package be.kdg.kandoe.frontend.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

@SuppressWarnings("unused")
public class CircleView extends RelativeLayout {
    private int circleColor = 0;

    public int getStrokeWidth() {
        return strokeWidth;
    }

    private int strokeWidth = 1;
    private Paint paint;
    private int size;
    private boolean fill;

    Paint getPaint() {
        return paint;
    }

    public CircleView(Context context) {
        super(context);
        init(context, null);
    }

    public CircleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        this.setWillNotDraw(false);
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
    }

    public void setStrokeWidth(int strokeWidth) {
        if (this.strokeWidth == strokeWidth)
            return;
        this.strokeWidth = strokeWidth;
        invalidate();
    }

    public void setCircleColor(int circleColor) {

        if (this.circleColor == circleColor)
            return;
        this.circleColor = circleColor;
        invalidate();
    }

    public int getCircleColor() {
        return circleColor;
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int[] circleData = getCircleData();
        int cx = circleData[0];
        int cy = circleData[1];
        int radius = circleData[2];

        paint.setColor(circleColor);
        paint.setStrokeWidth(strokeWidth);
        canvas.drawCircle(cx, cy, radius, paint);
    }


    int[] getCircleData() {
        int w = getWidth();
        int h = getHeight();

        int usableWidth = w;
        int usableHeight = h;


        int cx = (usableWidth / 2);
        int cy = (usableHeight / 2);
        int radius = Math.min(usableWidth, usableHeight) / 2 - strokeWidth * 2;
        return new int[]{cx, cy, radius};
    }


    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public boolean isFill() {
        return fill;
    }

    public void setFill(boolean fill) {
        this.fill = fill;
        if (fill)
            paint.setStyle(Paint.Style.FILL_AND_STROKE);
        else
            paint.setStyle(Paint.Style.STROKE);
        invalidate();
    }
}
package be.kdg.kandoe.frontend.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.assist.ImageSize;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.content.Card;
import be.kdg.kandoe.utils.ImageLoaderHelper;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ruben on 12/03/2017.
 */

public class ThemeCardView extends FrameLayout {

    @BindView(R.id.icon)
    ImageView icon;
    @BindView(R.id.cardText)
    TextView cardText;

    private Card card;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ThemeCardView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs, defStyleAttr);
    }

    public ThemeCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs, defStyleAttr);
    }

    public ThemeCardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public ThemeCardView(Context context) {
        super(context);
        init(null, 0);
    }

    void init(AttributeSet attrs, int defStyleAttr) {
        initViews();
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
        handleCard();
    }

    void handleCard() {
        cardText.setText(card==null ? "Kaart Naam" : card.getText());
        if (!isInEditMode() && card!=null)
            ImageLoaderHelper.displayImage(card.getImageURL(), icon, null);
        else
            icon.setImageResource(R.drawable.ic_launcher);
    }



    void initViews() {
        View v = LayoutInflater.from(getContext()).inflate(R.layout.theme_card_layout, this, true);
        ButterKnife.bind(this, v);
        handleCard();
    }
}

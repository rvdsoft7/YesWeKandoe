package be.kdg.kandoe.frontend.views;


import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

public class MyViewHolder extends RecyclerView.ViewHolder {

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


        public void setOnViewHolderClickListener(final OnViewHolderClickListener onClickListener) {

            itemView.setOnClickListener(v -> onClickListener.onViewHolderClick(MyViewHolder.this, getLayoutPosition(), getItemId()));
        }

        public void setOnViewHolderLongClickListener(final OnViewHolderLongClickListener onLongClickListener) {
            itemView.setOnLongClickListener(v -> onLongClickListener.onViewHolderLongClick(MyViewHolder.this, getLayoutPosition(), getItemId()));
        }

    }
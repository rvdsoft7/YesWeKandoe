package be.kdg.kandoe.frontend.fragments;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.PopupMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.content.Theme;
import be.kdg.kandoe.frontend.activities.BaseActivity;
import be.kdg.kandoe.frontend.adapters.ThemeAdapter;
import be.kdg.kandoe.frontend.adapters.ThemeAdapter.OnThemeItemOverflowClickListener;
import be.kdg.kandoe.utils.CustomOnMenuItemClickListener;
import be.kdg.kandoe.utils.FragmentHelper;
import butterknife.BindView;
import butterknife.ButterKnife;

public class BaseThemeOverzichtFragment extends BaseFragment implements OnItemClickListener {


	@BindView(R.id.listView)
	ListView listView;
	@BindView(R.id.emptyTV)
	TextView emptyTv;



	static final String KEY_THEMES = "themes";

	List<Theme> getThemes() {
		return (List<Theme>) getOrCreateArgs().getSerializable(KEY_THEMES);
	}

	void setThemes(List<Theme> themes) {
		getOrCreateArgs().putSerializable(KEY_THEMES, new ArrayList<>(themes));
	}

    void setAdapter(List<Theme> items) {
        ThemeAdapter adapter =  new ThemeAdapter(getActivity(), items==null ? new ArrayList<>() : items);
		adapter.setOnThemeItemOverflowClickListener((v, pos) -> openMenu(v, pos, 0));
        listView.setAdapter(adapter);
    }



    ThemeAdapter getAdapter() {
        return (ThemeAdapter) listView.getAdapter();
    }

    void refreshAdapter() {
        resetAdapter();
    }

    void resetAdapter() {
        setAdapter(getThemes());
    }

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

	}

	@Override
	protected void initBaseActivity(BaseActivity baseActivity, ActionBar ab) {
		ab.setTitle(R.string.menu_search_themes);
		ab.setSubtitle("");
	}


	private void openMenu(View v, int pos, long id) {
		PopupMenu p = new PopupMenu(getActivity(), v);
		p.setOnMenuItemClickListener(new CustomOnMenuItemClickListener(pos, id) {

			@Override
			public boolean onMenuItemClick(MenuItem item, int pos, long id) {
				return handleOnMenuItemClick(item, pos, id);
			}
		});
		p.getMenuInflater().inflate(R.menu.popup_theme, p.getMenu());
		p.show();
	}

	void showSessions(int position) {
		Theme t = getAdapter().getItem(position);
		new FragmentHelper(getBaseActivity()).open(SessieOverzichtFragment.newInstance(t));
	}

	void showDetails(int pos) {
		Theme t = getAdapter().getItem(pos);
		ThemeDetailFragment f = ThemeDetailFragment.newInstance(t);
		showDialog(f);
	}


	private boolean handleOnMenuItemClick(MenuItem item, int pos, long id) {
		int itemId = item.getItemId();
		switch (itemId) {
			case R.id.menu_show_info:
				showDetails(pos);
				break;
		}
		return true;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		ButterKnife.bind(this, view);
		listView.setEmptyView(emptyTv);

		listView.setOnItemClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		showSessions(position);
	}
}

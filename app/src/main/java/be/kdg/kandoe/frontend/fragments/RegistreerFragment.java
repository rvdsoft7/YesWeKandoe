package be.kdg.kandoe.frontend.fragments;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.basgeekball.awesomevalidation.utility.RegexTemplate;

import java.util.Locale;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.login.ILoginHelper;
import be.kdg.kandoe.backend.login.LoginHelperImpl;
import be.kdg.kandoe.backend.login.LoginListener;
import be.kdg.kandoe.backend.login.SimpleLoginListener;
import be.kdg.kandoe.backend.login.kandoe.KandoeSignInHelper;
import be.kdg.kandoe.backend.retrofit.service.util.ErrorResult;
import be.kdg.kandoe.backend.retrofit.service.mock.user.MockUserRepo;
import be.kdg.kandoe.frontend.activities.BaseActivity;
import be.kdg.kandoe.frontend.views.MyEditText;
import be.kdg.kandoe.utils.StringUtils;
import butterknife.BindView;
import butterknife.OnClick;


public class RegistreerFragment extends BaseRegistreerFragment {

    @BindView(R.id.registerButton)
    Button registreerButton;


    @BindView(R.id.emailEdit)
    MyEditText emailEdit;
    ILoginHelper loginHelper;
    KandoeSignInHelper defaultSignInHelper;
    static final String KEY_EMAIL = "email";


    public static RegistreerFragment newInstance(String email) {
        RegistreerFragment r = new RegistreerFragment();
        r.getOrCreateArgs().putString(KEY_EMAIL, email);
        return r;
    }

    LoginListener l = new SimpleLoginListener() {

        public void onAuthenticationFailed(ErrorResult errorResult) {
            setLoading(false);
            setErrorResult(errorResult);
            showMessage(errorResult.getMessage());
        }

        @Override
        public void onRegistered(User user) {
            setLoading(false);
            showMessage(getString(R.string.register_success));
        }


    };


    @Override
    protected void initBaseActivity(BaseActivity baseActivity, ActionBar actionBar) {
        super.initBaseActivity(baseActivity, actionBar);
        actionBar.setTitle("Registreer");
        actionBar.setSubtitle("");

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        loginHelper.unregister(l);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.registreer, container, false);
    }

    @OnClick(R.id.registerButton)
    public void registreer(View view) {
        if (!mValidator.validate())
            return;
        User u = generateUser();
        u.setUsername(emailEdit.getText().toString().toLowerCase(Locale.US));

        clearErrorResult();
        setLoading(true);
        defaultSignInHelper.register(u);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mValidator.addValidation(getTextInputLayout(emailEdit), StringUtils.EMAIL_PATTERN, getString(R.string.validation_email));
        mValidator.addValidation(getTextInputLayout(wachtwoordEdit),
                RegexTemplate.NOT_EMPTY, getString(R.string.validation_password));
        if (savedInstanceState == null) {
            emailEdit.setText(getArguments().getString(KEY_EMAIL));
        }

        wachtwoordEdit.setText(MockUserRepo.getInstance().findUserById(2).getPassword());
        wachtwoordEdit2.setText(wachtwoordEdit.getText());
        voornaamEdit.setText("Ruben");
        achternaamEdit.setText("Van Dooren");
        loginHelper = LoginHelperImpl.getInstance();
        loginHelper.register(l);
        loginHelper.setActivity(getBaseActivity());
        defaultSignInHelper = loginHelper.getDefaultSignInHelper();
    }

}

package be.kdg.kandoe.frontend.activities;


import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import java.util.List;

import be.kdg.kandoe.R;
import be.kdg.kandoe.frontend.fragments.dialog.ProgressDialogFragment;
import be.kdg.kandoe.utils.MyLog;
import be.kdg.kandoe.utils.PauseHandler;
import be.kdg.kandoe.utils.PauseHandlerImpl;
import java8.util.stream.StreamSupport;

public abstract class BaseActivity extends AppCompatActivity {

	private Handler handler = new Handler();
	private PauseHandler pauseHandler = new PauseHandlerImpl();
    Boolean loading;

    static final String KEY_LOADING ="loading";

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(loading!=null)
        outState.putBoolean(KEY_LOADING, loading);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState!=null && loading==null)
            loading = savedInstanceState.getBoolean(KEY_LOADING);
    }

    public Fragment getTopFragment() {
        List<Fragment> fragentList = getSupportFragmentManager().getFragments();
        if(fragentList==null)
            return null;
        Fragment top = null;
        for (int i = fragentList.size() -1; i>=0 ; i--) {
            top = fragentList.get(i);
            if (top != null) {
                return top;
            }
        }
        return top;
    }

    public <T> T getCurrentFragment() {
        return (T) getTopFragment();
    }


    public void setLoading(Boolean loading) {
		if(this.loading!=null && this.loading.equals(loading))
			return;
		MyLog.d(getClass(),"setLoading: "+loading);
        this.loading = loading;
        if(loading)
        showProgressDialog(getString(R.string.loading));
        else
            hideProgressDialog();
    }

    public <T> T getFragment(Class<T> clazz) {
		return (T) getSupportFragmentManager().findFragmentByTag(clazz.getName());
	}

    public boolean isLoading() {
        return loading!=null?loading:false;
    }


    public void showDialog(DialogFragment f) {
		showDialog(f, f.getClass().getName());
	}

	public void showDialog(final DialogFragment f, final String tag) {
		showDialog(f, tag, null);
	}

	public void showDialog(final DialogFragment f, final String tag, final Runnable onShow) {
		pauseHandler.post2(() -> {
            // DialogFragment.show() will take care of adding the fragment
            // in a transaction. We also want to remove any currently
            // showing
            // dialog, so make our own transaction and take care of that
            // here.
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Fragment prev = getSupportFragmentManager().findFragmentByTag(tag);
            if (prev != null) {
                ft.remove(prev);
            }

            // Create and show the dialog.
            DialogFragment newFragment = f;
            newFragment.show(ft, tag);

            if (onShow != null) {
                onShow.run();
            }
        }, tag);
	}

	public void showMessageLong(String message) {
		getSnackbar(message, Snackbar.LENGTH_LONG).show();
	}

	public void showMessage(String message) {
		showMessageLong(message);
	}

	public Snackbar getSnackbar(String message, int length) {
		View root = findViewById(R.id.content_frame);
		Snackbar s = Snackbar.make(root, message, length);
		s.addCallback(new Snackbar.Callback(){

		});
		return s;
	}


	static final String DEFAULT_TAG ="test";
	
	public void postOnResume(Runnable r) {
		postOnResume(r, DEFAULT_TAG);
	}
	
	public void postOnResume(Runnable r, String tag) {
		pauseHandler.post2(r, tag);
	}

	protected final Handler getHandler() {
		return handler;
	}

	protected BaseActivity getSelf() {
		return this;
	}
	
	@Override
	protected void onPostResume() {
		super.onPostResume();
		MyLog.d(getClass(), "onPostResume");
		pauseHandler.resume();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		MyLog.d(getClass(), "onPause");
		pauseHandler.pause();
	}

    public ProgressDialogFragment getProgressDialog() {
        return (ProgressDialogFragment) getSupportFragmentManager().findFragmentByTag(ProgressDialogFragment.class.getName());
    }


    public void showProgressDialog(String message) {
        hideProgressDialog();
        ProgressDialogFragment f = ProgressDialogFragment.newInstance(message);
        showDialog(f, ProgressDialogFragment.class.getName());
    }

    public void hideProgressDialog() {
        postOnResume(()-> {
            ProgressDialogFragment f = getProgressDialog();
            MyLog.d(getClass(), "hideProgressDialog: "+f);
            if(f!=null) {
                f.dismissAllowingStateLoss();
            }
        });
    }

}

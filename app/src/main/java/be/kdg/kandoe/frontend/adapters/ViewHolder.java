package be.kdg.kandoe.frontend.adapters;

import android.view.View;
import butterknife.ButterKnife;

public class ViewHolder {
	
	@SuppressWarnings("unused")
	View root;

	
	public ViewHolder init(View view) {
		this.root = view;
		ButterKnife.bind(this, view);
		return this;
	}
}

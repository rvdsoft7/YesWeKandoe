package be.kdg.kandoe.frontend.fragments.dialog;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import be.kdg.kandoe.MyApplication;
import be.kdg.kandoe.R;
import be.kdg.kandoe.utils.MyLog;
import be.kdg.kandoe.utils.Utils;

@SuppressWarnings("unused")
public class MessageDialogFragment extends BaseDialogFragment{

	private static final String TAG = MessageDialogFragment.class.getSimpleName();
	public static final String DIALOG_ID = "dialog_id";
	public final static String TITLE = "title";
	public final static String IS_HTML = "is_html";
	public final static String MESSAGE = "message";
	public final static String ICON = "icon";
	public final static String CANCELABLE = "cancelable";
	public final static String NEUTRAL_BTN_TEXT = "neutral_btn_text";
	public final static String POSITIVE_BTN_TEXT = "positive_btn_text";
	public final static String NEGATIVE_BTN_TEXT = "negative_btn_text";
	public final static String NEUTRAL_BTN_ENABLED = "neutral_btn_enabled";
	public final static String POSITIVE_BTN_ENABLED = "positive_btn_enabled";
	public final static String NEGATIVE_BTN_ENABLED = "negative_btn_enabled";
	
	public static MessageDialogFragment newInstance(Fragment fragment) {
		return newInstance(fragment, 0);
	}

	public static MessageDialogFragment newInstance(Fragment fragment, int dialogId) {
		MessageDialogFragment f = new MessageDialogFragment();
		f.setTargetFragment(fragment, 0);
		f.setDialogId(dialogId);
		return f;
	}


	public String getMessage() {
		return getOrCreateArgs().getString(MESSAGE);
	}

	public void setDialogId(int id) {
		getOrCreateArgs().putInt(DIALOG_ID, id);
	}

	public void setPositiveButton(int textResId) {
		setPositiveButton(MyApplication.getInstance().getString(textResId));
	}

	public void setPositiveButton(String text) {
		getOrCreateArgs().putString(POSITIVE_BTN_TEXT, text);
	}
	
	public void setPositiveButtonEnabled(boolean enabled) {
		getOrCreateArgs().putBoolean(POSITIVE_BTN_ENABLED, enabled);
	}
	
	public void setNegativeButtonEnabled(boolean enabled) {
		getOrCreateArgs().putBoolean(NEGATIVE_BTN_ENABLED, enabled);
	}

	public void setNeutralButtonEnabled(boolean enabled) {
		getOrCreateArgs().putBoolean(NEUTRAL_BTN_ENABLED, enabled);
	}


	public void setNegativeButton(int textResId) {
		setNegativeButton(MyApplication.getInstance().getString(textResId));
	}

	public void setNegativeButton(String text) {
		getOrCreateArgs().putString(NEGATIVE_BTN_TEXT, text);
	}

	public void setNeutralButton(int textResId) {
		setNeutralButton(MyApplication.getInstance().getString(textResId));
	}

	public void setNeutralButton(String text) {
		getOrCreateArgs().putString(NEUTRAL_BTN_TEXT, text);
	}

	public void setCancelable(boolean cancelable) {
		super.setCancelable(cancelable);
		getOrCreateArgs().putBoolean(CANCELABLE, cancelable);
	}

	public void setIcon(int resId) {
		getOrCreateArgs().putInt(ICON, resId);
	}

	public void setTitle(int resId) {
		setTitle(MyApplication.getInstance().getString(resId));
	}

	public void setMessage(int resId) {
		setMessage(MyApplication.getInstance().getString(resId));
	}
	
	public void setMessage(CharSequence msg) {
		setMessage(msg, false);
	}

	public void setMessage(CharSequence msg, boolean isHtml) {
		getOrCreateArgs().putCharSequence(MESSAGE, msg);
		getArguments().putBoolean(IS_HTML, isHtml);
	}

	public void setTitle(CharSequence title) {
		getOrCreateArgs().putCharSequence(TITLE, title);
	}

	@SuppressLint("InflateParams")
	public static AlertDialog.Builder setBigMessage(Context context, AlertDialog.Builder builder, CharSequence message, boolean isHtml) {
		final View view = LayoutInflater.from(context).inflate(R.layout.dialog_message_layout, null);
		TextView tv = ((TextView) view.findViewById(R.id.tv_message));
		tv.setText(message);
		if(isHtml) {
			tv.setMovementMethod(LinkMovementMethod.getInstance());
		}
		builder.setView(view);
		return builder;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceStsate) {
		MyLog.d(getClass(), "onCreateDialog()");
		boolean cancelable = getArguments().getBoolean(CANCELABLE, false);
		setCancelable(cancelable);
		CharSequence title = getArguments().getCharSequence(TITLE);
		CharSequence message = getArguments().getCharSequence(MESSAGE);
		String posBtnText = getArguments().getString(POSITIVE_BTN_TEXT);
		String negBtnText = getArguments().getString(NEGATIVE_BTN_TEXT);
		String neutBtnText = getArguments().getString(NEUTRAL_BTN_TEXT);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setIcon(getArguments().getInt(ICON))
				.setTitle(title)
				.setCancelable(cancelable);
		boolean isHtml = getArguments().getBoolean(IS_HTML);
		if (message != null && (message.length() > 100 || isHtml))
			setBigMessage(getActivity(), builder, message, isHtml);
		else
			builder.setMessage(message);
		
		if (posBtnText != null) {
			builder.setPositiveButton(posBtnText, getArguments().getBoolean(POSITIVE_BTN_ENABLED, true) ? getOnClickListener() : null);
		}
		if (negBtnText != null) {
			builder.setNegativeButton(negBtnText, getArguments().getBoolean(NEGATIVE_BTN_ENABLED, true) ? getOnClickListener() : null);
		}
		if (neutBtnText != null) {
			builder.setNeutralButton(neutBtnText,  getArguments().getBoolean(NEUTRAL_BTN_ENABLED, true) ? getOnClickListener(): null);
		}
		return builder.create();
	}
	
	protected DialogInterface.OnClickListener getOnClickListener() {
		return new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				handleOnClick(which);
			}
		};
	}
	
	protected void handleOnClick(int which) {
		MyLog.d(TAG, "handleOnClick");
		MessageDialogFragmentListener listener =Utils.getListener(this, MessageDialogFragmentListener.class);
		if(listener!=null) {
			listener.onDialogButtonClick(MessageDialogFragment.this, getOrCreateArgs().getInt(DIALOG_ID), which);
		}
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}

	public interface MessageDialogFragmentListener {
		void onDialogButtonClick(MessageDialogFragment f, int dialogId, int which);
	}
}
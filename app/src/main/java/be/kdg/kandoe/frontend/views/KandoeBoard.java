package be.kdg.kandoe.frontend.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.Session.SessionCard;
import be.kdg.kandoe.backend.dom.Session.Session;
import be.kdg.kandoe.utils.MyLog;
import be.kdg.kandoe.utils.Utils;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;


/**
 * Created by Ruben on 05/03/2017.
 */

public class KandoeBoard extends RelativeLayout {
    List<KandoeCircle> circles = new ArrayList<>();
    List<KandoeCard> cards = new ArrayList<>();
    OnCircleClickListener onCircleClickListener;
    OnKandoeCardClickListener onKandoeCardClickListener;
    private int cardSize;

    public OnKandoeCardLongClickListener getOnKandoeCardLongClickListener() {
        return onKandoeCardLongClickListener;
    }

    @SuppressWarnings("unused")
    public void setOnKandoeCardLongClickListener(OnKandoeCardLongClickListener onKandoeCardLongClickListener) {
        this.onKandoeCardLongClickListener = onKandoeCardLongClickListener;
    }

    private OnKandoeCardLongClickListener onKandoeCardLongClickListener;

    @SuppressWarnings("unused")
    public OnDataChangedListener getOnDataChangedListener() {
        return onDataChangedListener;
    }

    public void setOnDataChangedListener(OnDataChangedListener onDataChangedListener) {
        this.onDataChangedListener = onDataChangedListener;
    }

    private OnDataChangedListener onDataChangedListener;

    public KandoeBoard(Context context) {
        this(context, null);
    }

    public KandoeBoard(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public List<KandoeCard> getAllCards() {
        Collections.sort(cards);
        return StreamSupport.stream(cards).collect(Collectors.toList());
    }


    void deleteCard(KandoeCard card) {
        cards.remove(card);
        removeView(card);
    }

    void disableCard(KandoeCard card) {
        card.detachFromCircle();
    }

    public void reset(List<SessionCard> sessionCards) {
        MyLog.d(getClass(), "EenTest RemovingCards");
        StreamSupport.stream(cards).forEach(card -> disableCard(card));
        MyLog.d(getClass(), "EenTest looping state");
        StreamSupport.stream(sessionCards).forEach(sessionCard -> {
            MyLog.d(getClass(), "EenTest looping state: "+sessionCard);
            boolean attached = false;
            for(KandoeCard kandoeCard : cards) {
                if(kandoeCard.isDetachedFromCircle()) {
                    MyLog.d(getClass(), "updateSessionCard: "+kandoeCard);
                    updateSessionCard(sessionCard, kandoeCard);
                    attached = true;
                    break;
                }
            }
            if(!attached) {
                addSessionCard(sessionCard);
            }
        });
        StreamSupport.stream(cards).forEach(c->{
            if(c.isDetachedFromCircle())
                deleteCard(c);
        });

        updateCards();
        StreamSupport.stream(cards).forEach(card->{
            MyLog.d(getClass(),"findViewById"+card);
            if(findViewById(card.getId())==null){
                MyLog.d(getClass(), "AddView: "+card);
                addView(card);
            }
        });
        triggerDataSetChanged();
    }

    public void setCircleColor(int color) {
        StreamSupport.stream(circles).forEach(x -> x.setCircleColor(color));
    }


    void triggerDataSetChanged() {
        if (onDataChangedListener != null) {
            onDataChangedListener.onDataSetChanged();
        }
    }

    public List<KandoeCard> getCards(int level) {
        return StreamSupport.stream(cards).filter(x->x.getLevel()==level).collect(Collectors.toList());
    }

    public void updateCards() {
        MyLog.d(getClass(), "StartupdateCards");
        Collections.sort(cards);
        StreamSupport.stream(circles).forEach(c->c.resetSlots());
        for (int level = Session.MIN_LEVEL; level <= Session.MAX_LEVEL; level++) {
            List<KandoeCard> values = getCards(level);
            for(KandoeCard card : values) {
                KandoeCircle circle = card.getCircle();
                MyLog.d(getClass(), "updateCards: "+card + " Circle: "+circle);
                List<Coordinate> positions = circle.getPositions();
                float spaceBetweenF = (float) positions.size() / (float) values.size();
                int spaceBetween = (int) Math.floor(spaceBetweenF);
                for (int i = 0; i < positions.size(); i += spaceBetween) {
                    Coordinate coordinate = positions.get(i);
                    if (circle.hasFreeSlot(coordinate)) {
                        card.setCoordinate(coordinate);
                        circle.putSlot(coordinate, card);
                        updateLayoutParams(card);
                       break;
                    }
                }
            }

        }

        MyLog.d(getClass(), "EndUpdateCards");
    }


    void updateLayoutParams(KandoeCard kandoeCard) {
        applyLayoutParams((LayoutParams) kandoeCard.getLayoutParams(), kandoeCard, kandoeCard.getCoordinate());
    }

    void applyLayoutParams(LayoutParams lp, KandoeCard kandoeCard, Coordinate coordinate) {
        MyLog.d(getClass(), "applyLayoutParams: "+kandoeCard);
        if(lp==null) {
            lp = new RelativeLayout.LayoutParams(cardSize, cardSize);
        }
        kandoeCard.setCoordinate(coordinate);
        int topMarginOld = lp.topMargin;
        int leftMarginOld = lp.leftMargin;
        lp.topMargin = (int) coordinate.getY() - cardSize / 2 + kandoeCard.getCircle().getTop();
        lp.leftMargin = (int) coordinate.getX() - cardSize / 2 + kandoeCard.getCircle().getLeft();
        //only request layout if changed.
        if (topMarginOld != lp.topMargin || leftMarginOld != lp.leftMargin)
            kandoeCard.setLayoutParams(lp);
    }


    private void updateSessionCard(SessionCard card, KandoeCard kandoeCard) {
        kandoeCard.setCircle(getCircle(card.getLevel()));
        kandoeCard.setCard(card);
    }

    private void addSessionCard(SessionCard card) {
        MyLog.d(getClass(), "addSessionCard: "+card);
        KandoeCard kandoeCard = new KandoeCard(getContext());
        kandoeCard.setId(View.generateViewId());
        updateSessionCard(card, kandoeCard);
        kandoeCard.setOnClickListener((v) -> {
            //setHighlighted(true);
            if (getOnKandoeCardClickListener() != null) {
                getOnKandoeCardClickListener().onClick((KandoeCard) v, ((KandoeCard) v).getCircle());
            }
        });

        kandoeCard.setOnLongClickListener((v) -> {
            if (getOnKandoeCardLongClickListener() != null) {
                return getOnKandoeCardLongClickListener().onLongClick((KandoeCard) v, ((KandoeCard) v).getCircle());
            }
            return false;
        });
        cards.add(kandoeCard);
    }


    KandoeCircle createCircle(int size) {
        KandoeCircle c = new KandoeCircle(getContext());
        int stroke = convertDipToPixels(10);
        c.setStrokeWidth(stroke);
        if(isInEditMode())
        c.setCircleColor(getResources().getColor(R.color.kansen_circle_color));
        c.setId(View.generateViewId());
        LayoutParams params = new LayoutParams(size, size);
        params.addRule(CENTER_IN_PARENT);
        c.setLayoutParams(params);
        return c;
    }

    public KandoeCircle getCircle(int index) {
        return circles.get(index);
    }

    void createCircles() {
        int width = getMeasuredWidth();
        int maxSize = width;
        MyLog.d(getClass(), "createCircles: maxSize"+maxSize);
        if (isInEditMode()) {
            maxSize = 750;
        }
        int size = maxSize;
        for (int i = 0; i < Session.AMOUNT_OF_CIRCLES; i++) {
            final int index = i;
            final KandoeCircle c = createCircle(size);
            c.setKandoeBoard(this);
            c.setLevel(i);
            c.setSize(size);
            if(c.getLevel()==Session.MAX_LEVEL) {
                c.setFill(true);
            }
            if (onCircleClickListener != null)
                c.setOnClickListener(v -> onCircleClickListener.onClick((KandoeCircle) v));

            circles.add(c);
//            if(i==0) {
//                c.setHighlighted(true);
//            }
            addView(c);
            size -= (maxSize * 0.19);
        }
    }


    @SuppressWarnings("unused")
    public OnCircleClickListener getOnCircleClickListener() {
        return onCircleClickListener;
    }

    @SuppressWarnings("unused")
    public void setOnCircleClickListener(OnCircleClickListener onCircleClickListener) {
        this.onCircleClickListener = onCircleClickListener;
    }

    public OnKandoeCardClickListener getOnKandoeCardClickListener() {
        return onKandoeCardClickListener;
    }

    public void setOnKandoeCardClickListener(OnKandoeCardClickListener onKandoeCardClickListener) {
        this.onKandoeCardClickListener = onKandoeCardClickListener;
    }


    public interface OnDataChangedListener {
        void onDataSetChanged();
    }

    public interface OnKandoeCardClickListener {
        void onClick(KandoeCard card, KandoeCircle circle);
    }

    @SuppressWarnings("unused")
    public interface OnKandoeCardLongClickListener {
        boolean onLongClick(KandoeCard card, KandoeCircle circle);
    }

    @SuppressWarnings("unused")
    public interface OnCircleClickListener {
        void onClick(KandoeCircle circle);
    }

    void setUpLayout() {
        createCircles();
    }

    int convertDipToPixels(int dip) {
        return Utils.convertDipToPixels(dip, getResources().getDisplayMetrics());
    }

    void init() {
        cardSize = Utils.convertDipToPixels(30, getResources().getDisplayMetrics());
        if (isInEditMode())
            setUpLayout();
        else {
            addOnLayoutChangeListener(new OnLayoutChangeListener() {
                @Override
                public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                    removeOnLayoutChangeListener(this);
                    setUpLayout();
                }
            });
        }
    }

}

package be.kdg.kandoe.frontend.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.Session.SessionCard;
import be.kdg.kandoe.backend.dom.Session.Session;
import be.kdg.kandoe.backend.dom.content.Card;
import be.kdg.kandoe.backend.dom.content.Theme;
import be.kdg.kandoe.frontend.activities.BaseActivity;
import be.kdg.kandoe.frontend.fragments.dialog.BaseDialogFragment;
import be.kdg.kandoe.frontend.fragments.dialog.RemarkOverzichtDailogFragment;
import be.kdg.kandoe.frontend.views.MyViewHolder;
import be.kdg.kandoe.frontend.views.ThemeCardView;
import butterknife.BindView;
import butterknife.ButterKnife;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by Ruben on 07/03/2017.
 */

public class CardOverzichtFragment extends BaseDialogFragment {


    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;


    public static CardOverzichtFragment newInstance(Theme t) {
        CardOverzichtFragment fragment = new CardOverzichtFragment();
        fragment.setThema(t);
        return fragment;
    }

    public static CardOverzichtFragment newInstance(Session s) {
        CardOverzichtFragment fragment = new CardOverzichtFragment();
        fragment.setSession(s);
        return fragment;
    }



    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        if(!getShowsDialog()) {
            BaseActivity baseActivity = (BaseActivity) getActivity();
           initBaseActivity(baseActivity,baseActivity.getSupportActionBar());
        }
    }

    protected void initBaseActivity(BaseActivity baseActivity, ActionBar actionBar) {
        actionBar.setTitle(R.string.card_overzicht_title);
        String subtitle = getThema()!=null ? getThema().getName() : getSession().getName();
        actionBar.setSubtitle(subtitle);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getAdapter() != null)
            getAdapter().notifyDataSetChanged();
    }

    static final String KEY_THEME = "theme";

    void setThema(Theme theme) {
        getOrCreateArgs().putSerializable(KEY_THEME, theme);
    }

    Theme getThema() {
        return (Theme) getArguments().getSerializable(KEY_THEME);
    }

    static final String KEY_SESSION = "session";

    void setSession(Session session) {
        getOrCreateArgs().putSerializable(KEY_SESSION, session);
    }

    Session getSession() {
        return (Session) getArguments().getSerializable(KEY_SESSION);
    }


    List<SessionCard> getSessionCards() {
        return getSession().getOrganiserSessionCards();
    }

    List<Card> getCards() {
        if (getSession() != null) {
            return StreamSupport.stream(getSessionCards()).map(SessionCard::getCard).collect(Collectors.toList());
        } else
            return getThema().getCards();
    }

    void initViews(Bundle savedInstanceState) {
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        GridLayoutManager glm = new GridLayoutManager(getActivity(), 3);
        recyclerView.setLayoutManager(glm);
        recyclerView.setAdapter(new MyAdapter(getActivity()));
    }

    MyAdapter getAdapter() {
        return (MyAdapter) recyclerView.getAdapter();
    }

    class CustomViewHolder extends MyViewHolder {

        @BindView(R.id.text)
        ThemeCardView text;

        public CustomViewHolder(View itemView) {
            super(itemView);
        }
    }

    class MyAdapter extends RecyclerView.Adapter<CustomViewHolder> {


        public MyAdapter(Context context) {
            inflater = LayoutInflater.from(context);
        }

        LayoutInflater inflater;


        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = inflater.inflate(R.layout.card_overzicht_row, parent, false);
            return new CustomViewHolder(v);
        }

        @Override
        public void onBindViewHolder(CustomViewHolder holder, int position) {
            Card card = getCards().get(position);
            holder.text.setCard(card);
            holder.setOnViewHolderClickListener((viewHolder, position1, id) -> {
                if (getSession() != null)
                    showRemarks(viewHolder.itemView, position1);
            });
        }

        @Override
        public int getItemCount() {
            return getCards().size();
        }
    }


    void showRemarks(View v, int postition) {
        SessionCard card = getSessionCards().get(postition);
        RemarkOverzichtDailogFragment f = RemarkOverzichtDailogFragment.newInstance(card);
        showDialog(f);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.card_overzicht_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initViews(savedInstanceState);
    }
}

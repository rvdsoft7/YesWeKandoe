package be.kdg.kandoe.frontend.fragments;

import android.animation.Animator;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import be.kdg.kandoe.MyApplication;
import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.Session.Session;
import be.kdg.kandoe.backend.dom.Session.SessionCard;
import be.kdg.kandoe.backend.retrofit.service.util.MyCallback;
import be.kdg.kandoe.backend.retrofit.service.util.RetrofitException;
import be.kdg.kandoe.backend.retrofit.service.util.RetrofitHelper;
import be.kdg.kandoe.backend.retrofit.service.factory.ServiceGenerator;
import be.kdg.kandoe.backend.retrofit.service.api.session.SessionService;
import be.kdg.kandoe.backend.websocket.WebSocketUtil;
import be.kdg.kandoe.backend.websocket.WebSocketUtil.EndPoint;
import be.kdg.kandoe.backend.websocket.WebSocketUtil.Topic;
import be.kdg.kandoe.frontend.activities.BaseActivity;
import be.kdg.kandoe.frontend.fragments.dialog.CardChooserDialogFragment;
import be.kdg.kandoe.frontend.fragments.dialog.MessageDialogFragment;
import be.kdg.kandoe.frontend.fragments.dialog.RemarkOverzichtDailogFragment;
import be.kdg.kandoe.frontend.fragments.dialog.UserOverzichtDailogFragment;
import be.kdg.kandoe.frontend.views.KandoeBoard;
import be.kdg.kandoe.frontend.views.KandoeCard;
import be.kdg.kandoe.frontend.views.KandoeCircle;
import be.kdg.kandoe.frontend.views.MyViewHolder;
import be.kdg.kandoe.frontend.views.OnViewHolderClickListener;
import be.kdg.kandoe.frontend.views.OnViewHolderLongClickListener;
import be.kdg.kandoe.frontend.views.ThemeCardView;
import be.kdg.kandoe.utils.CustomOnMenuItemClickListener;
import be.kdg.kandoe.utils.FragmentHelper;
import be.kdg.kandoe.utils.MyLog;
import be.kdg.kandoe.utils.StringUtils;
import butterknife.BindView;
import butterknife.ButterKnife;
import java8.util.stream.StreamSupport;
import retrofit2.Call;
import retrofit2.Response;
import ua.naiksoftware.stomp.client.StompClient;


/**
 * Created by Ruben on 06/03/2017.
 */

public class KandoeBoardFragment extends BaseFragment implements CardChooserDialogFragment.CardChooserFragmentListener, MessageDialogFragment.MessageDialogFragmentListener {

    static final String KEY_SESSION = " session";
    private static final String TAG = KandoeBoardFragment.class.getSimpleName();

    SessionService sessionService = ServiceGenerator.getInstance().createSessionService();
    Call<Session> mSessionCall;
    private Handler handler = new Handler();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        stompClient = WebSocketUtil.createStompClient(EndPoint.SESSION);
        stompClient.lifecycle().subscribe(lifecycleEvent -> {
            switch (lifecycleEvent.getType()) {

                case OPENED:
                    MyLog.d(getClass(), "Stomp connection opened");
                    break;

                case ERROR:
                    MyLog.e(getClass(), "Error", lifecycleEvent.getException());
                    break;

                case CLOSED:
                    MyLog.d(getClass(), "Stomp connection closed");
                    break;
            }
        });
    }




    void loadSession() {
        setLoading(true);
        mSessionCall = sessionService.getSession(getSession().getId());
        manage(mSessionCall);
        mSessionCall.enqueue(new MyCallback<Session>() {
            @Override
            public void onFailure(Call<Session> call, RetrofitException t) {
                if(!call.isCanceled()) {
                    setLoading(false);
                    handleOnFailure(t);
                }
            }

            @Override
            public void onResponse(Call<Session> call, Response<Session> response) {
                setSession(response.body());
                if(!call.isCanceled()) {
                    setLoading(false);
                    onLoaded();
                }
            }
        });
    }

    void handlePayload(String data) {
        Session session = RetrofitHelper.readValue(data, Session.class);
        setSession(session);
        onSessionUpdate(getSession());
    }


    void onLoaded() {
        refreshIndicators();
        int color = ContextCompat.getColor(getActivity(), getCircleColorRedId());
        kandoeBoard.setCircleColor(color);
        kandoeBoard.post(new Runnable() {
            @Override
            public void run() {
                onSessionUpdate(getSession());
            }
        });

    }

    private int getCircleColorRedId() {
        return getSession().isProblem() ? R.color.problemen_circle_color : R.color.kansen_circle_color;
    }

    @SuppressWarnings("unused")
    public static KandoeBoardFragment newInstance(long sessionId) {
        KandoeBoardFragment fragment = new KandoeBoardFragment();
        Session session = new Session();
        session.setId(sessionId);
        fragment.setSession(session);
        return fragment;
    }

    public static KandoeBoardFragment newInstance(Session session) {
        KandoeBoardFragment fragment = new KandoeBoardFragment();
        fragment.setSession(session);
        return fragment;
    }

    @BindView(R.id.roundTv)
    TextView roundTv;

    @BindView(R.id.typeTv)
    TextView typeTv;

    @BindView(R.id.statusTv)
    TextView statusTv;

    @BindView(R.id.kandoeBoard)
    KandoeBoard kandoeBoard;

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    StompClient stompClient;

    void setSession(Session session) {
        getOrCreateArgs().putSerializable(KEY_SESSION, session);
    }


    Session getSession() {
        return (Session) getOrCreateArgs().getSerializable(KEY_SESSION);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_kandoeboard, menu);
    }

    void toonRemarks(SessionCard card) {
        RemarkOverzichtDailogFragment f = RemarkOverzichtDailogFragment.newInstance(card);
        showDialog(f);
    }

    void kiesKaartjes() {
        CardChooserDialogFragment f = CardChooserDialogFragment.newInstance(getSession());
        f.setTargetFragment(this, 0);
        showDialog(f);
    }

    void toonDeelnemers() {
        UserOverzichtDailogFragment f = UserOverzichtDailogFragment.newInstance(getSession().getUsers(), getSession().getOrganiserId(), getSession().getCurrentUserId());
        showDialog(f);
    }

    String getRoundIndicatorText(String id) {
        return StringUtils.format(getString(R.string.round_indicator), id);
    }


    String getTypeText() {
        return StringUtils.format("Type: %s", getSession().getSessionTypeName(getActivity()));
    }

    String getStatusText() {
        return StringUtils.format("Status: %s", getSession().getSessionStatusName(getActivity()));
    }


    void refreshIndicators() {
        Session session = getSession();
        Integer roundNumber = session.getCurrentRound();
        String roundText = session.isPlaying() ? String.valueOf(roundNumber) : getString(R.string.not_available);
        roundTv.setText(getRoundIndicatorText(roundText));
        typeTv.setText(getTypeText());
        statusTv.setText(getStatusText());
    }

    static final int DIALOG_KIES_KAARTJES = 1;
    static final int DIALOG_MOVE = 2;

    void promptDoMove() {
        MessageDialogFragment f = MessageDialogFragment.newInstance(this, DIALOG_MOVE);
            f.setMessage(getString(R.string.prompt_do_move));
            f.setPositiveButton(android.R.string.ok);
            f.setNegativeButton(android.R.string.cancel);
            showDialog(f);
    }

    void promptKiesKaartjes() {
        if(getFragmentManager().findFragmentByTag(CardChooserDialogFragment.class.getName()) == null) {
            MessageDialogFragment f = MessageDialogFragment.newInstance(this, DIALOG_KIES_KAARTJES);
            f.setMessage(getString(R.string.prompt_pick_cards));
            f.setPositiveButton(android.R.string.ok);
            f.setNegativeButton(android.R.string.cancel);
            showDialog(f);
        }
    }

    void showDetails() {
        Session s = getSession();
        SessionDetailFragment f = SessionDetailFragment.newInstance(s);
        showDialog(f);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();
        if (itemId == R.id.menu_show_participants) {
            toonDeelnemers();
            return true;
        } else if (itemId == R.id.menu_show_info) {
            showDetails();
            return true;
        } else if(itemId == R.id.menu_chat) {
            chat();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    void chat() {
        ChatFragment f = ChatFragment.newInstance(getSession());
        new FragmentHelper(this).open(f);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.kandoe_board_fragment_layout, container, false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(stompClient.isConnected()) {
            stompClient.disconnect();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initViews();

        //subscribe to session updates
        stompClient.connect();
        stompClient.topic(Topic.session(getSession().getId())).subscribe(stompMessage -> {
            String payload = stompMessage.getPayload();
            MyLog.d(TAG, payload);
            handler.post(()->handlePayload(payload));
        });
    }

    @Override
    void loadData() {
        loadSession();
    }

    @Override
    protected void initBaseActivity(BaseActivity baseActivity, ActionBar actionBar) {
        super.initBaseActivity(baseActivity, actionBar);
        actionBar.setTitle("Kandoe");
        if (getSession() != null && getSession().getName() != null)
            actionBar.setSubtitle(getSession().getName());
        else
            actionBar.setSubtitle("Loading...");
    }


    int onKandoeCardClick(KandoeCard card, KandoeCircle circle) {
        System.out.println("setOnKandoeCardClickListener: " + card.getCard().getText());
        final int pos = getAdapter().getItemPosition(card);
        recyclerView.scrollToPosition(pos);

        recyclerView.post(() -> {
            final RecyclerView.ViewHolder viewHolder = recyclerView.findViewHolderForLayoutPosition(pos);
            if (viewHolder != null)
                startColorAnimation(viewHolder.itemView, pos);
        });
        return pos;
    }

    void initViews() {
        kandoeBoard.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                kandoeBoard.removeOnLayoutChangeListener(this);
                initRecyclerView();

                kandoeBoard.setOnDataChangedListener(new KandoeBoard.OnDataChangedListener() {
                    @Override
                    public void onDataSetChanged() {
                        getAdapter().setData(findData());
                        getAdapter().notifyDataSetChanged();
                    }
                });
                kandoeBoard.setOnKandoeCardClickListener((card, circle) -> {
                    onKandoeCardClick(card, circle);
                });

            }
        });
    }


    List<KandoeCard> findData() {
        return kandoeBoard.getAllCards();
    }

    MyAdapter getAdapter() {
        return (MyAdapter) recyclerView.getAdapter();
    }


    @Override
    public void onCardsChoosen(List<SessionCard> cards) {
        Long ids[] = StreamSupport.stream(cards).map(c->c.getId()).toArray(Long[]::new);
         mSessionCall = sessionService.pickCards(getSession().getId(), ids);
        mSessionCall.enqueue(new MyCallback<Session>() {
            @Override
            public void onFailure(Call<Session> call, RetrofitException t) {
                if(!call.isCanceled())
                handleOnFailure(t);
            }

            @Override
            public void onResponse(Call<Session> call, Response<Session> response) {
            }
        });
    }

    void onSessionUpdate(Session session) {
        syncSessionCards();
        refreshIndicators();
       if(isLoggedInUserTurn() || MyApplication.isMockMode()) {
           if (session.isPickingCards()) {
               promptKiesKaartjes();
           } else if (session.isPlaying()) {
               promptDoMove();
           }
       }
    }


    public void syncSessionCards() {
        kandoeBoard.reset(getSession().getParticipantSessionCards());
    }

    @Override
    public void onDialogButtonClick(MessageDialogFragment f, int dialogId, int which) {
        MyLog.d(getClass(), "onDialogButtonClick: "+dialogId +" "+which);
        switch (dialogId) {
            case DIALOG_KIES_KAARTJES:
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    kiesKaartjes();
                }
                break;
        }

    }


    class MyAdapter extends RecyclerView.Adapter<CustomViewHolder> {
        List<KandoeCard> cards;
        private OnViewHolderClickListener onViewHolderClickListener;
        private OnViewHolderLongClickListener onViewHolderLongClickListener;
        private Context context = getActivity();
        private LayoutInflater inflater;

        public MyAdapter() {
            inflater = LayoutInflater.from(context);
        }

        private KandoeCard getItemAtPosition(int position) {
            return cards.get(position);
        }

        public int getItemPosition(KandoeCard card) {
            return cards.indexOf(card);
        }

        void setData(List<KandoeCard> cards) {
            MyLog.d(getClass(), "setData: " + cards);
            this.cards = cards;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = inflater.inflate(R.layout.kandoe_textview, parent, false);
            return new CustomViewHolder(v);
        }

        @Override
        public void onBindViewHolder(CustomViewHolder holder, int position) {
            KandoeCard kandoeCard = getItemAtPosition(position);
            holder.themeCardView.setCard(kandoeCard.getCard().getCard());
            holder.colorIndicator.setBackgroundColor(kandoeCard.getCircleColor());
            holder.level.setText("" + kandoeCard.getLevel());
            if (onViewHolderClickListener != null)
                holder.setOnViewHolderClickListener(onViewHolderClickListener);
            if (onViewHolderLongClickListener != null) {
                holder.setOnViewHolderLongClickListener(onViewHolderLongClickListener);
            }
        }

        @Override
        public int getItemCount() {
            return cards.size();
        }

        public void setOnViewHolderClickListener(OnViewHolderClickListener onViewHolderClickListener) {
            this.onViewHolderClickListener = onViewHolderClickListener;
        }

        public void setOnViewHolderLongClickListener(OnViewHolderLongClickListener onViewHolderLongClickListener) {
            this.onViewHolderLongClickListener = onViewHolderLongClickListener;
        }
    }

    class CustomViewHolder extends MyViewHolder {
        @BindView(R.id.text)
        ThemeCardView themeCardView;
        @BindView(R.id.colorIndicator)
        View colorIndicator;
        @BindView(R.id.level)
        TextView level;

        public CustomViewHolder(View itemView) {
            super(itemView);
        }
    }

    void initRecyclerView() {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.HORIZONTAL));
        MyAdapter adapter = new MyAdapter();
        adapter.setData(findData());
        adapter.setOnViewHolderClickListener(new OnViewHolderClickListener() {
            @Override
            public void onViewHolderClick(RecyclerView.ViewHolder viewHolder, int position, long id) {
                MyLog.d(KandoeBoardFragment.class.getSimpleName(), "onViewHolderClick");
                KandoeCard card = getAdapter().getItemAtPosition(position);
                //showToolTip();
                openMenu(viewHolder.itemView, position, id);
            }
        });
        recyclerView.setAdapter(adapter);
    }

    static final int COLOR_START = Color.WHITE;
    static final int COLOR_END = Color.parseColor("#CCCCCC");

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void startColorAnimation(View v, int pos) {
        System.out.println("StartColorAnim");
        if (animationHelper != null)
            animationHelper.cancel();

        animationHelper = new AnimationHelper(v, COLOR_START, COLOR_END);

        animationHelper.start();
    }

    private AnimationHelper animationHelper;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private class AnimationHelper implements Animator.AnimatorListener {

        private ValueAnimator colorAnim;
        int colorStart;
        Drawable background = null;
        View v;

        public AnimationHelper(View v, int startColor, int endColor) {
            colorStart = startColor;
            int colorEnd = endColor;
            background = v.getBackground();
            this.v = v;
            colorAnim = ObjectAnimator.ofInt(v,
                    "backgroundColor", colorStart, colorEnd);
            colorAnim.setDuration(1000);
            colorAnim.setEvaluator(new ArgbEvaluator());
            colorAnim.setRepeatCount(1);
            colorAnim.setRepeatMode(ValueAnimator.REVERSE);
            colorAnim.addListener(this);

        }

        public void start() {
            colorAnim.start();
        }

        public void cancel() {
            if (colorAnim != null && colorAnim.isRunning())
                colorAnim.cancel();
        }

        @Override
        public void onAnimationStart(Animator animation) {

        }

        @Override
        public void onAnimationEnd(Animator animation) {
            animationHelper = null;
            v.setBackground(background);
        }

        @Override
        public void onAnimationCancel(Animator animation) {
            onAnimationEnd(animation);

        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    private void openMenu(View v, int pos, long id) {
        PopupMenu p = new PopupMenu(getActivity(), v);
        p.setOnMenuItemClickListener(new CustomOnMenuItemClickListener(pos, id) {

            @Override
            public boolean onMenuItemClick(MenuItem item, int pos, long id) {
                return handleOnMenuItemClick(item, pos, id);
            }
        });
        p.getMenuInflater().inflate(R.menu.popup_kandoe, p.getMenu());
        initMenu(p.getMenu(), pos);
        p.show();
    }

    boolean isLoggedInUserTurn() {
     return isLoggedIn() && getLoggedInUser().equals(getSession().getCurrentUser());
    }

    boolean isPlaying() {
        return getSession().isPlaying();
    }

    void initMenu(Menu menu, int pos) {
        KandoeCard card = getAdapter().getItemAtPosition(pos);
        if (!isPlaying() || !card.canMoveUp() || !isLoggedInUserTurn()) {
            menu.removeItem(R.id.menu_doe_zet);
        }
    }

    void doeZet(int pos) {
        KandoeCard kandoeCard = getAdapter().getItemAtPosition(pos);
        SessionCard sessionCard = kandoeCard.getCard();
        mSessionCall = sessionService.makeMove(getSession().getId(), sessionCard.getId());
        mSessionCall.enqueue(new MyCallback<Session>() {
            @Override
            public void onFailure(Call<Session> call, RetrofitException t) {
                if(!call.isCanceled())
                handleOnFailure(t);
            }

            @Override
            public void onResponse(Call<Session> call, Response<Session> response) {
            }
        });
    }

    private boolean handleOnMenuItemClick(MenuItem item, int pos, long id) {
        int itemId = item.getItemId();
        switch (itemId) {
            case R.id.menu_doe_zet:
                doeZet(pos);
                break;

            case R.id.menu_show_remarks:
                toonRemarks(getAdapter().getItemAtPosition(pos).getCard());
                break;
        }
        return true;
    }
}

package be.kdg.kandoe.frontend.fragments;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.content.Theme;
import be.kdg.kandoe.backend.retrofit.service.util.MyCallback;
import be.kdg.kandoe.backend.retrofit.service.util.RetrofitException;
import be.kdg.kandoe.backend.retrofit.service.factory.ServiceGenerator;
import be.kdg.kandoe.frontend.activities.BaseActivity;
import retrofit2.Call;
import retrofit2.Response;

public class MijnThemasFragment extends BaseThemeOverzichtFragment {

    Call<List<Theme>> mCall;


    public static MijnThemasFragment newInstance() {
        MijnThemasFragment fragment = new MijnThemasFragment();
        fragment.getOrCreateArgs();
        return fragment;
    }


    @Override
    void loadData() {
       load();
    }

    void load() {
        if(!isLoggedIn())
            return;
        mCall = ServiceGenerator.getInstance().createThemeSevice().getThemesByOrganiser(getLoggedInUser().getId());
        manage(mCall);
        mCall.enqueue(new MyCallback<List<Theme>>() {
            @Override
            public void onFailure(Call<List<Theme>> call, RetrofitException t) {
                if(!call.isCanceled())
                handleOnFailure(t);
            }

            @Override
            public void onResponse(Call<List<Theme>> call, Response<List<Theme>> response) {
                if(!call.isCanceled()) {
                    setThemes(response.body());
                    refreshAdapter();
                }
            }
        });
    }

    @Override
    protected void initBaseActivity(BaseActivity baseActivity, ActionBar ab) {
        ab.setTitle(R.string.menu_my_themes);
        ab.setSubtitle("");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(false);
        return inflater.inflate(R.layout.theme_overzicht, container, false);
    }


}

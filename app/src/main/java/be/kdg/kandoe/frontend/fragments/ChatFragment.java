package be.kdg.kandoe.frontend.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.Session.Session;
import be.kdg.kandoe.backend.dom.content.Message;
import be.kdg.kandoe.backend.retrofit.service.util.MyCallback;
import be.kdg.kandoe.backend.retrofit.service.util.RetrofitException;
import be.kdg.kandoe.backend.retrofit.service.util.RetrofitHelper;
import be.kdg.kandoe.backend.retrofit.service.factory.ServiceGenerator;
import be.kdg.kandoe.backend.retrofit.service.api.session.SessionService;
import be.kdg.kandoe.backend.websocket.WebSocketUtil;
import be.kdg.kandoe.backend.websocket.WebSocketUtil.EndPoint;
import be.kdg.kandoe.backend.websocket.WebSocketUtil.Topic;
import be.kdg.kandoe.frontend.activities.BaseActivity;
import be.kdg.kandoe.frontend.adapters.MessageAdapter;
import be.kdg.kandoe.utils.MyLog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;
import ua.naiksoftware.stomp.client.StompClient;

/**
 * Created by Ruben on 14/03/2017.
 */

public class ChatFragment extends BaseFragment {
    static final String TAG = ChatFragment.class.getSimpleName();

    @BindView(R.id.listView)
    ListView listview;
    @BindView(R.id.emptyTV)
    TextView emptyTv;

    @BindView(R.id.messageEdit)
    EditText messageEdit;

    @BindView(R.id.sendBtn)
    View sendBtn;

    @BindView(R.id.inputSection)
    View inputSection;

    SessionService sessionService = ServiceGenerator.getInstance().createSessionService();

    StompClient stompClient;

    static final String KEY_SESSION = "session";


    private List<Message> newMessages = new ArrayList<>();

    private List<Message> messages = new ArrayList<>();

    public static ChatFragment newInstance(Session session) {
        ChatFragment fragment = new ChatFragment();
        fragment.getOrCreateArgs().putSerializable(KEY_SESSION, session);
        return fragment;
    }

    Session getSession() {
        return (Session) getArguments().getSerializable(KEY_SESSION);
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        BaseActivity activity = (BaseActivity) getActivity();
        ActionBar actionBar = activity.getSupportActionBar();
        actionBar.setTitle(R.string.chat);
        actionBar.setSubtitle(getSession().getName());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(false);
        stompClient = WebSocketUtil.createStompClient(EndPoint.CHAT);
        stompClient.lifecycle().subscribe(lifecycleEvent -> {
            switch (lifecycleEvent.getType()) {

                case OPENED:
                    MyLog.d(getClass(), "Stomp connection opened");
                    break;

                case ERROR:
                    MyLog.e(getClass(), "Error", lifecycleEvent.getException());
                    break;

                case CLOSED:
                    MyLog.d(getClass(), "Stomp connection closed");
                    break;
            }
        });
    }

    void refreshAdapter() {
        setAdapter(getMessages());
    }



    void setAdapter(List<Message> items) {
        MessageAdapter adapter = new MessageAdapter(getActivity(), items);
        listview.setAdapter(adapter);
    }

    void setMessages(List<Message> messages) {
        newMessages.clear();
        this.messages = messages;
    }

    List<Message> getMessages() {
        List<Message> list = new ArrayList<>();
        list.addAll(messages);
        list.addAll(newMessages);
        return list;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(false);
        return inflater.inflate(R.layout.chat_layout, container, false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (stompClient.isConnected()) {
            stompClient.disconnect();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initViews(savedInstanceState);
        stompClient.connect();
        stompClient.topic(Topic.chat(getSession().getId())).subscribe(stompMessage -> {
            String payload = stompMessage.getPayload();
            MyLog.d(TAG, payload);
            handler.post(() -> handlePayload(payload));

        });
    }

    @Override
    void loadData() {
        load();
    }

    @OnClick(R.id.sendBtn)
    void sendChat(View v) {
        MyLog.d(getClass(), "sendChat");
        String str = messageEdit.getText().toString();
//        if(str.trim().length()==0)
//            return;
        Message message = new Message();
        message.setText(str);
        message.setUser(getLoggedInUser());
        Call<Message> messageCall = sessionService.uploadMessage(getSession().getId(), message);
        messageCall.enqueue(new MyCallback<Message>() {
            @Override
            public void onFailure(Call<Message> call, RetrofitException t) {
                handleOnFailure(t);
            }

            @Override
            public void onResponse(Call<Message> call, Response<Message> response) {
                //do nothing
            }
        });
        messageEdit.setText("");
    }


    private void scrollMyListViewToBottom() {
        listview.post(new Runnable() {
            @Override
            public void run() {
                // Select the last row so it will scroll into view...
                listview.setSelection(listview.getCount() - 1);
            }
        });
    }

    void handlePayload(String data) {
        Message message = RetrofitHelper.readValue(data, Message.class);
        newMessages.add(message);
        refreshAdapter();
        scrollMyListViewToBottom();
    }

    Handler handler = new Handler();

    void setSendButtonEnabled(boolean enabled) {
        sendBtn.setVisibility(enabled ? View.VISIBLE : View.INVISIBLE);
        sendBtn.setClickable(enabled);
        sendBtn.setEnabled(enabled);
    }

    void initViews(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            setSendButtonEnabled(false);
        }
        boolean chatAllowed = getSession().isParticipant(getLoggedInUser());
        inputSection.setVisibility(chatAllowed ? View.VISIBLE : View.GONE);
        messageEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                boolean enabled = s.toString().trim().length() > 0;
                setSendButtonEnabled(enabled);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        listview.setEmptyView(emptyTv);

    }


    void onChatUpdated() {
        setAdapter(getMessages());
        scrollMyListViewToBottom();
    }

    void load() {
       final Call<List<Message>> call = sessionService.getChat(getSession().getId());
        manage(call);
        emptyTv.setText(R.string.loading);
        call.enqueue(new MyCallback<List<Message>>() {
            @Override
            public void onFailure(Call<List<Message>> call, RetrofitException t) {
                handleOnFailure(t);
                if(!call.isCanceled()) {
                    emptyTv.setText(R.string.chat_empty);
                }
            }

            @Override
            public void onResponse(Call<List<Message>> call, Response<List<Message>> response) {
                if(!call.isCanceled()) {
                    emptyTv.setText(R.string.chat_empty);
                    setMessages(response.body());
                    onChatUpdated();
                }
            }
        });
    }
}

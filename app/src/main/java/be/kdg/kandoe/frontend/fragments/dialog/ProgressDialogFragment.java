package be.kdg.kandoe.frontend.fragments.dialog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;

/**
 * Created by Ruben on 08/03/2017.
 */

public class ProgressDialogFragment extends BaseDialogFragment {



    static final String KEY_INDETERMINATE = "indeterminate";
    static final String KEY_MESSAGE = "message";
    static final String KEY_TITLE = "title";

    public static ProgressDialogFragment newInstance(String message) {
        ProgressDialogFragment fragment = new ProgressDialogFragment();
       fragment.setMessage(message);
        fragment.setIndeterminate(true);
        return fragment;
    }

    public void setTitle(String title) {
       getOrCreateArgs().putString(KEY_TITLE, title);
    }
    
    public String getTitle() {
        return getOrCreateArgs().getString(KEY_TITLE);
    }
    public void setMessage(String message) {
        getOrCreateArgs().putString(KEY_MESSAGE, message);
    }

    public String getMessage() {
        return getOrCreateArgs().getString(KEY_MESSAGE);
    }

    public void setIndeterminate(Boolean indeterminate) {
        getOrCreateArgs().putBoolean(KEY_INDETERMINATE, indeterminate);
    }

    public Boolean getIndeterminate() {
        return getOrCreateArgs().getBoolean(KEY_INDETERMINATE);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(false);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(getIndeterminate());
        progressDialog.setTitle(getTitle());
        progressDialog.setMessage(getMessage());
        return progressDialog;
    }
}

package be.kdg.kandoe.frontend.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wefika.flowlayout.FlowLayout;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.content.Theme;
import be.kdg.kandoe.backend.retrofit.service.util.MyCallback;
import be.kdg.kandoe.backend.retrofit.service.util.RetrofitException;
import be.kdg.kandoe.backend.retrofit.service.factory.ServiceGenerator;
import be.kdg.kandoe.frontend.fragments.dialog.BaseDialogFragment;
import be.kdg.kandoe.utils.FlowLayoutHelper;
import be.kdg.kandoe.utils.MyLog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Ruben on 27/02/2017.
 */

public class ThemeDetailFragment extends BaseDialogFragment {

    @BindView(R.id.flOrganisators)
    FlowLayout organisators;
    @BindView(R.id.flTags)
    FlowLayout tags;

    @BindView(R.id.tvNaam)
    TextView naam;

    @BindView(R.id.tvDescription)
    TextView description;

    @BindView(R.id.tvAccessability)
    TextView accessability;

    Call<Theme> mCall;

    static final String KEY_ID_TO_FETCH = "item_id";

    public void setIdToFetch(long id) {
        getOrCreateArgs().putLong(KEY_ID_TO_FETCH, id);
    }

    public long getIdToFetch() {
        return getOrCreateArgs().getLong(KEY_ID_TO_FETCH);
    }

    static final String KEY_THEME = "theme";

    public static ThemeDetailFragment newInstance(long id) {
        ThemeDetailFragment fragment = new ThemeDetailFragment();
        fragment.setIdToFetch(id);
        return fragment;
    }

    @OnClick(R.id.showCardsBtn)
    public void onCardsButtonClicked(View v) {
        CardOverzichtFragment cardOverzichtFragment = CardOverzichtFragment.newInstance(getThema());
        showDialog(cardOverzichtFragment);
    }

    public static ThemeDetailFragment newInstance(Theme theme) {
        ThemeDetailFragment fragment = new ThemeDetailFragment();
        fragment.setThema(theme);
        return fragment;
    }

    public void setThema(Theme theme) {
        getOrCreateArgs().putSerializable(KEY_THEME, theme);
    }

    public Theme getThema() {
        return (Theme) getOrCreateArgs().getSerializable(KEY_THEME);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    protected void loadData() {
        long id = getIdToFetch();
        if(getIdToFetch()==0){
            MyLog.d(getClass(), "theme set manually, not need to fetch");
            onLoaded();
        } else {
            MyLog.d(getClass(), "idToFetch specified, fetching theme "+id);
            load(id);
        }
    }

    void load(long id) {
        mCall = ServiceGenerator.getInstance().createThemeSevice().getTheme(id);
        manage(mCall);
        mCall.enqueue(new MyCallback<Theme>() {
            @Override
            public void onFailure(Call<Theme> call, RetrofitException t) {
                if(!call.isCanceled())
                handleOnFailure(t);
            }

            @Override
            public void onResponse(Call<Theme> call, Response<Theme> response) {
                if(!call.isCanceled()) {
                    setThema(response.body());
                    onLoaded();
                }
            }
        });
    }

    void onLoaded() {
        MyLog.d(getClass(), "onLoaded");
        initViews(getThema());
    }

    void initViews(Theme theme) {
       new FlowLayoutHelper(tags).setTags(theme.getTags());
        new FlowLayoutHelper(organisators).setUsers(theme.getOrganisers(), null, 0);
        description.setText(theme.getDescription());
        naam.setText(theme.getName());
        accessability.setText((!theme.isPublicAllowed()? "Private" : "Public"));
        int drawable = !theme.isPublicAllowed()? R.drawable.ic_private : R.drawable.ic_public;
        accessability.setCompoundDrawablesWithIntrinsicBounds(drawable, 0, 0, 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(false);
        return inflater.inflate(R.layout.theme_detail, container, false);
    }
}

package be.kdg.kandoe.frontend.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.frontend.views.MyEditText;
import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseRegistreerFragment extends BaseFragment {

    @BindView(R.id.voornaamEdit)
    MyEditText voornaamEdit;
    @BindView(R.id.achternaamEdit)
    MyEditText achternaamEdit;
    @BindView(R.id.passwordEdit)
    MyEditText wachtwoordEdit;
    @BindView(R.id.passwordEdit2)
    MyEditText wachtwoordEdit2;
    AwesomeValidation mValidator;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        ButterKnife.bind(this, view);
        mValidator = new AwesomeValidation(ValidationStyle.TEXT_INPUT_LAYOUT);

        mValidator.addValidation(getTextInputLayout(wachtwoordEdit2),
                getTextInputLayout(wachtwoordEdit), getString(R.string.validation_password_confirm));
    }

    User generateUser() {
        User u = new User();
        u.setLastName(achternaamEdit.getText().toString());
        u.setFirstName(voornaamEdit.getText().toString());
        if(wachtwoordEdit.length()>0)
        u.setPassword(wachtwoordEdit.getText().toString());
        return u;
    }

}

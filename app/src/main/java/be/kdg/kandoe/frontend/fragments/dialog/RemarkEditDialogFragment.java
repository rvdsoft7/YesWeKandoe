package be.kdg.kandoe.frontend.fragments.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.View;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.Session.SessionCard;
import be.kdg.kandoe.backend.dom.content.Remark;
import be.kdg.kandoe.backend.login.LoginHelperImpl;
import be.kdg.kandoe.frontend.views.MyEditText;
import be.kdg.kandoe.utils.Utils;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ruben on 06/03/2017.
 */

public class RemarkEditDialogFragment extends BaseDialogFragment {

    static final String KEY_CARD = "card";

    public static RemarkEditDialogFragment newInstance(SessionCard card, Fragment f) {
        RemarkEditDialogFragment fragment = new RemarkEditDialogFragment();
        fragment.getOrCreateArgs().putSerializable(KEY_CARD, card);
        fragment.setTargetFragment(f, 0);
        return fragment;
    }

    @BindView(R.id.remarkEdit)
    MyEditText remarkEdit;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View v = getActivity().getLayoutInflater().inflate(R.layout.remark_edit_layout, null, false);
        ButterKnife.bind(this, v);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setView(v);
        builder.setTitle(R.string.card_remark).setPositiveButton(android.R.string.ok, (dialog, which) -> {
                    Utils.hideKeyboard(remarkEdit);
            Remark remark = new Remark();
            remark.setText(remarkEdit.getText().toString());
            remark.setUser(LoginHelperImpl.getInstance().getUser());
            remark.setCard((SessionCard) getArguments().getSerializable(KEY_CARD));
            getListener().onRemarkMade(remark);
        }) .setNegativeButton(android.R.string.no, null);
        return builder.create();
    }

    RemarkEditDialogFragmentListener getListener() {
        return getListener(RemarkEditDialogFragmentListener.class);
    }


   public interface RemarkEditDialogFragmentListener {
        void onRemarkMade(Remark remark);
    }
}

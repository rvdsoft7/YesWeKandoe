package be.kdg.kandoe.frontend.fragments.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import java.util.ArrayList;
import java.util.List;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.Session.SessionCard;
import be.kdg.kandoe.backend.dom.Session.Session;
import be.kdg.kandoe.frontend.views.MyViewHolder;
import be.kdg.kandoe.frontend.views.ThemeCardView;
import be.kdg.kandoe.utils.SparseBooleanArrayParcelable;
import be.kdg.kandoe.utils.StringUtils;
import butterknife.BindView;
import butterknife.ButterKnife;
import java8.util.stream.StreamSupport;

/**
 * Created by Ruben on 06/03/2017.
 */

public class CardChooserDialogFragment extends BaseDialogFragment {


    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    static final String KEY_CARDS = "cards";
    static final String KEY_SELECTIONS = "selections";

    static final String KEY_SESSION = "session";

    public static CardChooserDialogFragment newInstance(Session session) {
        CardChooserDialogFragment fragment = new CardChooserDialogFragment();
        fragment.getOrCreateArgs().putSerializable(KEY_SESSION, session);
        fragment.getOrCreateArgs().putSerializable(KEY_CARDS, new ArrayList<>(session.getUnselectedSessionCards()));
        return fragment;
    }

    boolean validateListSelections() {
        int checkedCount = getAdapter().getCheckedCount();
        System.out.println("CheckedCount: "+checkedCount);
        return checkedCount>=getSession().getMinCards();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(false);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.card_chooser_layout, null, false);
        ButterKnife.bind(this, view);

        initViews(savedInstanceState);
        AlertDialog d =  new AlertDialog.Builder(getActivity()).setTitle(R.string.kies_kaarten)
                .setNegativeButton(android.R.string.cancel, null).setPositiveButton(android.R.string.ok,null
                        ).setView(view).create();
        d.setCancelable(isCancelable());
        d.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                d.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(validateListSelections()) {
                            dismiss();
                            StreamSupport.stream(getAdapter().getSelectedCards()).forEach(card -> System.out.println("Selected "+card));
                           if(getListener()!=null)
                            getListener().onCardsChoosen(getAdapter().getSelectedCards());
                        } else {
                            showMessage(StringUtils.format(getString(R.string.min_cards_message), getSession().getMinCards()));
                        }
                    }
                });
            }
        });
        return d;
    }

    private Session getSession() {
        return (Session) getOrCreateArgs().getSerializable(KEY_SESSION);
    }

    private List<SessionCard> getCards() {
        return (List<SessionCard>) getOrCreateArgs().getSerializable(KEY_CARDS);
    }


    void initViews(Bundle savedInstanceState) {
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        GridLayoutManager glm = new GridLayoutManager(getActivity(), 3);
        recyclerView.setLayoutManager(glm);
        SparseBooleanArray array = null;
        if (savedInstanceState != null) {
            array = savedInstanceState.getParcelable(KEY_SELECTIONS);
        }
        recyclerView.setAdapter(new MyAdapter(getActivity(), array));
    }

    class CustomViewHolder extends MyViewHolder {

        @BindView(R.id.card)
        ThemeCardView card;

        @BindView(R.id.check)
        CheckBox check;

        public CustomViewHolder(View itemView) {
            super(itemView);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(KEY_SELECTIONS, new SparseBooleanArrayParcelable(getAdapter().checkedList));
    }


    MyAdapter getAdapter() {
        return (MyAdapter) recyclerView.getAdapter();
    }

    class MyAdapter extends RecyclerView.Adapter<CustomViewHolder> {

        SparseBooleanArray checkedList = new SparseBooleanArray();

        public MyAdapter(Context context, SparseBooleanArray checkedList) {
            inflater = LayoutInflater.from(context);
            this.checkedList = checkedList == null ? new SparseBooleanArray() : checkedList;
        }

        public List<SessionCard> getSelectedCards() {
            List<SessionCard> cards = getCards();
            List<SessionCard> newCards = new ArrayList<>();
            for (int i = 0; i < checkedList.size(); i++) {
                if (checkedList.valueAt(i)) {
                    newCards.add(cards.get(checkedList.keyAt(i)));
                }
            }

            return newCards;
        }

        LayoutInflater inflater;


        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = inflater.inflate(R.layout.card_chooser_row, parent, false);
            return new CustomViewHolder(v);
        }

        int getCheckedCount() {
            int size = 0;
            for (int i = 0; i < checkedList.size(); i++) {
                if (checkedList.valueAt(i)) {
                    size++;
                }
            }
            return size;
        }

        @Override
        public void onBindViewHolder(CustomViewHolder holder, int position) {
            SessionCard card = getCards().get(position);
            holder.card.setCard(card.getCard());
            holder.check.setChecked(checkedList.get(position));
            holder.setOnViewHolderClickListener((viewHolder, position1, id) -> {
                boolean checked = holder.check.isChecked();
                if (checked ||getCheckedCount() < getSession().getMaxCards()) {
                    boolean check = !checked;
                    checkedList.put(position1, check);
                    holder.check.setChecked(check);
                } else {
                    String msg =  StringUtils.format(getString(R.string.max_cards_message), getSession().getMaxCards());
                    showMessage(msg);
                }
            });
        }

        @Override
        public int getItemCount() {
            return getCards().size();
        }
    }


    CardChooserFragmentListener getListener() {
        return getListener(CardChooserFragmentListener.class);
    }

    public interface CardChooserFragmentListener {
        void onCardsChoosen(List<SessionCard> cards);
    }
}

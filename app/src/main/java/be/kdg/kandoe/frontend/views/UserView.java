package be.kdg.kandoe.frontend.views;


import com.nostra13.universalimageloader.core.assist.ImageSize;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.utils.ImageLoaderHelper;
import butterknife.BindView;
import butterknife.ButterKnife;

public class UserView extends FrameLayout {

    @BindView(R.id.naam)
    TextView naam;
    @BindView(R.id.avatar)
    ImageView avatar;
    @BindView(R.id.fullname)
    TextView username;
    @BindView(R.id.indicator)
    ImageView indicator;
    @BindView(R.id.role)
    TextView role;


    boolean isInteractable;

    User user;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public UserView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs, defStyleAttr);
    }

    public UserView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs, defStyleAttr);
    }

    public UserView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public UserView(Context context) {
        super(context);
        init(null, 0);
    }

    void init(AttributeSet attrs, int defStyleAttr) {
        initViews();
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.MyEditText, defStyleAttr, 0);
            try {
                isInteractable = a.getBoolean(R.styleable.UserView_interactive, false);
            } finally {
                a.recycle();
            }
        }

        handleInteractable();

    }


    void initViews() {
        View v = LayoutInflater.from(getContext()).inflate(R.layout.user_layout, this, true);
        ButterKnife.bind(this, v);
        handleUser();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        handleUser();
    }

    public void setIsOnTurn(boolean onTurn) {
        setIndicator(onTurn?R.drawable.ic_spel:0);
    }
    public void setOrganistor(boolean organistor) {
        setRole(organistor?getContext().getString(R.string.organistor_role):null);
    }

    public void setRole(int stringResId) {
        setRole(getContext().getString(stringResId));
    }

    public void setRole(String role) {
        this.role.setVisibility(role!=null ? View.VISIBLE : View.GONE);
        this.role.setText(role);
    }

    public void setIndicator(int drawableResId) {
        this.indicator.setVisibility(drawableResId>0?View.VISIBLE : View.GONE);
        this.indicator.setImageResource(drawableResId);
    }

    String getUrl() {
        return user != null ? user.getAvatarUrl() : "";
    }

    void handleUser() {
        naam.setText(user == null ? "<Name>" : user.getFullName());
        if(naam.getText().toString().trim().length()==0) {
            naam.setText("Name not available");
            naam.setTypeface(null, Typeface.ITALIC);
        } else {
            naam.setTypeface(null, Typeface.NORMAL);
        }
        if (!isInEditMode())
            ImageLoaderHelper.displayImage(getUrl(), avatar, new ImageSize(100, 100));
        else
            avatar.setImageResource(R.drawable.ic_launcher);

        username.setText(user==null ? "<geen email beschikbaar>" : user.getUsername());
    }

    public void setInteractable(boolean interactable) {
        isInteractable = interactable;
        handleInteractable();

    }

    void handleInteractable() {
        setClickable(isInteractable);
        if (isInteractable)
            setBackgroundResource(isInteractable ? R.drawable.list_selector : 0);
    }



}

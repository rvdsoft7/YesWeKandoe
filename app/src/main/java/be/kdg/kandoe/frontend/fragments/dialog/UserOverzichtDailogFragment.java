package be.kdg.kandoe.frontend.fragments.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.frontend.adapters.UserAdapter;
import be.kdg.kandoe.utils.MyLog;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ruben on 07/03/2017.
 */

public class UserOverzichtDailogFragment extends BaseDialogFragment {

    @BindView(android.R.id.list)
    ListView listview;
    @BindView(android.R.id.empty)
    TextView emptyTv;

    static final String KEY_USERS = "users";

    static final String KEY_ORGANISATOR = "organistor";
    static final String KEY_ON_TURN = "on_turn";

    public static UserOverzichtDailogFragment newInstance(List<User> users, long organistor, long onTurn) {
        UserOverzichtDailogFragment fragment = new UserOverzichtDailogFragment();
        fragment.getOrCreateArgs().putSerializable(KEY_USERS, new ArrayList<>(users));
        fragment.getOrCreateArgs().putLong(KEY_ORGANISATOR, organistor);
        fragment.getOrCreateArgs().putLong(KEY_ON_TURN, onTurn);
        return fragment;
    }

    void initViews(Bundle savedInstanceState) {
        listview.setEmptyView(emptyTv);
        refreshAdapter();

    }

    void setAdapter(List<User> items) {
        UserAdapter adapter =  new UserAdapter(getActivity(), items);
        long organistor = getOrCreateArgs().getLong(KEY_ORGANISATOR);
        long onTurn = getOrCreateArgs().getLong(KEY_ON_TURN);
        if(organistor>0)
        adapter.setOrganistors(new ArrayList<>(Arrays.asList(organistor)));
        adapter.setOnTurn(onTurn);
        listview.setAdapter(adapter);
    }

    void refreshAdapter() {
        resetAdapter();
    }

    void resetAdapter() {
        setAdapter(getUsers());
    }

    List<User> getUsers() {
        return (List<User>) getArguments().getSerializable(KEY_USERS);
    }



    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        MyLog.d(getClass(), "onCreateDialog");
        View v = getActivity().getLayoutInflater().inflate(R.layout.user_overzicht, null, false);
        ButterKnife.bind(this, v);
        AlertDialog d = new AlertDialog.Builder(getActivity()).setTitle(R.string.users).setView(v).create();
        initViews(savedInstanceState);
        return d;
    }
}

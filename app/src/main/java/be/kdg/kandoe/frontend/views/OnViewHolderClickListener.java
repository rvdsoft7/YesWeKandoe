package be.kdg.kandoe.frontend.views;

import android.support.v7.widget.RecyclerView;

public interface OnViewHolderClickListener {

	void onViewHolderClick(RecyclerView.ViewHolder viewHolder, int position, long id);
}

package be.kdg.kandoe.frontend.activities;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.login.ILoginHelper;
import be.kdg.kandoe.backend.login.LoginHelperImpl;
import be.kdg.kandoe.backend.login.LoginListener;
import be.kdg.kandoe.backend.login.SimpleLoginListener;
import be.kdg.kandoe.frontend.fragments.LoginFragment;
import be.kdg.kandoe.frontend.fragments.MijnSessiesFragment;
import be.kdg.kandoe.frontend.fragments.MijnThemasFragment;
import be.kdg.kandoe.frontend.fragments.ProfielBewerkFragment;
import be.kdg.kandoe.frontend.fragments.ThemeSearchFragment;
import be.kdg.kandoe.utils.FragmentHelper;
import be.kdg.kandoe.utils.ImageLoaderHelper;
import be.kdg.kandoe.utils.MyLog;
import be.kdg.kandoe.utils.Utils;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    DrawerLayout mDrawerLayout;

    NavigationView navView;

    ActionBarDrawerToggle mDrawerToggle;

    @BindView(R.id.avatar)
    ImageView avatar;
    @BindView(R.id.fullname)
    TextView fullName;
    @BindView(R.id.email)
    TextView email;

    ILoginHelper loginHelper;



    private BroadcastReceiver activityReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getBundleExtra("msg");
            showMessage(bundle.getString("msgBody"));
        }
    };


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_drawer);
        loginHelper = LoginHelperImpl.getInstance();
        loginHelper.setActivity(this);
        loginHelper.register(l);
        initDrawerToggle();
        initHeaderLayout();
        initFragMGr();
        test();
        initTitle(getSupportActionBar());
        //open home screen
        home(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        loginHelper.unregister(l);
    }


    LoginListener l = new SimpleLoginListener() {

        @Override
        public void onLoggedOut() {
            MyLog.d(getClass(), "onLoggedOutMainActivity");
            updateUI2();
            clearFragments();
        }

        @Override
        public void onLoggedIn(User user) {
            updateUI2();
            clearFragments();
        }

        @Override
        public void onRegistered(User user) {
            updateUI2();
            clearFragments();
        }

        public void onProfileEdited(User user) {
            updateUI2();
        }

        ;

    };

    void updateUI2() {
        updateUI();
    }


    void initTitle(ActionBar ab) {
        ab.setTitle(R.string.app_name);
        ab.setSubtitle("");
    }


    @Override
    protected void onResume() {
        super.onResume();
        loginHelper.getGoogleSignInHelper().connect();
        IntentFilter intentFilter = new IntentFilter("ACTION_STRING_ACTIVITY");
        registerReceiver(activityReceiver, intentFilter);
    }

    void test() {
    }


    void changeNavMenu() {
        navView.getMenu().clear();
        if (loginHelper.isLoggedIn()) {
            navView.inflateMenu(R.menu.drawer_menu_logged_in);
        } else
            navView.inflateMenu(R.menu.drawer_menu_logged_out);
    }

    void initFragMGr() {
        getSupportFragmentManager().addOnBackStackChangedListener(() ->
                System.out.println("getCurrentFrag:" + getCurrentFragment()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    protected void onActivityResult(int arg0, int arg1, Intent arg2) {
        System.out.println("onActivityResult activity");
        super.onActivityResult(arg0, arg1, arg2);
        Fragment f = getCurrentFragment();
        if (f != null) {
            f.onActivityResult(arg0, arg1, arg2);
        }
    }


    public void updateUI() {
        System.out.println("updateUI");
        changeNavMenu();
        if (loginHelper.isLoggedIn()) {
            User u = loginHelper.getUser();
            email.setText(u.getUsername());
            fullName.setText(u.getFullName());
            ImageLoaderHelper.displayImage(u.getAvatarUrl(), avatar);
        } else {
            System.out.println("Removing user data in header");
            avatar.setImageBitmap(null);
            email.setText("");
            fullName.setText("");

        }
    }


    private void initHeaderLayout() {
        View headerLayout = navView.getHeaderView(0);
        ButterKnife.bind(this, headerLayout);
        updateUI();
    }

    @SuppressLint("RtlHardcoded")
    protected boolean isDrawerOpen() {
        return mDrawerLayout.isDrawerOpen(Gravity.LEFT);
    }

    @Override
    public void onBackPressed() {
        if (isDrawerOpen()) {
            closeDrawer();
        } else if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            super.onBackPressed();
        } else {
            finish();
        }
    }

    private void initNavView() {
        navView = (NavigationView) findViewById(R.id.nvView);
        navView.setNavigationItemSelectedListener(this);
        onCreateNavMenu(navView.getMenu());

    }

    @SuppressLint("RtlHardcoded")
    protected void closeDrawer() {
        mDrawerLayout.closeDrawer(Gravity.LEFT);
    }

    protected void openLoginFragment() {
        Object o = getCurrentFragment();
        if (o != null && o.getClass() == LoginFragment.class)
            return;
        LoginFragment nextFrag = new LoginFragment();
        new FragmentHelper(this).open(nextFrag);
    }


    @SuppressLint("InflateParams")
    protected void about() {
        View v = getLayoutInflater().inflate(R.layout.about, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.menu_about).setView(v);
        builder.create().show();
    }


    @Override
    protected void onPause() {
        super.onPause();
        loginHelper.getGoogleSignInHelper().disconnect();
        unregisterReceiver(activityReceiver);
    }

    void clearFragments() {
        System.out.println("clearFragments");
        postOnResume(() -> {
            FragmentManager fm = getSupportFragmentManager();
            while (fm.getBackStackEntryCount() > 1) {
               fm.popBackStackImmediate();
            }
        }, "clear_frags");
    }

    void home(Bundle savedInstanceState){
        if(savedInstanceState==null) {
            zoekThemas();
        }


    }

    void mijnProfiel() {
        new FragmentHelper(this).open(ProfielBewerkFragment.newInstance());
    }

    void signIn() {
        openLoginFragment();
    }

    void signOut() {
        loginHelper.logOut();
    }

    void mijnThemas() {
        new FragmentHelper(this).open(MijnThemasFragment.newInstance());
    }

    void zoekThemas() {
        new FragmentHelper(this).open(ThemeSearchFragment.newInstance());
    }

    void mijnSessies() {
        new FragmentHelper(this).open(MijnSessiesFragment.newInstance());
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        closeDrawer();
        int itemId = item.getItemId();

        if (itemId == R.id.menu_sign_in) {
            signIn();
            return true;
        } else if (itemId == R.id.menu_sign_out) {
            signOut();
            return true;
        } else if (itemId == R.id.menu_mijn_sessies) {
            mijnSessies();
            return true;
        } else if (itemId == R.id.menu_mijn_themas) {
            mijnThemas();
            return true;
        } else if (itemId == R.id.menu_zoek_themas) {
            zoekThemas();
            return true;
        } else if (itemId == R.id.menu_mijn_profiel) {
            mijnProfiel();
            return true;
        } else if (itemId == R.id.menu_home) {
            home(null);
            return true;
        } else if (itemId == R.id.menu_about) {
            about();
            return true;
        }else {
            return false;
        }
    }

    protected void onCreateNavMenu(Menu menu) {

    }

    private void initDrawerToggle() {
        initNavView();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close);

        mDrawerLayout.addDrawerListener(mDrawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
        getHandler().post(new Runnable() {

            @Override
            public void run() {
                Utils.hideKeyboard(getSelf());
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}

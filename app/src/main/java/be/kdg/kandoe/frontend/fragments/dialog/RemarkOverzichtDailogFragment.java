package be.kdg.kandoe.frontend.fragments.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionMenu;

import java.util.ArrayList;
import java.util.List;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.Session.SessionCard;
import be.kdg.kandoe.backend.dom.content.Remark;
import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.login.LoginHelperImpl;
import be.kdg.kandoe.backend.retrofit.service.util.MyCallback;
import be.kdg.kandoe.backend.retrofit.service.util.RetrofitException;
import be.kdg.kandoe.backend.retrofit.service.factory.ServiceGenerator;
import be.kdg.kandoe.frontend.adapters.RemarkAdapter;
import be.kdg.kandoe.utils.MyLog;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Ruben on 07/03/2017.
 */

public class RemarkOverzichtDailogFragment extends BaseDialogFragment implements RemarkEditDialogFragment.RemarkEditDialogFragmentListener {


    static final String TAG = RemarkOverzichtDailogFragment.class.getSimpleName();
    @BindView(android.R.id.list)
    ListView listview;
    @BindView(android.R.id.empty)
    TextView emptyTv;

    @BindView(R.id.add)
    FloatingActionMenu fabMenu;

    Call<List<Remark>> mCall;


    void load() {
        if (!isLoggedIn())
            return;
        emptyTv.setText(R.string.loading);
        mCall = ServiceGenerator.getInstance().createSessionService().getRemarks(getCard().getSession().getId(), getCard().getId());
        manage(mCall);
        mCall.enqueue(new MyCallback<List<Remark>>() {
            @Override
            public void onFailure(Call<List<Remark>> call, RetrofitException t) {
                if(!call.isCanceled()) {
                    handleOnFailure(t);
                    emptyTv.setText(t.getErrorResult().getMessage());
                }
            }

            @Override
            public void onResponse(Call<List<Remark>> call, Response<List<Remark>> response) {
                if(!call.isCanceled()) {
                    emptyTv.setText(R.string.geen_reviews_beschikbaar);
                    MyLog.d(TAG, "remarks loaded");
                    updateRemarks(response.body());
                }
            }
        });
    }

    static final String KEY_CARD = "card";
    static final String KEY_COMMENT_ALLOWED = "comment_allowed";


    public static RemarkOverzichtDailogFragment newInstance(SessionCard sessionCard) {
        return newInstance(sessionCard, calcCommentaryAllowed(sessionCard));
    }

    static boolean calcCommentaryAllowed(SessionCard sessionCard) {
        User u = LoginHelperImpl.getInstance().getUser();
        if (u == null)
            return false;

        return sessionCard.getSession().isCommentaryAllowed() && sessionCard.getSession().isParticipant(u);
    }

    public static RemarkOverzichtDailogFragment newInstance(SessionCard sessionCard, boolean commentaryAllowed) {
        RemarkOverzichtDailogFragment fragment = new RemarkOverzichtDailogFragment();
        fragment.setCard(sessionCard);
        fragment.getArguments().putBoolean(KEY_COMMENT_ALLOWED, commentaryAllowed);
        return fragment;
    }

    void setCard(SessionCard card) {
        getOrCreateArgs().putSerializable(KEY_CARD, card);
    }

    static final String KEY_REMARKS = "remarks";

    void setRemarks(List<Remark> remarkList) {
        getOrCreateArgs().putSerializable(KEY_REMARKS, new ArrayList<>(remarkList));
    }

    boolean isCommentaryAllowed() {
        return getArguments().getBoolean(KEY_COMMENT_ALLOWED);
    }

    void initViews(Bundle savedInstanceState) {
        listview.setEmptyView(emptyTv);
        if (isCommentaryAllowed()) {
            fabMenu.setOnMenuButtonClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    handleOnAddClick();
                }
            });
        } else
            fabMenu.setVisibility(View.GONE);


    }

    @Override
   protected void loadData() {
        load();
    }

    void handleOnAddClick() {
        RemarkEditDialogFragment f = RemarkEditDialogFragment.newInstance(getCard(), this);
        showDialog(f);
    }


    void setAdapter(List<Remark> items) {
        RemarkAdapter adapter = new RemarkAdapter(getActivity(), items);
        listview.setAdapter(adapter);
    }

    void refreshAdapter() {
        resetAdapter();
    }

    void resetAdapter() {
        MyLog.d(getClass(), "resetAdapter");
        setAdapter(getRemarks());
    }

    SessionCard getCard() {
        return (SessionCard) getArguments().getSerializable(KEY_CARD);
    }

    List<Remark> getRemarks() {
        return (List<Remark>) getOrCreateArgs().getSerializable(KEY_REMARKS);
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        MyLog.d(getClass(), "onCreateDialog");
        View v = getActivity().getLayoutInflater().inflate(R.layout.remark_overzicht, null, false);
        ButterKnife.bind(this, v);

        AlertDialog d = new AlertDialog.Builder(getActivity()).setTitle(R.string.remarks).setPositiveButton(R.string.close, null).setView(v).create();
        initViews(savedInstanceState);
        return d;
    }

    void updateRemarks(List<Remark> remarks) {
        MyLog.d(getClass(), "updateRemarks: "+(remarks!=null?remarks.size() :null));
        if (isFragmentReady()) {
            setRemarks(remarks);
            refreshAdapter();
        }
    }

    @Override
    public void onRemarkMade(Remark remark) {
        Call<List<Remark>> call = ServiceGenerator.getInstance().createSessionService().uploadRemark(remark.getCard().getSession().getId(), remark.getCard().getId(), remark);
        call.enqueue(new MyCallback<List<Remark>>() {
            @Override
            public void onFailure(Call<List<Remark>> call, RetrofitException t) {
                if(!call.isCanceled()) {
                    handleOnFailure(t);
                }
            }

            @Override
            public void onResponse(Call<List<Remark>> call, Response<List<Remark>> response) {
                if(!call.isCanceled()) {
                    List<Remark> remarkList = response.body();
                    updateRemarks(remarkList);
                }
            }
        });
    }
}

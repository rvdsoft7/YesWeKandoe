package be.kdg.kandoe.frontend.adapters;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.frontend.views.UserView;
import be.kdg.kandoe.utils.Utils;
import butterknife.BindView;
import java8.util.stream.StreamSupport;


public class UserAdapter extends MyAdapter<User> {

	List<Long> organistors = new ArrayList<>();
	Long onTurn;

	@SuppressWarnings("unused")
    public List<Long> getOrganistors() {
		return organistors;
	}

	public void setOrganistors(List<Long> organistors) {
		this.organistors = organistors;
	}

	@SuppressWarnings("unused")
    public Long getOnTurn() {
		return onTurn;
	}

	public void setOnTurn(Long onTurn) {
		this.onTurn = onTurn;
	}

	public UserAdapter(Context context, List<User> items) {
		super(context, R.layout.user_row, items);
	}

	
	static class UserHolder extends ViewHolder {
		@BindView(R.id.user)
		UserView user;

	}
	
	@Override
	protected ViewHolder createViewHolder() {
		return new UserHolder();
	}
	@Override
	public void initItem(int position, User item, ViewHolder holder) {
		UserHolder userHolder = (UserHolder) holder;
		userHolder.user.setUser(item);
		boolean isOrganistor = false;
		if(!Utils.isEmpty(organistors)) {
			isOrganistor = StreamSupport.stream(organistors).anyMatch(id->item.getId()==id);
		}
		userHolder.user.setOrganistor(isOrganistor);
		userHolder.user.setIsOnTurn(item.getId()==onTurn);
	}

}

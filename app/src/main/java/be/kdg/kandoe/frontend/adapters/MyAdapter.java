package be.kdg.kandoe.frontend.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import be.kdg.kandoe.MyApplication;
import be.kdg.kandoe.utils.MyLog;

public abstract class MyAdapter<T> extends BaseAdapter {

	List<T> items;
	Context context = MyApplication.getInstance();
	LayoutInflater inflater;
	private int layoutResId;

	public MyAdapter(Context context, int layoutResId, List<T> items) {
		this.items = items == null ? Collections.emptyList() : new ArrayList<>(items);
		MyLog.d(getClass(), "new adapter: "+this.items.size());
		inflater = LayoutInflater.from(context);
		this.layoutResId = layoutResId;
	}

	public Context getContext() {
		return context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		ViewHolder holder;
		if (v == null) {
			v = getLayoutInflater().inflate(layoutResId, parent, false);
			holder = createViewHolder().init(v);
			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}

		initItem(position, getItem(position), holder);

		return v;
	}

	protected abstract ViewHolder createViewHolder();

	public abstract void initItem(int position, T item, ViewHolder holder);

	protected LayoutInflater getLayoutInflater() {
		return inflater;
	}

	public void clear() {
		if (items.size() > 0) {
			items.clear();
			notifyDataSetChanged();
		}
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public T getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

}

package be.kdg.kandoe.frontend.views;

import java.util.Objects;

public class Coordinate{
        private float x, y;
    private int index;

        public Coordinate(float x, float y) {
            this.x = x;
            this.y = y;
        }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinate that = (Coordinate) o;
        return Integer.compare(that.getXInt(), getXInt()) == 0 &&
                Integer.compare(that.getYInt(), getYInt()) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getXInt(), getYInt());
    }

    public float getX() {
            return x;
        }

        public float getY() {
            return y;
        }

        public int getXInt() {
            return Math.round(x);
        }

    public int getYInt() {
        return Math.round(y);
    }

        @Override
        public String toString() {
            return index+"->"+x+":"+y;
        }

    @SuppressWarnings("unused")
    int getIndex() {
        return this.index;
    }


    public void setIndex(int index) {
        this.index = index;
    }
}
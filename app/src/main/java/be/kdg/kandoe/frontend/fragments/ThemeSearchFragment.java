package be.kdg.kandoe.frontend.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.content.Theme;
import be.kdg.kandoe.backend.retrofit.service.util.MyCallback;
import be.kdg.kandoe.backend.retrofit.service.util.RetrofitException;
import be.kdg.kandoe.backend.retrofit.service.factory.ServiceGenerator;
import be.kdg.kandoe.backend.retrofit.service.api.theme.ThemeService;
import be.kdg.kandoe.frontend.activities.BaseActivity;
import be.kdg.kandoe.frontend.adapters.MyAdapter;
import be.kdg.kandoe.frontend.views.MyEditText;
import be.kdg.kandoe.utils.MyLog;
import be.kdg.kandoe.utils.Utils;
import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Response;

public class ThemeSearchFragment extends BaseThemeOverzichtFragment {


	@BindView(R.id.themeSearchEdit)
	MyEditText themeSearchEdit;
	Call<List<Theme>> mCall;

	String text = "";

	Handler handler = new Handler();

	Runnable search = () -> performSearch(text);


	public static ThemeSearchFragment newInstance() {
		ThemeSearchFragment fragment = new ThemeSearchFragment();
		fragment.getOrCreateArgs();
		return fragment;
	}

	@Override
	protected void initBaseActivity(BaseActivity baseActivity, ActionBar ab) {
		ab.setTitle(R.string.menu_search_themes);
		ab.setSubtitle("");
	}

	private void handleOnTextChange(String newText) {
        if(isFragmentReady())
		doSearch(newText);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		setHasOptionsMenu(false);
		return inflater.inflate(R.layout.theme_search, container, false);
	}

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        emptyTv.setText("");
        themeSearchEdit.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                handleOnTextChange(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

	@Override
	void loadData() {
		if(themeSearchEdit.length()==0)
			load("");
	}

	void performSearch(String text) {
		emptyTv.setText(R.string.loading);
		MyAdapter<?> adapter = getAdapter();
		if (adapter != null) {
			adapter.clear();
		}

		load(text);
	}

	public void load(String query) {
		MyLog.d(getClass(), "load" + mCall);
		if (mCall != null) {
			mCall.cancel();
		}
		ThemeService themeService = ServiceGenerator.getInstance().createThemeSevice();
		mCall = Utils.isEmpty(query) ? themeService.themes() : themeService.filterThemes(query);
		mCall.enqueue(new MyCallback<List<Theme>>() {
			@Override
			public void onFailure(Call<List<Theme>> call, RetrofitException t) {
				handleOnFailure(t, false);
                if(isFragmentReady())
				setAdapter(null);
				if(!call.isCanceled())
				emptyTv.setText(t.getErrorResult().getMessage());
			}

			@Override
			public void onResponse(Call<List<Theme>> call, Response<List<Theme>> response) {
				if(!call.isCanceled()) {
					setAdapter(response.body());
					if(getAdapter().getCount()==0) {
						emptyTv.setText(R.string.no_results);
					}
				}
			}
		});

	}

	public void doSearch(String text) {
		this.text = text;
		handler.removeCallbacks(search);
		handler.postDelayed(search, 200);
	}



}

package be.kdg.kandoe.frontend.fragments;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.Session.Session;
import be.kdg.kandoe.backend.dom.content.Theme;
import be.kdg.kandoe.backend.retrofit.service.util.MyCallback;
import be.kdg.kandoe.backend.retrofit.service.util.RetrofitException;
import be.kdg.kandoe.backend.retrofit.service.factory.ServiceGenerator;
import be.kdg.kandoe.frontend.activities.BaseActivity;
import be.kdg.kandoe.frontend.adapters.SessionAdapter;
import be.kdg.kandoe.utils.FragmentHelper;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

public class SessieOverzichtFragment extends BaseFragment {


    @BindView(R.id.listView)
    ListView listView;
    @BindView(R.id.emptyTV)
    TextView emptyTv;
    static final String KEY_THEME = "theme", KEY_MODE = "mode";

    static final int MODE_THEME = 1, MODE_VORIGE = 2, MODE_ACTIEVE = 3, MODE_GEPLANDE = 4;


    static final String KEY_SESSIONS = "sessions";


    Call<List<Session>> mCall;

    List<Session> getSessions() {
        return (List<Session>) getOrCreateArgs().getSerializable(KEY_SESSIONS);
    }

    void setSessions(List<Session> sessions) {
        if(sessions!=null)
        getOrCreateArgs().putSerializable(KEY_SESSIONS, new ArrayList<>(sessions));
    }
    public static SessieOverzichtFragment newInstance(Theme theme) {
        SessieOverzichtFragment f = new SessieOverzichtFragment();
        f.setTheme(theme);
        f.setMode(MODE_THEME);
        return f;
    }
    public static SessieOverzichtFragment newInstance(int mode, List<Session> sessions) {
        SessieOverzichtFragment f = new SessieOverzichtFragment();
       f.setSessions(sessions);
        f.setMode(mode);
        return f;
    }



    void load() {
        mCall = ServiceGenerator.getInstance().createSessionService().getSessionByThemeId(getTheme().getId());
        manage(mCall);
        mCall.enqueue(new MyCallback<List<Session>>() {
            @Override
            public void onFailure(Call<List<Session>> call, RetrofitException t) {
                if(!call.isCanceled())
                handleOnFailure(t);
            }

            @Override
            public void onResponse(Call<List<Session>> call, Response<List<Session>> response) {
                if(!call.isCanceled()) {
                    setSessions(response.body());
                    refreshAdapter();
                }
            }
        });
    }

    void setMode(int mode) {
        getOrCreateArgs().putInt(KEY_MODE, mode);
    }

    int getMode() {
        return getOrCreateArgs().getInt(KEY_MODE);
    }

    void setTheme(Theme theme) {
        getOrCreateArgs().putSerializable(KEY_THEME, theme);
    }

    Theme getTheme() {
        return (Theme) getOrCreateArgs().getSerializable(KEY_THEME);
    }



    void setAdapter(List<Session> items) {
        SessionAdapter adapter = new SessionAdapter(getActivity(), items==null ? new ArrayList<>() : items);
        listView.setAdapter(adapter);
    }

    SessionAdapter getAdapter() {
        return (SessionAdapter) listView.getAdapter();
    }

    void refreshAdapter() {
        resetAdapter();
    }

    void resetAdapter() {
        setAdapter(getSessions());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

    }


    @Override
    protected void initBaseActivity(BaseActivity baseActivity, ActionBar ab) {
        if (getMode() == MODE_THEME) {
            ab.setTitle(R.string.sessions);
            ab.setSubtitle(getTheme().getName());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(false);
        return inflater.inflate(R.layout.session_overzicht, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        ButterKnife.bind(this, view);
        listView.setEmptyView(emptyTv);

        listView.setOnItemClickListener(itemClickListener);

        resetAdapter();

    }

    @Override
    void loadData() {
        if (getMode() == MODE_THEME) {
            load();
        }
    }

    void play(int pos) {
        Session s = getAdapter().getItem(pos);
        KandoeBoardFragment f = KandoeBoardFragment.newInstance(s);
        new FragmentHelper(getBaseActivity()).open(f);
    }

    OnItemClickListener itemClickListener = (parent, view, position, id) -> {
       play(position);
    };

}

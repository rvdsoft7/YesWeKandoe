package be.kdg.kandoe.frontend.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.login.ILoginHelper;
import be.kdg.kandoe.backend.login.LoginHelperImpl;
import be.kdg.kandoe.backend.login.LoginListener;
import be.kdg.kandoe.backend.login.SimpleLoginListener;
import be.kdg.kandoe.backend.login.kandoe.KandoeSignInHelper;
import be.kdg.kandoe.backend.retrofit.service.util.ErrorResult;
import be.kdg.kandoe.frontend.activities.BaseActivity;
import butterknife.OnClick;

public class ProfielBewerkFragment extends BaseRegistreerFragment {


    ILoginHelper loginHelper;
    KandoeSignInHelper defaultSignInHelper;

    public static ProfielBewerkFragment newInstance() {
        ProfielBewerkFragment r = new ProfielBewerkFragment();
        return r;
    }

    LoginListener l = new SimpleLoginListener() {

        @Override
        public void onAuthenticationFailed(ErrorResult errorResult) {
                setLoading(false);
                showMessage(errorResult.getMessage());
        }

        public void onProfileEdited(User user) {
            setLoading(false);
            System.out.println("ProfileEdited");
            showMessage(getString(R.string.profile_saved));
        }

    };

    @Override
    protected void initBaseActivity(BaseActivity baseActivity, ActionBar actionBar) {
        super.initBaseActivity(baseActivity, actionBar);
        actionBar.setTitle(R.string.menu_mijn_profiel);
        actionBar.setSubtitle("");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.profiel_bewerk, container, false);
    }


    @OnClick(R.id.saveButton)
    public void save(View view) {
        if (!mValidator.validate())
            return;

        User u = generateUser();
        User loggedInUser = getLoggedInUser();
        u.setId(loggedInUser.getId());
        u.setUsername(loggedInUser.getUsername());
        setLoading(true);
        defaultSignInHelper.editProfile(u);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        loginHelper.unregister(l);
    }

    void setData(User u) {
        voornaamEdit.setText(u.getFirstName());
        achternaamEdit.setText(u.getLastName());
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loginHelper = LoginHelperImpl.getInstance();
        loginHelper.register(l);
        loginHelper.setActivity(getBaseActivity());
        defaultSignInHelper = loginHelper.getDefaultSignInHelper();
        setData(getLoggedInUser());
    }
}

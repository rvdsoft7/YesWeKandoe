package be.kdg.kandoe.frontend.views;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import be.kdg.kandoe.utils.MyLog;
import be.kdg.kandoe.utils.StringUtils;
import be.kdg.kandoe.utils.Utils;
import java8.util.stream.StreamSupport;

/**
 * Created by Ruben on 05/03/2017.
 */

public class KandoeCircle extends CircleView implements Comparable<KandoeCircle> {
    private int level;
    private KandoeBoard kandoeBoard;
    private HashMap<Coordinate, KandoeCard> map = new LinkedHashMap<>();
    private CircleSlots slots;


    public KandoeCircle(Context context) {
        super(context);
        init();
    }

    public KandoeCircle(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public void resetSlots() {
        map.clear();
    }

    public void putSlot(Coordinate coordinate, KandoeCard card) {
        map.put(coordinate, card);
    }

    public boolean hasFreeSlot(Coordinate coordinate) {
     return map.get(coordinate)==null;
    }

    @SuppressWarnings("unused")
    public boolean hasEmptySlot() {
        return StreamSupport.stream(getPositions()).anyMatch(x -> map.get(x) == null);
    }


    @SuppressWarnings("unused")
    public boolean canMoveUp() {
        MyLog.d(getClass(), "canMoveUp: " + level + " " + (kandoeBoard.circles.size() - 1));
        return !(level == kandoeBoard.circles.size() - 1);
    }


    void init() {
        //setHighlightedCircleColor(getResources().getColor(R.color.circle_highlight_color));
    }

    List<Coordinate> getPositions() {
        return slots.getSlots();
    }

    void init2() {
        slots = new CircleSlots(getSize(), getStrokeWidth() * 2);
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public KandoeBoard getKandoeBoard() {
        return kandoeBoard;
    }

    public void setKandoeBoard(KandoeBoard kandoeBoard) {
        this.kandoeBoard = kandoeBoard;
    }

    @Override
    public void setCircleColor(int circleColor) {
        System.out.println("testcircleColorr: "+ StringUtils.toHexString(circleColor));
        super.setCircleColor(circleColor);
    }

    @Override
    public void setSize(int size) {
        super.setSize(size);
        init2();
    }

    @Override
    public String toString() {
        return "Circle:" +getLevel();
    }

    @Override
    public int compareTo(@NonNull KandoeCircle o) {
        return Integer.compare(level, o.level);
    }
}

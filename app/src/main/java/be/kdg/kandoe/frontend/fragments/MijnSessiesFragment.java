package be.kdg.kandoe.frontend.fragments;

import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rey.material.widget.TabPageIndicator;

import java.util.ArrayList;
import java.util.List;

import be.kdg.kandoe.MyApplication;
import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.Session.Session;
import be.kdg.kandoe.backend.retrofit.service.util.MyCallback;
import be.kdg.kandoe.backend.retrofit.service.util.RetrofitException;
import be.kdg.kandoe.backend.retrofit.service.factory.ServiceGenerator;
import be.kdg.kandoe.frontend.activities.BaseActivity;
import be.kdg.kandoe.utils.Utils;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

import static be.kdg.kandoe.utils.SessionPredicates.filterSessions;
import static be.kdg.kandoe.utils.SessionPredicates.isActive;
import static be.kdg.kandoe.utils.SessionPredicates.isPast;
import static be.kdg.kandoe.utils.SessionPredicates.isPlanned;

public class MijnSessiesFragment extends BaseFragment {


    static final int PAGER_COUNT = 3;

    @BindView(R.id.viewpager)
    ViewPager mPager;

    @BindView(R.id.titles)
    TabPageIndicator tabIndicator;

    Call<List<Session>> mCall;

    public static MijnSessiesFragment newInstance() {
        MijnSessiesFragment fragment = new MijnSessiesFragment();
        fragment.getOrCreateArgs();
        return fragment;
    }

    static final String KEY_SESSIONS = "sessions";

    List<Session> getSessions() {
        return (List<Session>) getOrCreateArgs().getSerializable(KEY_SESSIONS);
    }

    void setSessions(List<Session> sessions) {
        getOrCreateArgs().putSerializable(KEY_SESSIONS, new ArrayList<>(sessions));
    }

    void load() {
        if(!isLoggedIn())
            return;
        mCall = ServiceGenerator.getInstance().createSessionService().getSessionsByUserId(getLoggedInUser().getId());
        manage(mCall);
        mCall.enqueue(new MyCallback<List<Session>>() {
            @Override
            public void onFailure(Call<List<Session>> call, RetrofitException t) {
                if(!call.isCanceled())
                handleOnFailure(t);
            }

            @Override
            public void onResponse(Call<List<Session>> call, Response<List<Session>> response) {
                if(!call.isCanceled()) {
                    setSessions(response.body());
                    initViews(getSessions());
                }
            }
        });
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(false);
        return inflater.inflate(R.layout.viewpager_layout, container, false);
    }

    protected PagerAdapter createAdapter() {
        return new MyPagerAdapter(getChildFragmentManager());
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        ButterKnife.bind(this, view);
        mPager.setOffscreenPageLimit(1);
        mPager.setAdapter(createAdapter());
        tabIndicator.setViewPager(mPager);
        tabIndicator.setOnPageChangeListener(mOnPageChangeListener);

    }

    @Override
    void loadData() {
        load();
    }

    OnPageChangeListener mOnPageChangeListener = new OnPageChangeListener() {

        @Override
        public void onPageSelected(int pos) {
            ActivityCompat.invalidateOptionsMenu(getActivity());
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {
        }
    };


    @Override
    protected void initBaseActivity(BaseActivity baseActivity, ActionBar actionBar) {
        super.initBaseActivity(baseActivity, actionBar);
        actionBar.setTitle(R.string.menu_my_sessions);
        actionBar.setSubtitle("");
    }


    void initViews(List<Session> sessions) {
        mPager.getAdapter().notifyDataSetChanged();
    }


    static int[] TITLES = {R.string.previous, R.string.active, R.string.planned};

    class MyPagerAdapter extends FragmentStatePagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public int getItemPosition(Object item) {
            SessieOverzichtFragment fragment = (SessieOverzichtFragment) item;
            if (!Utils.isEmpty(fragment.getSessions())) {
                switch (fragment.getMode()) {
                    case SessieOverzichtFragment.MODE_VORIGE:
                        return 0;
                    case SessieOverzichtFragment.MODE_ACTIEVE:
                        return 1;
                    case SessieOverzichtFragment.MODE_GEPLANDE:
                        return 2;
                }

            }
            return POSITION_NONE;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return MyApplication.getInstance().getString(TITLES[position]);
        }

        @Override
        public Fragment getItem(int pos) {
            List<Session> sessions = getSessions();
            int mode = 0;
            switch (pos) {
                case 0:
                    mode = SessieOverzichtFragment.MODE_VORIGE;
                    sessions = filterSessions(sessions, isPast());
                    break;
                case 1:
                    mode = SessieOverzichtFragment.MODE_ACTIEVE;
                    sessions = filterSessions(sessions, isActive());
                    break;
                case 2:
                    mode = SessieOverzichtFragment.MODE_GEPLANDE;
                    sessions = filterSessions(sessions, isPlanned());
                    break;
            }
            SessieOverzichtFragment f = SessieOverzichtFragment.newInstance(mode, sessions);
            return f;
        }

        @Override
        public int getCount() {
            return PAGER_COUNT;
        }

    }


}

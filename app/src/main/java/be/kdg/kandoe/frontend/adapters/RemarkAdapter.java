package be.kdg.kandoe.frontend.adapters;

import android.content.Context;
import android.widget.TextView;

import com.wefika.flowlayout.FlowLayout;

import java.util.List;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.content.Remark;
import be.kdg.kandoe.frontend.views.UserView;
import be.kdg.kandoe.utils.FlowLayoutHelper;
import be.kdg.kandoe.utils.MyLog;
import be.kdg.kandoe.utils.Utils;
import butterknife.BindView;

public class RemarkAdapter extends MyAdapter<Remark> {


	public RemarkAdapter(Context context, List<Remark> items) {
		super(context, R.layout.remark_row, items);
	}

	
	static class RemarkHolder extends ViewHolder {
		@BindView(R.id.remarkText)
		TextView text;
		@BindView(R.id.remarkUser)
		UserView user;
		@BindView(R.id.remarkDate)
		TextView timestamp;

	}
	
	@Override
	protected ViewHolder createViewHolder() {
		return new RemarkHolder();
	}
	@Override
	public void initItem(int position, Remark item, ViewHolder holder) {
		MyLog.d(getClass(), "initItem: "+position);
		RemarkHolder remarkHolder = (RemarkHolder) holder;
		remarkHolder.text.setText(item.getText());
		remarkHolder.timestamp.setText(Utils.DATE_TIME_FORMATTER.format(item.getTimeStamp()));
		remarkHolder.user.setUser(item.getUser());
	}

}

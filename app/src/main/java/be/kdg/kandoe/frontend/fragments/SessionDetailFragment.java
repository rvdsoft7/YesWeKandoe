package be.kdg.kandoe.frontend.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.wefika.flowlayout.FlowLayout;

import java.util.ArrayList;
import java.util.Arrays;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.Session.Session;
import be.kdg.kandoe.backend.retrofit.service.util.MyCallback;
import be.kdg.kandoe.backend.retrofit.service.util.RetrofitException;
import be.kdg.kandoe.backend.retrofit.service.factory.ServiceGenerator;
import be.kdg.kandoe.frontend.fragments.dialog.BaseDialogFragment;
import be.kdg.kandoe.utils.FlowLayoutHelper;
import be.kdg.kandoe.utils.MyLog;
import be.kdg.kandoe.utils.Utils;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Ruben on 27/02/2017.
 */

public class SessionDetailFragment extends BaseDialogFragment {

    @BindView(R.id.flDeelnemers)
    FlowLayout deelnemers;
    @BindView(R.id.tvTheme)
    TextView themeNaam;

    @BindView(R.id.tvNaam)
    TextView naam;

    @BindView(R.id.chkAddingAdmitted)
    CheckBox addingAdmitted;

    @BindView(R.id.chkCommentaryAllowed)
    CheckBox commentaryAllowed;

    @BindView(R.id.tvEndDate)
            TextView endDate;

    @BindView(R.id.tvStartDate)
            TextView startDate;

    @BindView(R.id.tvSessionStatus)
            TextView sessionStatus;

    @BindView(R.id.tvSessionType)
            TextView sessionType;

    @BindView(R.id.tvMinKaarten)
    TextView minKaarten;

    @BindView(R.id.tvMaxKaarten)
    TextView maxKaarten;


    Call<Session> mCall;


    public static SessionDetailFragment newInstance(long id) {
        SessionDetailFragment fragment = new SessionDetailFragment();
        fragment.setIdToFetch(id);
        return fragment;
    }

    public static SessionDetailFragment newInstance(Session session) {
        SessionDetailFragment fragment = new SessionDetailFragment();
        fragment.setSession(session);
        return fragment;
    }

    static final String KEY_ID_TO_FETCH = "item_id";

    public void setIdToFetch(long id) {
        getOrCreateArgs().putLong(KEY_ID_TO_FETCH, id);
    }

    public long getIdToFetch() {
        return getOrCreateArgs().getLong(KEY_ID_TO_FETCH);
    }

    static final String KEY_SESSION = "session";

    public void setSession(Session session) {
        getOrCreateArgs().putSerializable(KEY_SESSION, session);
    }

    public Session getSession() {
        return (Session) getOrCreateArgs().getSerializable(KEY_SESSION);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    protected void loadData() {
        long id = getIdToFetch();
        if(getIdToFetch()==0){
            MyLog.d(getClass(), "session set manually, not need to fetch");
            onLoaded();
        } else {
            MyLog.d(getClass(), "idToFetch specified, fetching session "+id);
            load(id);
        }
    }

    void load(long id) {
        mCall = ServiceGenerator.getInstance().createSessionService().getSession(id);
        manage(mCall);
        mCall.enqueue(new MyCallback<Session>() {
            @Override
            public void onFailure(Call<Session> call, RetrofitException t) {
                if(!call.isCanceled())
                handleOnFailure(t);
            }

            @Override
            public void onResponse(Call<Session> call, Response<Session> response) {
                if(!call.isCanceled()) {
                    setSession(response.body());
                    onLoaded();
                }
            }
        });
    }

    void onLoaded() {
        MyLog.d(getClass(), "onLoaded");
        initViews(getSession());
    }

    @OnClick(R.id.themeButton)
    public void onThemeButtonClicked(View v) {
        ThemeDetailFragment f = ThemeDetailFragment.newInstance(getSession().getTheme());
        showDialog(f);
    }

    @OnClick(R.id.showCardsBtn)
    public void onCardsButtonClicked(View v) {
        CardOverzichtFragment cardOverzichtFragment = CardOverzichtFragment.newInstance(getSession());
        showDialog(cardOverzichtFragment);
    }

    void initViews(Session session) {
        naam.setText(session.getName());
        commentaryAllowed.setChecked(session.isCommentaryAllowed());
        addingAdmitted.setChecked(session.isAddingAdmitted());
        themeNaam.setText(session.getTheme().getName());
        startDate.setText(Utils.ifNull(session.getStartDateToString(), getString(R.string.not_available)));
        endDate.setText(Utils.ifNull(session.getEndDateToString(), getString(R.string.not_available)));
        sessionType.setText(session.getSessionTypeName(getActivity()));
        sessionStatus.setText(session.getSessionStatusName(getActivity()));
        minKaarten.setText("Min: "+session.getMinCards());
        maxKaarten.setText("Max: "+session.getMaxCards());
        new FlowLayoutHelper(deelnemers).setUsers(session.getUsers(), new ArrayList<>(Arrays.asList(session.getOrganiserId())), session.getCurrentUserId());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(false);
        return inflater.inflate(R.layout.session_detail, container, false);
    }
}

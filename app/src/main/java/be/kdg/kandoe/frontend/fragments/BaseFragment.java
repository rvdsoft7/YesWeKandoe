package be.kdg.kandoe.frontend.fragments;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.view.ViewParent;
import android.widget.EditText;

import be.kdg.kandoe.backend.dom.user.User;
import be.kdg.kandoe.backend.login.LoginHelperImpl;
import be.kdg.kandoe.backend.retrofit.service.util.ErrorResult;
import be.kdg.kandoe.backend.retrofit.service.util.RetrofitException;
import be.kdg.kandoe.frontend.activities.BaseActivity;
import be.kdg.kandoe.utils.CallHelper;
import be.kdg.kandoe.utils.MyLog;
import retrofit2.Call;

public class BaseFragment extends Fragment {

    public void setLoading(boolean loading) {
        this.getBaseActivity().setLoading(loading);
    }


    private ErrorResult errorResult;

    static final String KEY_ERROR_RESULT = "error_result";
   CallHelper callHelper = new CallHelper();

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(KEY_ERROR_RESULT, errorResult);
    }

    @Override
    public void onResume() {
        super.onResume();
        MyLog.d(getClass(), "onResume()");
    }

    public boolean isLoggedIn() {
        return getLoggedInUser()!=null;
    }

    public User getLoggedInUser() {
        return LoginHelperImpl.getInstance().getUser();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if(savedInstanceState!=null)
        errorResult = (ErrorResult) savedInstanceState.getSerializable(KEY_ERROR_RESULT);
    }

    public boolean isFragmentReady() {
        return !isDetached() && getFragmentManager() != null;
    }

    protected void manage(Call<?> call) {
        callHelper.add(call);
    };


    void loadData() {

    }

    @Nullable
    protected TextInputLayout getTextInputLayout(@NonNull EditText editText) {
        View currentView = editText;
        for (int i = 0; i < 2; i++) {
            ViewParent parent = currentView.getParent();
            if (parent instanceof TextInputLayout) {
                return (TextInputLayout) parent;
            } else {
                currentView = (View) parent;
            }
        }
        return null;
    }

    public void showMessageLong(String message) {
        getSnackbar(message, Snackbar.LENGTH_LONG).show();
    }

    public void showMessage(String message) {
        showMessageLong(message);
    }

    protected Snackbar getSnackbar(String message, int length) {
      return getBaseActivity().getSnackbar(message, length);
    }


    protected void showDialog(DialogFragment f) {
        getBaseActivity().showDialog(f);
    }


    @Override
    public void onStart() {
        super.onStart();
        MyLog.d(getClass(), "onStart");
    }

    @Override
    public void onStop() {
        super.onStop();
        MyLog.d(getClass(), "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        MyLog.d(getClass(), "onDestroy()");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        MyLog.d(getClass(), "onDestroyView");
        cancelAllCalls();
    }

    protected void cancelAllCalls() {
        MyLog.d(getClass(), "cancelAllCalls");
       if(callHelper.cancelAll()) {
           setLoading(false);
       }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initBaseActivity(getBaseActivity(), getBaseActivity().getSupportActionBar());
        getActivity(). setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        loadData();
    }

    public BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    public BaseFragment getSelf() {
        return this;
    }

    protected void initBaseActivity(BaseActivity baseActivity, ActionBar actionBar) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    protected Bundle getOrCreateArgs() {
        if (getArguments() == null) {
            setArguments(new Bundle());
        }
        return getArguments();
    }

    protected void handleOnFailure(RetrofitException throwable) {
        handleOnFailure(throwable, true);
    }

    protected void handleOnFailure(RetrofitException throwable, boolean showMsg) {
        ErrorResult result = throwable.getErrorResult();
        MyLog.d(getClass(), "Retrofit error:::"+result, throwable);
        if(showMsg) {
            if (result != null && isFragmentReady())
                showMessage(result.getMessage());
        }

    }


    public ErrorResult getErrorResult() {
        return errorResult;
    }

    public void clearErrorResult() {
        setErrorResult(null);
    }

    public void setErrorResult(ErrorResult errorResult) {
        this.errorResult = errorResult;
    }

}

package be.kdg.kandoe.frontend.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.util.AttributeSet;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.Session.SessionCard;
import be.kdg.kandoe.utils.Utils;

/**
 * Created by Ruben on 05/03/2017.
 */

public class KandoeCard extends CircleView implements Comparable<KandoeCard> {

    private SessionCard mCard;

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    private Coordinate coordinate;

    private Paint textPaint;



   public void detachFromCircle() {
       boolean attached = false;
        if(!attached) {
            setCircle(null);
            setCoordinate(null);
            setCard(null);
        }
    }

    public void setCircle(KandoeCircle circle) {
        this.circle = circle;
        boolean attached = circle!=null;
        //setClickable(attached);
//        setLongClickable(attached);
//        Drawable backgroundOld = mBackground;
//        mBackground = attached?listSelectorBackground : transparentBackground;
//        if(mBackground!=backgroundOld) {
//            setBackground(mBackground);
//        }
    }
    KandoeCircle circle;
    public SessionCard getCard() {
        return mCard;
    }

    public KandoeCircle getCircle() {
        return circle;
    }
    public int getLevel() {
        return getCircle().getLevel();
    }

    public boolean canMoveUp() {
        return getLevel()<getCircle().getKandoeBoard().circles.size()-1;
    }
    @Override
    public String toString() {
        if(getCard()==null)
            return "NULL CARD";
        return getCard().getText();
    }

    public void setCard(SessionCard card) {
        this.mCard =card;
        if(mCard!=null)
        setCircleColor(Color.parseColor(card.getColor()));
        else {
           // setCircleColor(0);
        }
    }

    public boolean isDetachedFromCircle() {
        return circle==null;
    }


    public KandoeCard(Context context) {
        this(context, null);
    }

    public KandoeCard(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        textPaint.setTextSize(canvas.getWidth()/2);
        int xPos = (canvas.getWidth() / 2);
        int yPos = (int) ((canvas.getHeight() / 2) - ((textPaint.descent() + textPaint.ascent()) / 2)) ;
        //((textPaint.descent() + textPaint.ascent()) / 2) is the distance from the baseline to the center.
        if(getCard()!=null)
        canvas.drawText(String.valueOf(Character.toUpperCase(getCard().getText().charAt(0))), xPos, yPos, textPaint);
    }

    void init() {
        textPaint = new Paint();
        textPaint.setColor(Color.WHITE);
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setTextSize(300);
        setBackgroundResource(R.drawable.list_selector);
        setClickable(true);
      setStrokeWidth(Utils.convertDipToPixels(1, getResources().getDisplayMetrics()));
        getPaint().setStyle(Paint.Style.FILL);
    }

    @Override
    public int compareTo(@NonNull KandoeCard o) {
        int compare = o.getCircle().compareTo(getCircle());
        if(compare==0) {
            compare = getCard().compareTo(o.getCard());
        }
        return compare;
    }
}

package be.kdg.kandoe.frontend.adapters;

import android.content.Context;
import android.widget.TextView;

import java.util.List;

import be.kdg.kandoe.R;
import be.kdg.kandoe.backend.dom.Session.Session;
import be.kdg.kandoe.utils.Utils;
import butterknife.BindView;

public class SessionAdapter extends MyAdapter<Session> {

	public SessionAdapter(Context context, List<Session> items) {
		super(context, R.layout.session_row, items);
	}
	
	static class SessionHolder extends ViewHolder {
		@BindView(R.id.sessionName)
		TextView text;
		@BindView(R.id.sessionTheme)
		TextView theme;
		@BindView(R.id.sessionCurrentUser)
		TextView currentUser;
		@BindView(R.id.sessionStatus)
		TextView status;
		@BindView(R.id.sessionNumberOfPlayers)
		TextView numberOfPlayers;
		@BindView(R.id.sessionCurrentRound)
		TextView currentRound;

		@BindView(R.id.sessionType)
		TextView sessionType;


	}
	
	@Override
	protected ViewHolder createViewHolder() {
		return new SessionHolder();
	}
	@Override
	public void initItem(int position, Session item, ViewHolder holder) {
		SessionHolder sessionHolder = (SessionHolder) holder;
		sessionHolder.text.setText(item.getName());
		sessionHolder.theme.setText(item.getTheme().getName());
		sessionHolder.numberOfPlayers.setText("Number of Players: "+item.getUsers().size());
		sessionHolder.sessionType.setText("Session Type: "+item.getSessionTypeName(getContext()));
		sessionHolder.status.setText("Session Status: "+item.getSessionStatusName(getContext()));
		String username = item.getCurrentUser()!=null?item.getCurrentUser().getUsername():null;
		sessionHolder.currentUser.setText("Current user: "+ Utils.ifNull(username, getContext().getResources().getString(R.string.not_available)));
		sessionHolder.currentRound.setText("Current round: "+ (item.getCurrentRound() > 0 ? item.getCurrentRound() : getContext().getResources().getString(R.string.not_available)));
	}

}
